﻿using System;
using DDay.iCal;
using DDay.iCal.Serialization;
using DDay.iCal.Serialization.iCalendar;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Data.Entity;
using Walkers.Project360.Model;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using MCS.McsLogging;
using System.Data.Entity.SqlServer;
using System.Data;
using System.Configuration;
using System.Web;
using Excel;
using System.ComponentModel.DataAnnotations;
using System.Web.ModelBinding;
using System.Reflection;
using System.ComponentModel;
using System.Xml.Linq;
using Walkers.Project360.Model.ClientPortalIdentity;
using System.Collections.Specialized;
using System.IO.Compression;


namespace Walkers.Project360.DataAccess
{
    public partial class Repository : IDisposable, ISuitRepository, IClientPortalRepository
    {
        #region Private Members

        private bool _disposed = false;
        private EntityModel _db;
        private ViewpointEntities _ViewpointContext;
        private AderantEntities _AderantContext;

        #endregion

        #region Constructors
        public Repository()
        {
            _db = new EntityModel();
            _db.Configuration.AutoDetectChangesEnabled = true;
            _ViewpointContext = new ViewpointEntities();
            _AderantContext = new AderantEntities();
        }
        #endregion

        #region Homepage

        public Page GetHomepage(int userId)
        {
            if (userId != 0)
            {
                var x = _db.Users
                    .Include(u => u.Pages)
                    .Include(u => u.Pages.Select(p => p.PageWidgets.Select(pw => pw.Widget)))
                    .Include(u => u.Pages.Select(p => p.PageWidgets.Select(pw => pw.DefaultDisplaySetting)))
                    .Include(u => u.Pages.Select(p => p.PageWidgets.Select(pw => pw.CustomDisplaySetting)))
                    .Single(u => u.Id == userId);

                return x.Homepage ?? InitialiseHomepage(userId);
            }

            else throw new ArgumentNullException("user");
        }

        public Page InitialiseHomepage(int userId)
        {
            if (userId != 0)
            {
                var user = _db.Users
                    .Include(u => u.Pages)
                    .Include(u => u.Pages.Select(p => p.PageWidgets.Select(pw => pw.Widget)))
                    .Single(u => u.Id == userId);

                //Recreate homepage if it already exists
                if (user.Pages.Count > 0)
                    _db.Pages.RemoveRange(user.Pages);

                user.AddHomePage();

                user.Homepage.AddRequiredWidgets(this.GetNewRoleWidgets);

                _db.SaveChanges();

                return user.Homepage;
            }
            else throw new ArgumentNullException("user");
        }

        public void ReorderPageWidget(int widgetId, int pageId, int Column, int SortOrder)
        {
            PageWidget pWidget = _db.PageWidgets
                .Single(i => i.Widget.Id == widgetId && i.Page.Id == pageId);

            if (pWidget != null && (pWidget.Column != Column || pWidget.SortOrder != SortOrder))
            {
                pWidget.Column = Column;
                pWidget.SortOrder = SortOrder;
                _db.SaveChanges();
            }
        }

        public void RemovePageWidget(int widgetId, int pageId)
        {
            PageWidget w = _db.PageWidgets
                .Single(i => i.Widget.Id == widgetId && i.Page.Id == pageId);

            if (w != null)
            {
                _db.PageWidgets.Remove(w);
                _db.SaveChanges();
            }
        }

        public void AddPageWidget(User user, int widgetId, int roleId)
        {
            var h = GetHomepage(user.Id);

            var roleWidget = _db.RoleWidgets
                .Include(w => w.DisplaySetting)
                .Include(w => w.Widget)
                .Single(w => w.Widget.Id == widgetId && w.Role.Id == roleId);

            if (h != null && roleWidget != null)
            {
                //Add the new widget to the top-left
                int top = h.PageWidgets.Where(pw => pw.Column == 0).Select(pw => pw.SortOrder).DefaultIfEmpty().Min() - 1;

                h.AddPageWidget(new PageWidget
            {
                Page = h,
                Widget = roleWidget.Widget,
                DefaultDisplaySetting = roleWidget.DisplaySetting,
                SortOrder = top,
                Column = 0,
            });

                _db.SaveChanges();
            }
        }

        public void UpdatePageWidgetStyle(int widgetId, int pageId, string Style)
        {
            PageWidget pageWidget = _db.PageWidgets
                .Single(i => i.Widget.Id == widgetId && i.Page.Id == pageId);

            if (pageWidget != null)
            {
                pageWidget.UpdateStyle(Style);
            }

            _db.SaveChanges();
        }

        //private void AddRequiredWidgets(Page page, bool includeDefault = true)
        //{ //required Widgets do not 
        //    if (page != null)
        //    {
        //        var existingWidgets = page.PageWidgets.Select(w => w.Widget.Id);
        //        List<RoleWidget> newWidgets = GetNewRoleWidgets(includeDefault, existingWidgets, page.User.Id);
        //        page.AddPageWidgets(newWidgets);
        //        //var query = _db.RoleWidgets
        //        //        .Include(r => r.Widget)
        //        //        .OrderBy(r => r.DisplaySetting.SortOrder)
        //        //        .Where(r => (( r.Role.Users.Any(i => i.User.Id == page.User.Id)) && !existingWidgets.Contains(r.Widget.Id)));

        //        //if (includeDefault)
        //        //    query = query.Where(r => r.DisplaySetting.IsRequired || r.IsDefault);
        //        //else
        //        //    query = query.Where(r => r.DisplaySetting.IsRequired);

        //        //var newWidgets = query.ToList();

        //        //if (newWidgets.Count() > 0)
        //        //{
        //        //    foreach (RoleWidget roleWidget in newWidgets)
        //        //    {
        //        //        page.AddPageWidget(new PageWidget
        //        //            {
        //        //                Page = page,
        //        //                Widget = roleWidget.Widget,
        //        //                DefaultDisplaySetting = roleWidget.DisplaySetting,
        //        //                SortOrder = newWidgets.IndexOf(roleWidget),
        //        //                Column = newWidgets.IndexOf(roleWidget) % 3,
        //        //            });
        //        //    }
        //        //}
        //    }
        //}
        public List<RoleWidget> GetNewRoleWidgets(bool includeDefaults, IEnumerable<int> existingWidgetIds, IEnumerable<int> roleIds)
        {
            var query = _db.RoleWidgets
                    .Include(r => r.DisplaySetting)
                    .Include(r => r.Widget)
                                .OrderBy(r => r.DisplaySetting.SortOrder)
                    .Where(r => ((roleIds.Contains(r.Role.Id)) && !existingWidgetIds.Contains(r.Widget.Id)));

            if (includeDefaults)
                query = query.Where(r => r.DisplaySetting.IsRequired || r.IsDefault);
            else
                query = query.Where(r => r.DisplaySetting.IsRequired);

            var newWidgets = query.ToList();
            return newWidgets;
        }
        public IQueryable<RoleOperation> GetAllowedOperations(User user)
        {
            IQueryable<RoleOperation> allowedOperations = _db.RoleOperations.Where(r => r.Role.Users.Any(i => i.User.Id == user.Id));
            return allowedOperations;
        }
        public List<RoleWidget> GetWidgetsAccessible(User user)
        {
            if (user != null)
            {
                var roleWidgets = _db.RoleWidgets
                    .Include(r => r.DisplaySetting)
                        .Include(r => r.Widget)
                        .Include(r => r.DisplaySetting)
                        .Where(r => (r.Role.Name == "Default" || r.Role.Users.Any(i => i.User.Id == user.Id)))
                    .ToList();


                //it is possible that a widget can be in multiple roles, if so, return the first one which is requried
                roleWidgets = roleWidgets
                    .GroupBy(r => r.Widget.Id)
                    .Select(g => g.OrderByDescending(r => r.DisplaySetting.IsRequired).First())
                    .OrderBy(r => r.Widget.Name)
                    .ToList();

                IQueryable<RoleOperation> allowedOperations = _db.RoleOperations.Where(r => r.Role.Users.Any(i => i.User.Id == user.Id));

                var results = AllowedWidgets(roleWidgets, allowedOperations);

                return results.ToList();

            }
            throw new ArgumentNullException("user");
        }
        public List<RoleWidget> AllowedWidgets(List<RoleWidget> possible, IQueryable<RoleOperation> allowedOperations)
        {
            var results = from roleWidget in possible
                          join allowedOperation in allowedOperations on roleWidget.Widget.Operation equals allowedOperation.CanDoOperation
                          select roleWidget;
            return results.ToList();
        }

        public PageWidget GetPageWidget(int widgetId, int pageId)
        {
            return _db.PageWidgets
                .Include(pw => pw.Page)
                .Include(pw => pw.Widget)
                .SingleOrDefault(pw => pw.PageId == pageId && pw.WidgetId == widgetId);
        }

        #endregion

        #region User

        public User Login(string username)
        {
            if (!string.IsNullOrEmpty(username))
            {
                //remove domain prefix from username
                username = Regex.Replace(username, ".*\\\\(.*)", "$1", RegexOptions.None);

                User user = GetUserProfile(username);
                if (user == null) //user profile doesn't exist --so hydrate from nt domain info
                {
                    user = GetNtUser(username);
                    _db.Users.Add(user);
                }
                //make sure set up of homepage is in order
                user.UserLoginChecks(this.GetRole, this.GetNewRoleWidgets, this.GetDefaultLinks, false);


                _db.SaveChanges();

                return user;
            }
            else
            {
                throw new ArgumentNullException("Username");
            }
        }

        public User GetViewpointUser(string ViewpointRefCode)
        {
            if (!string.IsNullOrEmpty(ViewpointRefCode))
            {
                try
                {
                    var result = _db.Users
                        .Include(u => u.Roles)
                        .SingleOrDefault(u => ViewpointRefCode == (u.ViewpointRefCode ?? u.Username));

                    return result;
                }
                catch (Exception ex)
                {
                    ex.LogException();
                    return null;
                }
            }
            else
            {
                //do nothing because it should not fail--just return no value
                //throw new ArgumentNullException("ViewpointUsername");
                return null;
            }
        }

        /// <summary>
        /// Return a matching user object for the supplied refCode or return a default instance
        /// </summary>
        /// <param name="ViewpointRefCode"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public User GetViewpointUserOrDefault(string ViewpointRefCode, string name)
        {
            User result;

            result = GetViewpointUser(ViewpointRefCode);
            if (result == null)
                result = new User { DisplayName = name };

            return result;
        }

        public void GetViewpointUser(ref List<ExceptionViolation> exceptionList)
        {
            if (exceptionList != null)
            {

                foreach (var violation in exceptionList)
                {
                    if (!string.IsNullOrEmpty(violation.DefaultUserName))
                    {
                        List<string> names = violation.DefaultUserName.SplitAndTrim();
                        foreach (string name in names)
                        {
                            User user = GetViewpointUser(name.Trim());
                            if (user != null)
                            {
                                violation.DefaultUsers.Add(user);
                            }

                        }
                    }
                }
            }
            else
            {
                throw new ArgumentNullException("exceptionList");
            }
        }

        private UserPrincipal GetUserPrinciple(string username)
        {//do they exist in the domain, if so get info from the domain
            PrincipalContext domain = new PrincipalContext(ContextType.Domain, "walkersglobal.com");
            UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(domain, username);
            if (userPrincipal == null)
                throw new ArgumentException(string.Format("Username '{0}' does not belong to the current domain", username));

            return userPrincipal;
        }

        private User GetUser(UserPrincipal p)
        {
            return new User
            {
                Username = p.SamAccountName,
                DisplayName = p.DisplayName,
                FirstName = p.GivenName,
                LastName = p.Surname,
                Email = p.EmailAddress,
                Department = p.GetDepartment(),
                Office = p.GetOffice(),
                EmployeeCode = p.GetEmployeeCode(),
                JobTitle = p.GetJobTitle(),
                Guid = p.Guid,
                IsActive = true
            };
        }

        private User GetNtUser(string username)
        {
            var userPrincipal = GetUserPrinciple(username);

            User result = new User
                {
                    Guid = userPrincipal.Guid
                };


            UpdateUser(GetUser(userPrincipal), result);

            return result;
        }

        private List<User> GetNtUsers(string ntGroup)
        {
            List<User> result = new List<User>();

            using (PrincipalContext domain = new PrincipalContext(ContextType.Domain, "walkersglobal.com"))
            {
                GroupPrincipal group = GroupPrincipal.FindByIdentity(domain, ntGroup);

                if (group != null)
                {
                    foreach (UserPrincipal p in group.GetMembers(true))
                    {
                        try
                        {
                            result.Add(GetUser(p));
                        }
                        catch (Exception ex)
                        {
                            ex.LogException("Error when retrieving user from active directory");
                        }
                    }
                }
            }

            return result;
        }

        private List<User> GetAllNtUsers()
        {
            return GetNtUsers(GetSetting("User.AllUserGroup"));
        }

        public void AddUser(User user)
        {
            _db.Users.Add(user);
        }

        public User GetUserProfile(string userName)
        {
            User result = _db.Users
                               .Include(u => u.Roles)
                               .Include(u => u.Roles.Select(r => r.Role))
                               .Include(u => u.Roles.Select(r => r.Role).Select(r => r.Operations))
                               .Include(u => u.Pages)
                               .Include(u => u.Pages.Select(p => p.PageWidgets))
                               .Include(u => u.Links)
                               .Include(u => u.Links.Select(ul => ul.Link).Select(l => l.Category))
                               .SingleOrDefault(u => u.Username == userName);
            return result;
        }

        public User GetClientPortalUser(string userName)
        {
            User result = _db.Users
                               .Include(u => u.Roles)
                               .Include(u => u.Roles.Select(r => r.Role))
                               .Include(u => u.Roles.Select(r => r.Role).Select(r => r.Operations))
                          .Where(u => u.Username == userName).FirstOrDefault();
            return result;
        }

        public User GetSystemUser()
        {
            return GetUser(GetSetting("User.SystemName"));
        }

        public User GetUser(int id)
        {
            return _db.Users.Find(id);
        }

        //public string GetUserToCommaDelimited(int id)
        //{
        //    var s = string.Join(",", _db.RoleUsers.Where(p => p.RoleId == id)
        //                             .Select(p => p.UserId.ToString()));
        //    return s;
        //}

        public User GetUser(string username)
        {
            return _db.Users.Where(u => u.Username == username).FirstOrDefault();
        }

        public List<User> GetUsers(string displayName)
        {
            if (!string.IsNullOrEmpty(displayName))
            {
                return _db.Users.Where(u => u.IsActive && u.DisplayName.ToLower().Contains(displayName.ToLower())).OrderBy(u => u.DisplayName).ToList();
            }
            else throw new ArgumentException(displayName);
        }

        public string GetUserEmail(int id)
        {
            string result;
            User user = _db.Users.Find(id);
            if (user != null)
            { result = user.Email; }
            else
            { result = string.Empty; }
            return result;
        }

        public List<User> GetUsersFromCommaDelimited(string CommaDelimitedUserNames)
        {
            List<User> result = new List<User>();

            if (!string.IsNullOrWhiteSpace(CommaDelimitedUserNames))
            {
                List<string> names = CommaDelimitedUserNames.SplitAndTrim();
                foreach (string name in names)
                {
                    string trimmedName = name.Trim();
                    var userList = _db.Users.Where(u => u.Username == trimmedName);
                    foreach (var user in userList.ToList())
                    {
                        result.Add(user);
                    }
                }
            }
            return result;
        }

        public List<User> GetUsersFromCommaDelimitedId(string CommaDelimitedUserIds)
        {
            List<User> result = new List<User>();

            if (!string.IsNullOrWhiteSpace(CommaDelimitedUserIds))
            {
                List<string> ids = CommaDelimitedUserIds.SplitAndTrim();
                foreach (string item in ids)
                {
                    int id;
                    if (int.TryParse(item.Trim(), out id))
                    {
                        var user = _db.Users.Where(u => u.Id == id).First();
                        if (user != null)
                        {
                            result.Add(user);
                        }
                    }
                }
            }
            return result;
        }

        private void UpdateUser(User source, User target)
        {
            if (target.IsActive != source.IsActive)
                target.IsActive = source.IsActive;

            if (target.Username != source.Username)
                target.Username = source.Username;

            if (target.DisplayName != source.DisplayName)
                target.DisplayName = source.DisplayName;

            if (target.FirstName != source.FirstName)
                target.FirstName = source.FirstName;

            if (target.LastName != source.LastName)
                target.LastName = source.LastName;

            if (target.Email != source.Email)
                target.Email = source.Email;

            if (target.Department != source.Department)
                target.Department = source.Department;

            if (target.Office != source.Office)
                target.Office = source.Office;

            if (target.EmployeeCode != source.EmployeeCode)
                target.EmployeeCode = source.EmployeeCode;

            if (target.JobTitle != source.JobTitle)
                target.JobTitle = source.JobTitle;
        }

        private List<User> GetAllUsersWithActiveDirectoryIdentifier()
        {
            return _db.Users.Where(u => u.Guid.HasValue).ToList();
        }

        public void SyncRoleMembership()
        {
            SyncNtRoleMembership();
            _db.SaveChanges();
        }

        private void SyncNtRoleMembership()
        {
            List<Role> rolesToSync = _db.Roles
                                   .Include(r => r.Users)
                                   .Include(r => r.Users.Select(u => u.User))
                                   .Where(r => !string.IsNullOrEmpty(r.ADGroupToSync))
                                   .ToList();

            foreach (Role r in rolesToSync)
            {
                List<Guid> userGUIDs = GetNtUsers(r.ADGroupToSync).Where(u => u.Guid.HasValue).Select(nt => nt.Guid.Value).ToList();
                List<User> users = _db.Users.Where(u => u.Guid.HasValue && userGUIDs.Contains(u.Guid.Value)).ToList();

                r.ReassignUsers(users);
            }
        }

        public void SyncUsers()
        {
            List<User> adusers = GetAllNtUsers();
            List<User> users = GetAllUsersWithActiveDirectoryIdentifier();

            //look for changes to exisiting users
            foreach (User u in users)
            {
                var c = adusers.Where(x => x.Guid == u.Guid).FirstOrDefault();

                //if the user has been removed, mark them as inactive
                if (c == null)
                    u.IsActive = false;
                else
                    //check for any changes to the ad properties
                    UpdateUser(c, u);
            }

            //look for new users
            var newUsers = adusers.Where(x => !users.Select(ux => ux.Guid).Contains(x.Guid));
            foreach (User u in newUsers)
            {
                _db.Users.Add(u);
            }

            _db.SaveChanges();
        }

        public void SyncUserPictures(string rooturl, string rootpath)
        {
            string username;
            User user;

            using (PrincipalContext domain = new PrincipalContext(ContextType.Domain, "walkersglobal.com"))
            {
                GroupPrincipal group = GroupPrincipal.FindByIdentity(domain, GetSetting("User.AllUserGroup"));

                foreach (Principal p in group.GetMembers(true))
                {
                    DirectoryEntry directoryEntry = p.GetUnderlyingObject() as DirectoryEntry;
                    PropertyValueCollection picture = directoryEntry.Properties["thumbnailPhoto"];

                    if (picture.Value != null && picture.Value is byte[])
                    {
                        byte[] thumbnailInBytes = (byte[])picture.Value;
                        Bitmap thumbnailPhoto = new Bitmap(new MemoryStream(thumbnailInBytes));

                        username = directoryEntry.Properties["samAccountName"].Value.ToString();

                        SaveJpeg(string.Format("{0}/{1}.jpg", rootpath, username), SquareImage(thumbnailPhoto, int.Parse(GetSetting("User.ThumbnailHeightInPixels"))));

                        user = this.GetUser(username);

                        if (user.Picture == null)
                        {
                            user.Picture = string.Format("{0}/{1}.jpg", rooturl, username);
                            _db.SaveChanges();
                        }
                    }
                }
            }
        }

        private void SaveJpeg(string path, Image img)
        {
            var qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
            var jpegCodec = GetEncoderInfo("image/jpeg");

            var encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            img.Save(path, jpegCodec, encoderParams);
        }

        private ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            return ImageCodecInfo.GetImageEncoders().FirstOrDefault(t => t.MimeType == mimeType);
        }

        private Image Resize(Image image, int newWidth, int maxHeight, bool onlyResizeIfWider)
        {
            if (onlyResizeIfWider && image.Width <= newWidth) newWidth = image.Width;

            var newHeight = image.Height * newWidth / image.Width;
            if (newHeight > maxHeight)
            {
                // Resize with height instead
                newWidth = image.Width * maxHeight / image.Height;
                newHeight = maxHeight;
            }

            var thumbnail = new Bitmap(newWidth, newHeight);

            using (var graphic = Graphics.FromImage(thumbnail))
            {
                graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphic.SmoothingMode = SmoothingMode.HighQuality;
                graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphic.CompositingQuality = CompositingQuality.HighQuality;
                graphic.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return thumbnail;
        }

        private Image Crop(Image img, Rectangle cropArea)
        {
            var bmpImage = new Bitmap(img);
            var bmpCrop = bmpImage.Clone(cropArea, bmpImage.PixelFormat);
            return bmpCrop;
        }

        private Image SquareImage(Image img, int length)
        {
            Image result;
            int delta = Math.Abs(img.Height - img.Width);

            if (img.Height > img.Width)
                result = Crop(img, new Rectangle(0, delta / 2, img.Width, img.Height - delta));
            else
                result = Crop(img, new Rectangle(delta / 2, 0, img.Width - delta, img.Height));

            result = Resize(result, length, length, false);

            return result;
        }


        #endregion

        #region Role

        public List<Role> GetRoles()
        {
            return _db.Roles.OrderBy(r => r.Title).ToList();
        }

        public Role GetRole(int? id)
        {
            return _db.Roles
                .Include(r => r.Users)
                .Include(r => r.Users.Select(u => u.User))
                .Include(r => r.Widgets)
                .Include(r => r.Widgets.Select(w => w.Widget))
                .Include(r => r.Operations)
                .Where(r => r.Id == id)
                .FirstOrDefault();
        }

        public Role GetRole(string name)
        {
            return _db.Roles
                .Include(r => r.Users)
                .Include(r => r.Users.Select(u => u.User))
                .Include(r => r.Widgets)
                .Include(r => r.Widgets.Select(w => w.Widget))
                .Include(r => r.Operations)
                .Where(r => r.Name == name)
                .FirstOrDefault();
        }
        /// <summary>
        /// Return a list of users that can do an operation
        /// </summary>
        /// <param name="op"></param>
        /// <returns></returns>
        public List<User> GetUsersAllowed(Operation op)
        {
            var query = _db.Users
                .Where(u => u.Roles.Any(r => r.Role.Operations.Any(o => o.CanDoOperation == op)));

            return query.ToList();
        }

        public List<User> GetUsersInRole(string roleName)
        {
            return _db.Users
                .Where(u => u.Roles.Any(r => r.Role.Name == roleName))
                .ToList();
        }

        public List<User> GetUsersInRoles(string[] roleNames)
        {
            return _db.Users
                .Where(u => u.Roles.Any(r => roleNames.Contains(r.Role.Name)))
                .ToList();
        }

        public List<Operation> GetOperationFromCommaDelimitedId(string CommaDelimitedOperationIds)
        {
            List<Operation> result = new List<Operation>();

            if (!string.IsNullOrWhiteSpace(CommaDelimitedOperationIds))
            {
                List<string> ids = CommaDelimitedOperationIds.SplitAndTrim();
                foreach (string item in ids)
                {
                    int id;
                    if (int.TryParse(item.Trim(), out id))
                    {
                        if (Enum.IsDefined(typeof(Operation), id))
                        {
                            result.Add((Operation)id);
                        }
                    }
                }
            }
            return result;
        }

        public void AttachRole(Role role)
        {
            System.Data.Entity.Infrastructure.DbEntityEntry dbEntity = _db.Entry(role);
            switch (dbEntity.State)
            {
                case EntityState.Detached:
                    _db.Roles.Attach(role);
                    break;
                //case EntityState.Added:
                //_db.Roles.Add(role);
                //break;
            }

            if (role.Operations != null)
            {
                foreach (var rOperation in role.Operations)
                {
                    System.Data.Entity.Infrastructure.DbEntityEntry dbEntityParentEntry = _db.Entry(rOperation);

                    switch (dbEntityParentEntry.State)
                    {
                        case EntityState.Detached:
                            _db.RoleOperations.Attach(rOperation);
                            break;
                        case EntityState.Added:
                            _db.RoleOperations.Add(rOperation);
                            break;
                    }
                }
            }
            if (role.Users != null)
            {
                foreach (var item in role.Users)
                {
                    System.Data.Entity.Infrastructure.DbEntityEntry dbEntityParentEntry = _db.Entry(item);

                    switch (dbEntityParentEntry.State)
                    {
                        case EntityState.Detached:
                            _db.RoleUsers.Attach(item);
                            break;
                        case EntityState.Added:
                            _db.RoleUsers.Add(item);
                            break;
                    }
                }
            }
        }

        public Role AddRole()
        {
            Role result = new Role();
            this.AddRole(result);
            return result;
        }

        public void AddRole(Role role)
        {
            Role result = new Role();
            role.Users = result.Users;

            _db.Roles.Add(role);
            //_db.SaveChanges();
        }

        public void SaveRole(Role role)
        {
            //this.AttachRole(role);
            _db.SaveChanges();
        }

        public void DeleteRole(int? id)
        {
            Role r = GetRole(id);
            _db.Roles.Remove(r);
            _db.SaveChanges();
        }

        public void AddUserRole(RoleUser userRole)
        {
            if (userRole != null)
            {
                _db.RoleUsers.Add(userRole);
            }

        }


        #endregion

        #region Widget
        public void AddWidget(Widget widget)
        {
            _db.Widgets.Add(widget);
        }
        #endregion

        #region Link

        public Link GetLink(int id)
        {
            return _db.Links
                .Include(l => l.Category)
                .Where(l => l.Id == id)
                .Single();
        }

        public List<Link> GetEnabledPublicLinks()
        {
            return _db.Links
                .Include(l => l.Category)
                .Where(l => !l.IsPrivate && l.Enabled)
                .OrderBy(l => l.Title)
                .ToList();
        }

        public List<Link> GetPublicLinks()
        {
            return _db.Links
                .Include(l => l.Category)
                .Where(l => !l.IsPrivate)
                .OrderBy(l => l.Title)
                .ToList();
        }

        public List<Link> GetDefaultLinks()
        {
            return _db.Links
                .Include(l => l.Category)
                .Where(l => l.IsDefault && !l.IsPrivate)
                .ToList();
        }

        public LinkCategory GetCustomCategory()
        {
            return _db.LinkCategories
                .Single(lc => lc.Title == LinkCategory.CUSTOM_LINK_CATEGORY_NAME);
        }

        public LinkCategory GetCategory(string title)
        {
            return _db.LinkCategories
                .Single(lc => lc.Title == title);
        }

        public LinkCategory GetCategoryById(int id)
        {
            return _db.LinkCategories
                .Where(l => l.Id == id)
                .Single();
        }

        public List<LinkCategory> GetLinkCategories()
        {
            return _db.LinkCategories.OrderBy(lc => lc.Title).ToList();
        }

        public List<UserLink> GetUserLinks(int userId)
        {
            return _db.UserLinks
                .Include(ul => ul.Link)
                .Include(ul => ul.Link.Category)
                .Where(l => l.User.Id == userId)
                .ToList();
        }

        public List<LinkCategory> GetSelectedCategories()
        {
            return _db.LinkCategories.ToList();
        }

        public void AddUserLink(UserLink link)
        {
            _db.UserLinks.Add(link);
            _db.SaveChanges();
        }

        public void AddSystemLinks(Link link)
        {
            System.Data.Entity.Infrastructure.DbEntityEntry dbEntity = _db.Entry(link);
            switch (dbEntity.State)
            {
                case EntityState.Detached:
                    _db.Links.Attach(link);
                    break;
            }
            _db.Links.Add(link);
            _db.SaveChanges();

        }

        public void RemoveUserLink(UserLink link)
        {
            _db.UserLinks.Remove(link);
            _db.SaveChanges();
        }

        public void RemoveLinks(Link link)
        {
            var li = _db.Links.Where(d => d.Id == link.Id);
            _db.Links.RemoveRange(li);
            _db.SaveChanges();
        }

        public void EditLink(Link link)
        {
            var LinkInDb = GetLink(link.Id);

            _db.Entry(LinkInDb).CurrentValues.SetValues(link);
            LinkInDb.Category = GetCategoryById(link.Category.Id);
            _db.SaveChanges();
        }

        #endregion

        #region Message

        public List<Message> GetMessageSearchSubject(string subject)
        {
            if (!string.IsNullOrEmpty(subject))
            {
                return _db.Messages.Where(u => u.Subject.ToLower().Contains(subject.ToLower())).ToList();
            }
            else throw new ArgumentException(subject);
        }

        public Message GetMessage(int id)
        {
            if (id > 0)
            {
                var q = _db.Messages
                    .Include(m => m.To)
                    .Include(m => m.From)
                    .Single(m => m.Id == id);

                //we need to eager load the workflow if it is of the MessageWorkflow type
                //todo, determine if there is a better way to do this.
                if (q is MessageCard && ((q as MessageCard).Card.Id <= 0))
                    throw new ArgumentOutOfRangeException();

                return q;
            }
            else throw new ArgumentOutOfRangeException("id");
        }

        public List<Message> GetMessages(User user, int page, int pageSize)
        {
            if (user != null)
            {
                return _db.Messages
                    .Include(m => m.To)
                    .Include(m => m.From)
                    .OrderByDescending(m => m.Sent)
                    .Skip(page * pageSize)
                    .Take(pageSize)
                    .Where(m => m.To.Id == user.Id)
                    .ToList();

            }
            else throw new ArgumentNullException("user");
        }

        public List<Message> GetSentMessages(User user)
        {
            if (user != null)
            {
                return _db.Messages
                    .Include(m => m.To)
                    .Include(m => m.From)
                    .OrderByDescending(m => m.Sent)
                    .Where(m => m.From.Id == user.Id)
                    .ToList();
            }
            else throw new ArgumentNullException("user");
        }

        public int GetUnseenMessageCount(User user)
        {
            if (user != null)
            {
                return _db.Messages.Where(m => m.To.Id == user.Id && m.Seen == null).Count();
            }
            else throw new ArgumentNullException("user");
        }

        public int GetMessageCount(User user)
        {
            if (user != null)
            {
                return _db.Messages.Where(m => m.To.Id == user.Id).Count();
            }
            else throw new ArgumentNullException("user");
        }

        public List<Message> GetLatestMessages(User user, int limit = 1000)
        {
            if (user != null)
            {
                return _db.Messages.Where(m => m.To.Id == user.Id && m.Read == null).OrderByDescending(m => m.Sent).Take(limit).ToList();

            }
            else throw new ArgumentNullException("user");
        }

        private Message CreateWelcomeMessage(User user)
        {
            MessageHtml result =
             new MessageHtml
            {
                To = user,
                From = GetSystemUser(),
                Subject = GetSetting("Message.WelcomeSubject"),
                Body = GetSetting("Message.WelcomeBody")
            };
            if (result.Body == null || result.Subject == null || result.To == null) { throw new ArgumentNullException("Welcome Message fields"); }
            return result;
        }

        public void SendMessage(Message message)
        {
            System.Data.Entity.Infrastructure.DbEntityEntry dbEntityEntry = _db.Entry(message.To);
            if (dbEntityEntry.State == EntityState.Detached)
            {   //replace the user with one that is pulled into the context otherwise errors occur
                message.To = this.GetUser(message.To.Id);
            }

            _db.Messages.Add(message);
            _db.SaveChanges();
        }

        public void SendMessage(List<Message> messageList)
        {
            foreach (var message in messageList)
            {
                this.SendMessage(message);
            }
        }

        public void ArchiveMessage(int id)
        {
            Message m = GetMessage(id);
            m.IsArchived = true;
            _db.SaveChanges();
        }

        public void UnArchiveMessage(int id)
        {
            Message m = GetMessage(id);
            m.IsArchived = false;
            _db.SaveChanges();
        }
        #endregion

        #region Suit
        public Suit GetSuit(int id)
        {
            return _db.Suits.Where(s => s.Id == id).FirstOrDefault();
        }

        public Suit GetSuit(string evalBehavoirName)
        {
            return _db.Suits.Where(s => s.EvalBehaviorName == evalBehavoirName).FirstOrDefault();
        }

        public IQueryable<Suit> GetSuits()
        {
            return _db.Suits;
        }

        public List<Suit> GetSuitsByDeckId(int id)
        {
            return _db.Suits.Where(s => s.Deck.Id == id).ToList();
        }

        public void EnableSuit(Suit suit)
        {
            if (!suit.Enabled)
            {
                suit.Enabled = true;

                _db.SaveChanges();
            }
        }

        public void DisableSuit(Suit suit, User user)
        {
            if (suit.Enabled)
            {
                suit.Enabled = false;

                //close all open cards for this suit
                var cards = _db.Cards.Where(c => c.Suit.Id == suit.Id && c.Status == CardStatusOptions.Open).ToList();

                string title = string.Format("<strong>{0}</strong> completed the card", user.DisplayName);

                foreach (Card c in cards)
                {
                    c.Status = CardStatusOptions.Completed;
                    c.DateCompleted = DateTimeOffset.Now;
                    c.AddEvent(CardEventType.Completed, user, title, "Disabled Suit");
                }

                _db.SaveChanges();
            }
        }

        public void SaveRunningSuit(Suit suit)
        {
            suit.IsRunning = true;
            suit.LastStarted = DateTime.Now;
            _db.SaveChanges();
        }
        public void SaveFinishedSuit(Suit suit)
        {
            suit.IsRunning = false;
            suit.LastExecuted = DateTime.Now;
            _db.SaveChanges();
        }

        #endregion

        #region Deck
        public Deck GetDeck(int? id)
        {
            return _db.Decks.Where(d => d.Id == id).FirstOrDefault();
        }

        public List<Deck> GetDecks()
        {
            return _db.Decks.OrderBy(d => d.DeckName).ToList();
        }

        public Deck AddDeck()
        {
            Deck result = new Deck();
            this.AddDeck(result);
            return result;
        }

        public void AddDeck(Deck deck)
        {
            deck.Enabled = true;
            _db.Decks.Add(deck);
        }

        public void SaveDeck(Deck deck)
        {
            _db.SaveChanges();
        }


        public void EnableDeck(Deck deck)
        {
            if (!deck.Enabled)
            {
                deck.Enabled = true;

                _db.SaveChanges();
            }
        }

        public void DisableDeck(Deck deck, User user)
        {
            if (deck.Enabled)
            {
                deck.Enabled = false;
 
                var suits = _db.Suits.Where(c => c.Deck.Id == deck.Id && c.Enabled == true).ToList();

                foreach (Suit s in suits)
                {
                    DisableSuit(s, user);
                }

                 _db.SaveChanges();
            }
        }
        #endregion

        #region AssignmentAcceptanceHistory

        public void SaveApprovalResponse(int CardId, string response, string comment, User currentUser)
        {
            string title = string.Format("<strong>{0}</strong> responded: {1}",
                                        currentUser.DisplayName,
                                        response);
            //save the response 
            Card currentCard = this.GetCard(CardId);
            currentCard.AddEvent(CardEventType.ApprovalResponse, currentUser, title, comment);

            AssignResponse knownResponse;
            switch (response)
            {
                case "Accept":
                    knownResponse = AssignResponse.Accepted;
                    break;
                case "Decline":
                    knownResponse = AssignResponse.Declined;
                    break;
                default:
                    throw new NotImplementedException("Accept or Decline response was not recognized.");
            }

            AssignRole currentRole = SuitRoleLookup.GetRole(currentCard.Suit);

            var cardResponse = from approvHist in _db.AssignHistories
                               where approvHist.ReferenceCode == currentCard.ReferenceCode && approvHist.HistoryStatus == AssignHistoryStatus.Open && approvHist.Role == currentRole
                               select approvHist;
            foreach (var hist in cardResponse.ToList())
            {
                //there should only be one row.
                hist.Response = knownResponse;
                hist.ResponseDate = DateTime.Now;
                if (knownResponse == AssignResponse.Accepted)
                {
                    hist.HistoryStatus = AssignHistoryStatus.Current;
                }
                //History status should remain open if declined
                //else if (knownResponse==AssignResponse.Declined){hist.HistoryStatus = AssignHistoryStatus.Open;}

                //complete because the card user has responded
                currentCard.Complete(currentUser);

            }
            _db.SaveChanges();
            //the rest of the process ocurrs when the suit is run again.
        }
        private List<vw_W360_EntityAdmin> GetAssignmentEntities(AssignRole role)
        {
            List<vw_W360_EntityAdmin> entities;
            switch (role)
            {
                case AssignRole.ViewpointRelationshipManager:
                    entities = _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.IsCurrent && !string.IsNullOrEmpty(e.RelationshipMgrCode)).ToList();
                    break;
                case AssignRole.ViewpointCseOfficer:
                    entities = _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.IsCurrent && !string.IsNullOrEmpty(e.RelationshipMgrCode) && !string.IsNullOrEmpty(e.CseOfficerCode)).ToList();
                    break;
                case AssignRole.ViewpointCddOfficer:
                    entities = _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.IsCurrent && !string.IsNullOrEmpty(e.RelationshipMgrCode) && !string.IsNullOrEmpty(e.CddOfficerCode)).ToList();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("AssignRole");
            }
            return entities;
        }
        private bool ProcessAssignmentHistory(AssignRole role)
        {
            bool results = false;

            List<vw_W360_EntityAdmin> entities = GetAssignmentEntities(role);
            List<AssignHistory> existingAssignments = _db.AssignHistories.Where(a => a.Role == role && a.HistoryStatus != AssignHistoryStatus.Closed).ToList();



            //updated person now appears in the entity in Viewpoint for the given role, but the history table doesn't reflect that change yet
            var query = from ent in entities
                        join hist in existingAssignments on ent.EntCode equals hist.ReferenceCode
                        where hist.AssignedPersonCode != (role == AssignRole.ViewpointRelationshipManager ? ent.RelationshipMgrCode :  //check each role at a time when the corresponding suit is run
                                                            role == AssignRole.ViewpointCseOfficer ? ent.CseOfficerCode :
                                                            role == AssignRole.ViewpointCddOfficer ? ent.CddOfficerCode : null)
                        select new AssignHistory
                        {
                            AssignedDate = DateTime.Now,
                            ReferenceCode = ent.EntCode,
                            ReferenceName = ent.SearchName,
                            Response = AssignResponse.Pending,
                            HistoryStatus = AssignHistoryStatus.Open,
                            Role = role,
                            AssignedPersonCode = (role == AssignRole.ViewpointRelationshipManager ? ent.RelationshipMgrCode :  //check each role at a time when the corresponding suit is run
                                                            role == AssignRole.ViewpointCseOfficer ? ent.CseOfficerCode :
                                                            role == AssignRole.ViewpointCddOfficer ? ent.CddOfficerCode : null)
                        };

            foreach (AssignHistory row in query.ToList())
            {
                results = true;
                List<AssignHistory> updatedAssignments = existingAssignments.FindAll(x => x.ReferenceCode == row.ReferenceCode && x.Role == role).ToList();
                foreach (AssignHistory updated in updatedAssignments)
                {
                    updated.CompleteDate = DateTime.Now;
                    updated.HistoryStatus = AssignHistoryStatus.Closed;
                }
                this.AddAssignHistory(row);
            }

            //close any assignments for entities that are no longer in scope
            var obsoleteQ = from hist in existingAssignments
                            join ent in entities on hist.ReferenceCode equals ent.EntCode into gf
                            from sf in gf.DefaultIfEmpty()
                            where sf == null
                            select hist;

            foreach (AssignHistory row in obsoleteQ.ToList())
            {
                results = true;
                row.CompleteDate = DateTime.Now;
                row.HistoryStatus = AssignHistoryStatus.Closed;
            }

            _db.SaveChanges();


            //Get any new assignments where the Viewpoint doesn't match the assignment history
            //List<AssignHistory> existingAssignments2 = _db.AssignHistories.Where(a => a.Role == role && a.HistoryStatus != AssignHistoryStatus.Closed).ToList();
            var newAssignments = from ent in entities
                                 join hist in existingAssignments on ent.EntCode equals hist.ReferenceCode into gf
                                 from sf in gf.DefaultIfEmpty()
                                 where sf == null
                                 select new AssignHistory
                                 {
                                     AssignedDate = DateTime.Now,
                                     AssignedPersonCode = (role == AssignRole.ViewpointRelationshipManager ? ent.RelationshipMgrCode :  //identify the person to assign based on the role being processed
                                                             role == AssignRole.ViewpointCseOfficer ? ent.CseOfficerCode :
                                                             role == AssignRole.ViewpointCddOfficer ? ent.CddOfficerCode : null),
                                     ReferenceCode = ent.EntCode,
                                     ReferenceName = ent.SearchName,
                                     Response = AssignResponse.Pending,
                                     HistoryStatus = AssignHistoryStatus.Open,
                                     Role = role
                                 };

            foreach (AssignHistory row in newAssignments.ToList())
            {
                results = true;
                this.AddAssignHistory(row);
            }
            return results;
        }

        private void AddAssignHistory(AssignHistory row)
        {
            _db.AssignHistories.Add(row);
        }

        private List<ExceptionViolation> GetEntityAssigned(AssignRole role)
        {
            var query = from assign in _db.AssignHistories
                        where assign.HistoryStatus == AssignHistoryStatus.Open && assign.Response == AssignResponse.Pending //only get the ones that are actively needing approval
                        && assign.Role == role
                        select new ExceptionViolation
                        {
                            ReferenceCode = assign.ReferenceCode,
                            ReferenceName = assign.ReferenceName,
                            ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                            DefaultUserName = assign.AssignedPersonCode,

                        };
            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetAssignedRejected(AssignRole assignRole)
        {
            string relationshipManagerAssigner = this.GetSetting("ViewpointAssignerRelationship");
            List<vw_W360_EntityAdmin> entities = _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.RelationshipMgrCode != null).ToList();
            List<AssignHistory> assignHistories = _db.AssignHistories.Where(a => a.HistoryStatus == AssignHistoryStatus.Open  //only get the ones that are actively needing approval
                                                                                && a.Role == assignRole
                                                                                && a.Response == AssignResponse.Declined).ToList();

            var query = from assign in assignHistories
                        join ent in entities on assign.ReferenceCode equals ent.EntCode
                        select new ExceptionViolation
                        {
                            ReferenceCode = assign.ReferenceCode,
                            ReferenceName = assign.ReferenceName,
                            ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                            DefaultUserName = (assignRole == AssignRole.ViewpointCddOfficer ? assign.AssignedPersonCode : assignRole != AssignRole.ViewpointRelationshipManager ? ent.RelationshipMgrCode : relationshipManagerAssigner),
                            ReferenceDetail = assign.AssignedPersonCode + " has declined the assignment of the role " + assignRole.ToString()
                        };
            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }


        #endregion

        #region Suit for Processes
        public void RunProcess(Suit suit)
        {
            try
            {
                switch (suit.EvalBehaviorName)
                {
                    case "SyncUsers":
                        this.SyncUsers();
                        break;
                    case "SyncRoles":
                        this.SyncRoleMembership();
                        break;
                    case "SyncVpToWeb":
                        this.SyncVpToWeb();
                        break;
                    case "Sync360ToWeb":
                        this.Sync360ToWeb();
                        break;
                    case "AuditDataChanges":
                        this.AuditData();
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Can run a process for suit '{0}'. The suit is not linked to a process in the Run Process method in repository", suit.SuitName));
                }
            }
            catch (Exception ex)
            {
                suit.AutomationException = ex;
                ex.LogException("Suit Behavior name: " + suit.EvalBehaviorName);
            }
        }

        private void Sync360ToWeb()
        {
            _db.Database.ExecuteSqlCommand("execute dbo.WC_usp_SynchToClientPortal");
        }

        private void SyncVpToWeb()
        {
            var result = _ViewpointContext.WC_usp_SynchToClientPortal();
        }

        public void AuditData()
        {
            var configs = _db.AuditConfigs.Where(a => a.Enabled == true);
            DateTimeOffset currentDate = DateTime.Now;
            string errorDescription = string.Empty;

            foreach (var audit in configs.ToList())
            {
                try
                {
                    //get the source data to audit
                    string query = audit.GetQuery();
                    List<AuditResults> current = this.GetAuditValues(audit, query);

                    //get the audit history that has been stored for this config
                    var auditHistory = _db.AuditHistories.Where(a => a.AuditConfigId == audit.id && a.IsCurrent == true);

                    //get changes to the auditted field
                    List<AuditHistory> changedAuditData = audit.GetChangedAuditData(current, auditHistory, currentDate);

                    //remove current flag from previous row for changed values
                    List<AuditHistory> oldRows = audit.ProcessPrevious(auditHistory, changedAuditData);

                    //add new rows to context
                    _db.AuditHistories.AddRange(changedAuditData);

                    _db.SaveChanges();
                }
                catch (Exception ex)
                {
                    string configDescr = " audit configuration id: " + audit.id.ToString() + " " + audit.DataSetName;
                    ex.LogException(configDescr);
                    errorDescription = "Error(s) occurred in Audit Data; Please check error log for more detail. The last error in Audit Data method was: " + ex.ToString() + configDescr;
                }
            }
            if (!string.IsNullOrEmpty(errorDescription))
            {
                throw new Exception(errorDescription);
            }
        }
        public AuditResults GetAuditValues(AuditConfig config, string query, string referenceCode)
        {//Get the row from the audit source dataset for the supplied referenceCode
            AuditResults results;
            switch (config.DataContext)
            {
                case DataContext.Viewpoint:
                    results = this._ViewpointContext.Database.SqlQuery<AuditResults>(query + " Where convert(varchar(max)," + config.ReferenceFieldName + ") = " + referenceCode).FirstOrDefault();
                    break;

                default: throw new NotImplementedException("Getting current audit values failed because the configured DataContext not found in GetAuditValues.");
            }
            return results;
        }

        public List<AuditResults> GetAuditValues(AuditConfig config, string query)
        {
            List<AuditResults> results;
            switch (config.DataContext)
            {
                case DataContext.Viewpoint:
                    results = this._ViewpointContext.Database.SqlQuery<AuditResults>(query).ToList();
                    break;

                default: throw new NotImplementedException("Getting current audit values failed because the configured DataContext not found in GetAuditValues.");
            }
            return results;
        }

        #endregion

        #region Card Exception Switch Statement
        public List<ExceptionViolation> GetViolations(Suit suit)
        {
            List<ExceptionViolation> result = null;
            try
            {
                switch (suit.EvalBehaviorName)
                {
                    case "MissingOfficerAppointDate":
                        result = GetOfficerMissingDate();
                        break;
                    case "InvalidEntityEmailAddress":
                        result = GetInvalidEmailAddresses();
                        break;
                    case "MissingBillingEmailAddress":
                        result = GetMissingBillingEmailAddresses();
                        break;
                    case "InGroupNoSubGroup":
                        result = GetInGroupNoSubGroup();
                        break;
                    case "InSubGroupNoMasterGroup":
                        result = GetInSubGroupNoMasterGroup();
                        break;
                    case "MailingAddressMissing":
                        result = GetMailingMissing();
                        break;
                    case "RoAddressMissing":
                        result = GetRoAddressMissing();
                        break;
                    case "MultipleEmailAddress":
                        result = GetMultipleEmailAddress();
                        break;
                    case "RoMismatch":
                        result = GetRoAddressMismatch();
                        break;
                    case "IncorporationNumberMissing":
                        result = GetIncorporationNumberMissing();
                        break;
                    case "IncorporationDateMissing":
                        result = GetIncorporationDateMissing();
                        break;
                    case "ErrorDuringSuitAutomation":
                        result = GetSuitExceptions();
                        break;
                    case "LongRunningSuit":
                        result = GetLongRunningSuit();
                        break;
                    case "MissingFromCIMAWebsite":
                        result = GetMissingFromCIMAWebsite();
                        break;
                    case "IncorporationNumberMismatchCoris":
                        result = GetIncorporationNumberMismatchCoris();
                        break;
                    case "EntityNameMismatchCoris":
                        result = GetEntityNameMismatchCoris();
                        break;
                    case "ShareCapitalMismatch":
                        result = GetShareCapitalMismatch();
                        break;
                    case "ShareCapitalCurrencyMismatch":
                        result = GetShareCapitalCurrencyMismatch();
                        break;
                    case "MissingROCoris":
                        result = GetMissingROCoris();
                        break;
                    case "CorisMissingViewpoint":
                        result = GetCorisMissingViewpoint();
                        break;
                    case "StatusMismatchCoris":
                        result = GetStatusMismatchCoris();
                        break;
                    case "TransferOutMismatchCoris":
                        result = GetTransferOutMismatchCoris();
                        break;
                    case "NoWalkersIdentifierCoris":
                        result = GetNoWalkersIdentifierCoris();
                        break;
                    case "ResignationDateStillAppointed":
                        result = GetResignationDateStillAppointed();
                        break;
                    case "MultiplePartnersPerGroup":
                        result = GetMultiplePartnersPerGroup();
                        break;
                    case "MultipleAdminPerSubGroup":
                        result = GetMultipleAdminPerSubGroup();
                        break;
                    case "InsufficientNumberOfDirectors":
                        result = GetInsufficientNumberOfDirectors();
                        break;
                    case "AlertDirectorChange":
                        result = GetAlertDirectorChange();
                        break;
                    case "MatterNumberMissing":
                        result = GetMatterNumberMissing();
                        break;
                    case "EntityAssignedRelationshipManager":
                        AssignRole role = SuitRoleLookup.GetRole(suit);
                        bool changesExist = ProcessAssignmentHistory(role);
                        result = GetEntityAssigned(role);
                        break;
                    case "RelationshipManagerAssignmentRejected":
                        result = GetAssignedRejected(SuitRoleLookup.GetRole(suit));
                        break;
                    case "EntityAssignedCSE":
                        AssignRole roleCse = SuitRoleLookup.GetRole(suit);
                        bool changesExistCse = ProcessAssignmentHistory(roleCse);
                        result = GetEntityAssigned(roleCse);
                        break;
                    case "CseRejected":
                        result = GetAssignedRejected(SuitRoleLookup.GetRole(suit));
                        break;
                    case "EntityAssignedCdd":
                        AssignRole roleCdd = SuitRoleLookup.GetRole(suit);
                        bool changesExistCdd = ProcessAssignmentHistory(roleCdd);
                        result = GetEntityAssigned(roleCdd);
                        break;
                    case "AlertRegistration":
                        result = GetAlertRegistration();
                        break;
                    case "CddRejected":
                        result = GetAssignedRejected(SuitRoleLookup.GetRole(suit));
                        break;
                    case "AnnualReturnsNotFiled":
                        result = GetAnnualReturnsNotFiled();
                        break;
                    case "UnpostedShareTransactions":
                        result = GetUnpostedShareTransactions();
                        break;
                    case "MissingAGMMeetingNotice":
                        result = GetMissingAGMMeetingNotice();
                        break;
                    case "AGMDatesInPast":
                        result = GetAGMDatesInPast();
                        break;
                    case "MultipleSharedBillingContactsPerGroup":
                        result = GetMultipleBillingContactsPerSubGroup();
                        break;
                    case "MultipleGroupPerSharedBillingContacts":
                        result = GetMultipleGroupPerSharedBillingContacts();
                        break;
                    case "CorisBalanceNotUpdated":
                        result = GetCorisBalanceNotUpdated();
                        break;
                    case "CorisBalanceIsTooLow":
                        result = GetCorisBalanceIsTooLow();
                        break;
                    case "SubGroupCodeIncorrect":
                        result = GetSubGroupCodeIncorrect();
                        break;
                    case "IncorrectGroupForIndividual":
                        result = GetIncorrectGroupForIndividual();
                        break;
                    case "BusinessPurposeMissing":
                        result = GetBusinessPurposeMissing();
                        break;
                    case "PartnerNotAssigned":
                        result = GetPartnerNotAssigned();
                        break;
                    case "SetupAdministratorGroup":
                        result = GetSetupAdministratorGroup();
                        break;
                    case "ApplicantForBusinessMissing":
                        result = GetApplicantForBusinessMissing();
                        break;
                    case "ClientContactMissing":
                        result = GetClientContactMissing();
                        break;
                    case "BillingContactMissing":
                        result = GetBillingContactMissing();
                        break;
                    case "SourceOfFundsMissing":
                        result = GetSourceOfFundsMissing();
                        break;
                    case "IncorporationNumberMismatch":
                        result = GetIncorporationNumberMismatch();
                        break;
                    case "BillingFileMissing":
                        result = GetBillingFileMissing();
                        break;
                    case "CDDOfficerMissing":
                        result = GetCDDOfficerMissing();
                        break;
                    case "RefferralOfficeMissing":
                        result = GetReferralOfficeCodeMissing();
                        break;
                    case "AnnualReturnDateMissing":
                        result = GetAnnualReturnsDateMissing();
                        break;
                    case "CSEOfficerMissing":
                        result = GetCSEOfficerMissing();
                        break;
                    case "RelationshipManagerMissing":
                        result = GetRelationshipManagerMissing();
                        break;
                    case "PlaceOfIncorporationMissing":
                        result = GetPlaceOfIncorporationMissing();
                        break;
                    case "DateResignDirectorOfficerMissing":
                        result = GetDateResignDirectorOfficerMissing();
                        break;
                    case "AddressRegisteredMembersMissing":
                        result = GetAddressRegisteredMembersMissing();
                        break;
                    case "ShareholderNotUpdated":
                        result = GetShareholderNotUpdated();
                        break;
                    case "FinancialYearEndMissing":
                        result = GetFinancialYearEndMissing();
                        break;
                    case "MultipleClientContacts":
                        result = GetMultipleClientContact();
                        break;
                    case "MultipleBillingContacts":
                        result = GetMultipleBillingContact();
                        break;
                    case "MissingDealDates":
                        result = GetMissingDealDates();
                        break;
                    case "NonLimitedRecourseMissingContract":
                        result = GetNonLimitedRecourseMissingContract();
                        break;
                    case "DealTypeBlankAdminGroupSPV":
                        result = GetDealTypeBlankAdminGroupSPV();
                        break;
                    case "AdminGroupBlankDealTypeOther":
                        result = GetAdminGroupBlankDealTypeOther();
                        break;
                    case "MissingSection10Document":
                        result = GetMissingSection10Document();
                        break;
                    case "AlertOfficerResigned":
                        result = GetAlertOfficerResigned();
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Can not get violations for suit '{0}'. The suit does not have an associated method in the repository in the Get Violations method of repository", suit.SuitName));
                }
            }
            catch (Exception ex)
            {
                suit.AutomationException = ex;
                ex.LogException("Suit Behavior name: " + suit.EvalBehaviorName);
            }
            return result;
        }



        #endregion

        #region Card Exceptions
        private List<ExceptionViolation> GetNonLimitedRecourseMissingContract()
        {
            var query = from deal in
                            (from d in _ViewpointContext.vw_W360_Deal where d.Indemity != "LR" && d.Indemity.Trim() != "" && d.Indemity != null select d)
                        join cnt in
                            (from v in _ViewpointContext.vw_W360_DocFinancial where v.Category == "CNT" select v) on deal.EntCode equals cnt.EntCode into contract
                        from cnt in contract.DefaultIfEmpty()
                        where cnt == null
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on deal.EntCode equals ent.EntCode
                        select new ExceptionViolation
                        {
                            ReferenceCode = deal.EntCode,
                            ReferenceName = ent.RefName,
                            DefaultUserName = ent.CseOfficerCode,
                            ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                            ReferenceDetail = "Viewpoint Indemity is '" + deal.Indemity
                        };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);
            return results;
        }

        private List<ExceptionViolation> GetNoWalkersIdentifierCoris()
        {
            List<CorisEntityStatusRow> corisList = _db.CorisEntityStatusRows.ToList();

            var query = from coris in _db.CorisEntityStatusRows
                        where string.IsNullOrEmpty(coris.PAR_CLIENT_REF)
                        select new ExceptionViolation
                        {
                            ReferenceCode = coris.PAR_IDENTIFIER,
                            ReferenceName = coris.PAR_NAME,
                            ReferenceObject = ReferenceObjectOptions.CorisFile,
                            ReferenceDetail = "On the Coris row missing the Walkers identifier: PAR_IDENTIFIER is '" + coris.PAR_IDENTIFIER + "' and PAR_NAME is '" + coris.PAR_NAME + "'"
                        };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);
            return results;
        }

        private List<ExceptionViolation> GetEntityNameMismatchCoris()
        {
            List<CorisEntityStatusRow> corisList = _db.CorisEntityStatusRows.ToList();
            List<vw_W360_EntityAdmin> vpEntities = _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.IsCurrent).ToList();

            var query = from ent in vpEntities
                        join coris in corisList on ent.EntCode.Trim().ToUpper() equals coris.PAR_CLIENT_REF.Trim().ToUpper()
                        where coris.PAR_NAME.Trim().ToUpper() != ent.RefName.Trim().ToUpper()
                        select new ExceptionViolation
            {
                ReferenceCode = ent.EntCode,
                ReferenceName = ent.SearchName,
                ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                DefaultUserName = ent.CseOfficerCode,
                ReferenceDetail = "Coris has '" + coris.PAR_NAME + "' but Viewpoint has '" + ent.RefName + "'"
            };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetShareCapitalMismatch()
        {
            List<CompaniesByAgent> corisList = _db.CompaniesByAgent.ToList();
            List<ShareCurrencyMap> currencyMap = _db.ShareCurrencyMaps.ToList();
            //group on EntCode, Type, Currency then sum
            var vpEntities = (from share in _ViewpointContext.vw_W360_ShareCapital
                              join ent in _ViewpointContext.vw_W360_EntityAdmin on share.EntCode equals ent.EntCode
                              where ent.IsCurrent
                              group new { share, ent } by new { share.EntCode, share.Currency, ent.RefName, ent.CseOfficerCode } into cap
                              select new { cap.Key.EntCode, cap.Key.Currency, cap.Key.RefName, cap.Key.CseOfficerCode, AuthorisedCapitalSum = cap.Sum(x => x.share.AuthorisedCapital) }
                              ).ToList();
            var corisTotals = (from file in corisList
                               group file by new { file.SHARE_CODE, file.COMPANY } into gf
                               select new { Company = gf.Key.COMPANY, Currency = gf.Key.SHARE_CODE, ShareAmt = gf.Sum(x => x.ShareAmount) }).ToList();
            var query = from vp in vpEntities
                        join cur in currencyMap on vp.Currency equals cur.ViewpointCurrency
                        join coris in corisTotals on new { Name = vp.RefName.Trim().ToUpper(), Currency = cur.AgentFileCurrency.Trim().ToUpper() }
                                            equals new { Name = coris.Company.Trim().ToUpper(), Currency = coris.Currency.Trim().ToUpper() }
                        where coris.ShareAmt != vp.AuthorisedCapitalSum
                        select new ExceptionViolation
                        {
                            ReferenceCode = vp.EntCode,
                            ReferenceName = vp.RefName,
                            ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                            DefaultUserName = vp.CseOfficerCode,
                            ReferenceDetail = "Coris has '" + coris.Currency + " " + coris.ShareAmt.ToString("N2") + "' but Viewpoint has '" + vp.Currency + " " + vp.AuthorisedCapitalSum.Value.ToString("N2") + "'"
                        };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetShareCapitalCurrencyMismatch()
        {
            List<CompaniesByAgent> corisList = _db.CompaniesByAgent.ToList();
            List<ShareCurrencyMap> currencyMap = _db.ShareCurrencyMaps.ToList();

            //get who is in charge of this entity
            var vpEntities = (from ent in _ViewpointContext.vw_W360_EntityAdmin
                              where ent.IsCurrent
                              select new { ent.EntCode, ent.RefName, ent.CseOfficerCode }
                  ).ToList();


            var query = from coris in corisList
                        join cur in currencyMap on coris.SHARE_CODE equals cur.AgentFileCurrency
                        into gf
                        from cur in gf.DefaultIfEmpty()
                        where cur == null && !string.IsNullOrEmpty(coris.SHARE_CODE)
                        join vp in vpEntities on coris.COMPANY equals vp.RefName
                        group vp by new { EntCode = vp.EntCode, RefName = vp.RefName, CseOfficerCode = vp.CseOfficerCode, Currency = coris.SHARE_CODE } into ent
                        select new ExceptionViolation
                        {
                            ReferenceCode = ent.Key.EntCode,
                            ReferenceName = ent.Key.RefName,
                            ReferenceObject = ReferenceObjectOptions.ViewpointLookup,
                            DefaultUserName = ent.Key.CseOfficerCode,
                            ReferenceDetail = "Share Capital File has unmapped currency code: " + ent.Key.Currency
                        };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }
        private List<ExceptionViolation> GetMissingROCoris()
        {
            string roAddressCode = GetSetting("Address.RegisteredOffice.TypeCode");

            var roAddresses = _db.CorisAgents.Where(o => !string.IsNullOrEmpty(o.RegOfficeAddress)).ToList();

            var vpEntities = from ent in _ViewpointContext.vw_W360_EntityAdmin
                             join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                             join addr in _ViewpointContext.vw_W360_Addresses.Where(x => x.AddrType == roAddressCode) on ent.EntCode equals addr.RefCode
                             where mf.EntityAdmin && ent.IsCurrent && !string.IsNullOrEmpty(addr.AdLine)
                             select new { ent.EntCode, ent.SearchName, ent.CseOfficerCode, addr.AdLine };

            var corisList = _db.CorisEntityStatusRows.ToList();

            var query = from ent in vpEntities.ToList()
                        join addr in roAddresses on ent.AdLine.ToUpper() equals addr.RegOfficeAddress.ToUpper()
                        join coris in corisList on ent.EntCode.ToUpper() equals coris.PAR_CLIENT_REF.ToUpper() into nf
                        from nfc in nf.DefaultIfEmpty()
                        where nfc == null
                        select new ExceptionViolation
            {
                ReferenceCode = ent.EntCode,
                ReferenceName = ent.SearchName,
                ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                DefaultUserName = ent.CseOfficerCode,
                ReferenceDetail = "The entity is no longer on Coris but has a Viewpoint RO address of " + ent.AdLine
            };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetCorisMissingViewpoint()
        {
            List<User> defaultUsers = GetUsersAllowed(Operation.AssignedCorisMissingViewpointCard);
            var agents = _db.CorisAgents.Where(o => !string.IsNullOrEmpty(o.AgentName)).ToList();

            var vpEntities = from ent in _ViewpointContext.vw_W360_EntityAdmin
                             join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                             where mf.EntityAdmin
                             select new { ent.EntCode, ent.SearchName, ent.CseOfficerCode };

            var corisList = _db.CorisEntityStatusRows.ToList();

            var query = from coris in corisList
                        join agent in agents on coris.AGENT_NAME.ToUpper() equals agent.AgentName.ToUpper()
                        join ent in vpEntities.ToList() on coris.PAR_CLIENT_REF.ToUpper() equals ent.EntCode.ToUpper() into me
                        from mec in me.DefaultIfEmpty()
                        where mec == null
                        select new ExceptionViolation
                        {
                            DefaultUsers = defaultUsers,
                            ReferenceCode = coris.PAR_IDENTIFIER,
                            ReferenceName = coris.PAR_NAME,
                            ReferenceObject = ReferenceObjectOptions.CorisFile,
                            ReferenceDetail = "The Coris Entity '" + coris.PAR_NAME + "' has a Walkers identifier of '" + coris.PAR_CLIENT_REF + "' however there is no matching administered entity in Viewpoint"
                        };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetIncorporationNumberMismatchCoris()
        {
            List<CorisEntityStatusRow> corisList = _db.CorisEntityStatusRows.ToList();
            List<vw_W360_EntityAdmin> vpEntities = _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.IsCurrent).ToList();

            var query = from ent in vpEntities
                        join coris in corisList on ent.EntCode.ToUpper() equals coris.PAR_CLIENT_REF.ToUpper()
                        where coris.PAR_FILENO != ent.IncorpNr
                        select new ExceptionViolation
        {
            ReferenceCode = ent.EntCode,
            ReferenceName = ent.SearchName,
            ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
            DefaultUserName = ent.CseOfficerCode,
            ReferenceDetail = "Coris says " + coris.PAR_FILENO + " but Viewpoint says " + (string.IsNullOrEmpty(ent.IncorpNr) ? "no incorporation number provided" : ent.IncorpNr)
        };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetTransferOutMismatchCoris()
        {
            //add names to CorisWalkersName table for all coris names for walkers entities
            List<CorisEntityStatusRow> corisList = _db.CorisEntityStatusRows.ToList();
            List<CorisAgent> agentNames = _db.CorisAgents.Where(n => !string.IsNullOrEmpty(n.AgentName)).ToList();
            List<vw_W360_EntityAdmin> vpEntities = _ViewpointContext.vw_W360_EntityAdmin.ToList();
            string TransferStatusCode = this.GetSetting("Viewpoint.TransferOutStatusCode");
            var query = from ent in vpEntities
                        join coris in corisList on ent.EntCode.ToUpper() equals coris.PAR_CLIENT_REF.ToUpper()
                        join name in agentNames on coris.AGENT_NAME.ToUpper() equals name.AgentName.ToUpper()  //is a walkers entity
                        where ent.EntityStatusCode == TransferStatusCode// "13"  //TBC - Transfer Out
                        select new ExceptionViolation
                        {
                            ReferenceCode = ent.EntCode,
                            ReferenceName = ent.SearchName,
                            ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                            DefaultUserName = ent.CseOfficerCode,
                            ReferenceDetail = "Coris says Agent Name is " + coris.AGENT_NAME + ", but Viewpoint says the status is " + ent.EntityStatus + "."
                        };
            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetStatusMismatchCoris()
        {
            //add statuses that are equivalent to the corisStatusMatches table. Other combinations are considered invalid
            List<CorisEntityStatusRow> corisList = _db.CorisEntityStatusRows.ToList();
            List<CorisStatusMatch> corisStatusMatches = _db.CorisStatusMatches.ToList();
            List<vw_W360_EntityAdmin> vpEntities = _ViewpointContext.vw_W360_EntityAdmin.ToList();

            var query = from ent in vpEntities
                        join coris in corisList on ent.EntCode equals coris.PAR_CLIENT_REF
                        join matches in corisStatusMatches on new { vpStatus = ent.EntityStatusCode, corisStatus = coris.PAR_STATUS } equals new { vpStatus = matches.ViewpointStatusCode, corisStatus = matches.CorisStatus } into gf
                        from sc in gf.DefaultIfEmpty()
                        where sc == null
                        select new ExceptionViolation
                        {
                            ReferenceCode = ent.EntCode,
                            ReferenceName = ent.SearchName,
                            ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                            DefaultUserName = ent.CseOfficerCode,
                            ReferenceDetail = "Coris says " + coris.PAR_STATUS + ", but Viewpoint says " + ent.EntityStatus + "."
                        };
            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetRoAddressMismatch()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            string[] roAddresses = _db.CorisAgents.Where(o => !string.IsNullOrEmpty(o.RegOfficeAddress)).Select(o => o.RegOfficeAddress).ToArray();
            string roAddressCode = GetSetting("Address.RegisteredOffice.TypeCode");


            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        join addr in _ViewpointContext.vw_W360_Addresses.Where(x => x.AddrType == roAddressCode) on ent.EntCode equals addr.RefCode
                        where mf.EntityAdmin == true && ent.IsCurrent && !roAddresses.Contains(addr.AdLine)
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetRoAddressMissing()
        {
            string roAddressCode = GetSetting("Address.RegisteredOffice.TypeCode");

            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        join addr in _ViewpointContext.vw_W360_Addresses.Where(x => x.AddrType == roAddressCode) on ent.EntCode equals addr.RefCode into gj
                        from sc in gj.DefaultIfEmpty()
                        where mf.EntityAdmin == true && ent.IsCurrent && sc == null
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetMultipleAdminPerSubGroup()
        {
            List<ExceptionViolation> results = new List<ExceptionViolation>();

            string individualSubGroupCode = this.GetSetting("IndividualSubGroupCode");

            var subGroups = from ent in _ViewpointContext.vw_W360_EntityAdmin
                            where ent.IsCurrent && !string.IsNullOrEmpty(ent.SubGroupCode) && ent.SubGroupCode != individualSubGroupCode && ent.CseOfficerCode != null && ent.CseOfficerCode != string.Empty
                            group ent by new { ent.SubGroupCode, ent.CseOfficerCode } into admins
                            group admins by admins.Key.SubGroupCode into groups
                            where groups.Count() > 1
                            select groups;

            foreach (var group in subGroups.ToList())
            {
                foreach (var admins in group)
                {
                    foreach (var entity in admins)
                    {
                        results.Add(new ExceptionViolation { ReferenceCode = entity.EntCode, ReferenceName = entity.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = entity.CseOfficerCode, ReferenceDetail = "The sub group, " + group.Key + ", has more than one administrator." });
                    }
                }
            }
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetMultiplePartnersPerGroup()
        {
            string individualMasterGroupCode = this.GetSetting("IndividualMasterGroupCode");
            string individualSubGroupCode = this.GetSetting("IndividualSubGroupCode");

            List<ExceptionViolation> results = new List<ExceptionViolation>();

            //AD: disable the master group exception
            //get the master groups
            //var masterGroup = from ent in _ViewpointContext.vw_W360_EntityAdmin
            //                  where ent.IsCurrent && !string.IsNullOrEmpty(ent.MasterGroupCode) && ent.MasterGroupCode != individualMasterGroupCode && !string.IsNullOrEmpty(ent.PartnerCode)
            //                  group ent by new { ent.MasterGroupCode, ent.PartnerCode } into partners
            //                  group partners by partners.Key.MasterGroupCode into groups
            //                  where groups.Count() > 1
            //                  select groups;

            //foreach (var group in masterGroup.ToList())
            //{
            //    foreach (var partner in group)
            //    {
            //        foreach (var entity in partner)
            //        {
            //            results.Add(new ExceptionViolation { ReferenceCode = entity.EntCode, ReferenceName = entity.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = entity.CseOfficerCode, ReferenceDetail = "The master group, " + group.Key + ", has more than one partner." });
            //        }
            //    }
            //}

            //get the sub groups
            var subGroups = from ent in _ViewpointContext.vw_W360_EntityAdmin
                            where ent.IsCurrent && !string.IsNullOrEmpty(ent.SubGroupCode) && ent.SubGroupCode != individualSubGroupCode && !string.IsNullOrEmpty(ent.PartnerCode)
                            group ent by new { ent.SubGroupCode, ent.PartnerCode } into partners
                            group partners by partners.Key.SubGroupCode into groups
                            where groups.Count() > 1
                            select groups;

            foreach (var group in subGroups.ToList())
            {
                foreach (var partner in group)
                {
                    foreach (var entity in partner)
                    {
                        results.Add(new ExceptionViolation { ReferenceCode = entity.EntCode, ReferenceName = entity.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = entity.CseOfficerCode, ReferenceDetail = "The sub group, " + group.Key + ", has more than one partner." });
                    }
                }
            }
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetResignationDateStillAppointed()
        {
            var query = from dir in _ViewpointContext.vw_W360_StatutoryOfficers.Where(d => d.DateResign <= DateTime.Today && d.Status != "R")
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on dir.EntCode equals ent.EntCode
                        where ent.IsCurrent
                        select new ExceptionViolation { ReferenceCode = dir.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode, ReferenceDetail = dir.OfficerTypeText + " " + dir.OfficerName + " resigned on " + dir.DateResign + " but has a status of '" + dir.Status + "'" };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetLongRunningSuit()
        {
            int maxDuration = int.Parse(GetSetting("Suit.LongRunningSuit.MaxDurationInMinutes"));

            List<User> defaultUsers = GetUsersAllowed(Operation.AssignedLongRunningSuitCard);

            DateTime now = DateTime.Now;

            var badSuits = _db.Suits.Where(s => s.IsRunning && SqlFunctions.DateDiff("mm", s.LastStarted, now) > maxDuration).ToList();

            var results = badSuits.Select(s => new ExceptionViolation
                        {
                            ReferenceCode = s.Id.ToString(),
                            ReferenceName = s.SuitName,
                            ReferenceObject = ReferenceObjectOptions.Suit
                        }).ToList();

            results.ForEach(r => r.DefaultUsers = defaultUsers);

            return results;
        }

        public List<ExceptionViolation> GetSuitExceptions()
        {
            List<User> defaultUsers = GetUsersAllowed(Operation.AssignedSuitExceptionCard);

            var query = from suit in _db.Suits.Local
                        where suit.AutomationException != null
                        select new ExceptionViolation
                        {
                            ReferenceCode = suit.Id.ToString(),
                            ReferenceName = suit.SuitName,
                            ReferenceObject = ReferenceObjectOptions.Suit,
                            ReferenceDetail = suit.AutomationException.ToString()
                        };

            var results = query.ToList();

            results.ForEach(r => r.DefaultUsers = defaultUsers);

            return results;
        }

        private List<ExceptionViolation> GetIncorporationNumberMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            //ignore Trust master file types
            List<string> mfTypesToExclude = GetListSetting("Suit.ExcludeTrusts.MasterFileTypes");

            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        where
                            mf.EntityAdmin
                            && ent.IsCurrent
                            && string.IsNullOrEmpty(ent.IncorpNr)
                            && !mfTypesToExclude.Contains(mf.RefType)
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetIncorporationDateMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            //ignore Trust master file types
            List<string> mfTypesToExclude = GetListSetting("Suit.ExcludeTrusts.MasterFileTypes");

            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        where
                            mf.EntityAdmin
                            && ent.IsCurrent
                            && !mfTypesToExclude.Contains(mf.RefType)
                            && !ent.IncorpDate.HasValue
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetMultipleEmailAddress()
        {
            string individualMasterGroupCode = this.GetSetting("IndividualMasterGroupCode");
            var query = from email in _ViewpointContext.vw_W360_EmailAddresses
                        group email by email.MasterfileCode into g
                        where g.Count() > 1
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on g.Key equals ent.EntCode
                        where ent.MasterGroupCode == individualMasterGroupCode && ent.IsCurrent
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointIndividual, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetMailingMissing()
        {
            //VH: The client contact should have a mailing address. 

            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        join o in _ViewpointContext.vw_W360_StatutoryOfficers.Where(x => x.OfficerType == "E26" && x.Status == "A") on ent.EntCode equals o.EntCode
                        join addr in _ViewpointContext.vw_W360_Addresses.Where(x => x.AddrType == "MA") on o.AddrCode equals addr.RefCode into gj
                        from sc in gj.DefaultIfEmpty()
                        where mf.EntityAdmin == true && ent.IsCurrent && sc == null
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, ReferenceDetail = "The client contact, " + o.OfficerName + ", is missing a mailing address", DefaultUserName = ent.CseOfficerCode };


            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetInGroupNoSubGroup()
        {
            //VH: It has been requested that this exception card is assigned at the relationship manager level rather than the CSE level
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        where mf.EntityAdmin == true && ent.IsCurrent && mf.MasterGroup != null && (ent.SubGroupCode == null || ent.SubGroupCode == string.Empty)
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.RelationshipMgrCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetInSubGroupNoMasterGroup()
        {
            //VH: It has been requested that this exception card is assigned at the relationship manager level rather than the CSE level
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        where mf.EntityAdmin == true && ent.IsCurrent && mf.MasterGroup == null && ent.SubGroupCode != null && ent.SubGroupCode != string.Empty
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.RelationshipMgrCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetMissingBillingEmailAddresses()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        join email in _ViewpointContext.vw_W360_EmailAddresses.Where(x => x.cType == "EI") on ent.EntCode equals email.MasterfileCode into gj
                        from sc in gj.DefaultIfEmpty()
                        where mf.EntityAdmin == true && ent.IsCurrent && sc == null
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        public List<ExceptionViolation> GetOfficerMissingDate()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from dir in _ViewpointContext.vw_W360_StatutoryOfficers.Where(d => d.DateAppoint == null && d.Status == "A")
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on dir.EntCode equals ent.EntCode
                        where ent.IsCurrent
                        select new ExceptionViolation { ReferenceCode = dir.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode, ReferenceDetail = dir.OfficerTypeText + ": " + dir.OfficerName };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetInvalidEmailAddresses()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            var badEmails = _ViewpointContext.vw_W360_EmailAddresses.ToList().Where(e => !IsEmailAddressValid(e.EmailAddress));

            var query = from email in badEmails
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on email.MasterfileCode equals ent.EntCode
                        where ent.IsCurrent
                        select new ExceptionViolation { ReferenceCode = email.MasterfileCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode, ReferenceDetail = "Invalid email: " + email.EmailAddress };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);
            return results;
        }

        private List<ExceptionViolation> GetMissingFromCIMAWebsite()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join reg in _ViewpointContext.vw_W360_Registrations on ent.EntCode equals reg.EntCode
                        join regType in _ViewpointContext.vw_W360_Registration_Types on reg.BusinessTypeCode equals regType.ElCode
                        where ent.IsCurrent && reg.IsActive && regType.Authority == "CIMA"
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode, ReferenceDetail = "Searched the CIMA website for '" + ent.SearchName + "' which has an active registration type of '" + reg.BusinessType + "'" };

            //find the entities that don't show up on the website
            List<ExceptionViolation> results = query.ToList().Where(e => !IsFoundOnCIMAWebsite(e.ReferenceName)).ToList();

            GetViewpointUser(ref results);
            return results;
        }

        private List<ExceptionViolation> GetInsufficientNumberOfDirectors()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            //get the list of registration types for the exception
            List<string> businessTypeCodes = GetListSetting("Suit.InsufficientNumberOfDirectors.BusinessTypeCodes");

            //return all administered entities that have active CIMA registrations and less than two active directors
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        let dCount =
                        (
                            from director in _ViewpointContext.vw_W360_StatutoryOfficers
                            where ent.EntCode == director.EntCode && director.OfficerType == "DIR" && director.Status == "A"
                            select director
                        ).Count()
                        let rCount =
                        (
                            from reg in _ViewpointContext.vw_W360_Registrations
                            where ent.EntCode == reg.EntCode && reg.IsActive && businessTypeCodes.Contains(reg.BusinessTypeCode)
                            select reg
                        ).Count()
                        where ent.IsCurrent && rCount > 0 && dCount < 2
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode, ReferenceDetail = "The client entity currently has " + dCount.ToString() + " appointed director(s)" };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);
            return results;
        }

        private List<ExceptionViolation> GetAnnualReturnsNotFiled()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        where mf.EntityAdmin && ent.IsCurrent && ent.EntityStatus.Contains("Active") && ent.NextAnnualReturn <= DateTime.Today
                        select ent;

            List<ExceptionViolation> results = query.ToList().Select(ent => new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode, ReferenceDetail = "The next annual return was due on " + ent.NextAnnualReturn.Value.ToString("dd MMMM yyyy") }).ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetAnnualReturnsDateMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            //ignore Trust master file types
            List<string> mfTypesToExclude = GetListSetting("Suit.ExcludeTrusts.MasterFileTypes");

            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        where
                            mf.EntityAdmin == true
                            && ent.IsCurrent
                            && !mfTypesToExclude.Contains(mf.RefType)
                            && !ent.NextAnnualReturn.HasValue
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetMatterNumberMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            DateTime startDate = DateTime.Parse(GetSetting("Date.StartOfBusiness"));
            string roAddressCode = GetSetting("Address.RegisteredOffice.TypeCode");
            string[] roAddresses = _db.CorisAgents.Where(o => !string.IsNullOrEmpty(o.RegOfficeAddress)).Select(o => o.RegOfficeAddress).ToArray();

            var query = from mf in _ViewpointContext.vw_W360_MasterFileSetup
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                        join addr in _ViewpointContext.vw_W360_Addresses on ent.EntCode equals addr.RefCode
                        where
                            mf.EntityAdmin &&
                            ent.IsCurrent &&
                            string.IsNullOrEmpty(mf.MatterNumber) &&
                            !string.IsNullOrEmpty(ent.ReferralOffice) &&
                            addr.AddrType == roAddressCode &&
                            roAddresses.Contains(addr.AdLine) &&
                            ent.IncorpDate >= startDate
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);
            return results;
        }

        private List<ExceptionViolation> GetUnpostedShareTransactions()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            var query = from mf in _ViewpointContext.vw_W360_MasterFileSetup
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                        let tCount =
                        (
                            from tran in _ViewpointContext.vw_W360_ShareTransactions
                            where ent.EntCode == tran.EntCode && !tran.Posted
                            select tran
                        ).Count()
                        where mf.EntityAdmin && ent.IsCurrent && tCount > 0
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode, ReferenceDetail = "The entity has " + tCount.ToString() + " un-posted transaction(s)" };

            List<ExceptionViolation> results = query.ToList();

            GetViewpointUser(ref results);
            return results;
        }

        private List<ExceptionViolation> GetMissingAGMMeetingNotice()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            var query = from mf in _ViewpointContext.vw_W360_MasterFileSetup
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                        join agm in _ViewpointContext.vw_W360_Entity_AGM on mf.RefCode equals agm.EntCode
                        where mf.EntityAdmin && ent.IsCurrent && ent.EntityTypeCode == "CL" && (agm.DateLastAGM == null || agm.DateNextAGM == null || agm.DateDueAGM == null || agm.MA_AgmNotice == null || agm.MA_EgmNotice == null)
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();

            GetViewpointUser(ref results);
            return results;
        }
        private List<ExceptionViolation> GetAlertDirectorChange()
        {
            var auditHistories = this._db.AuditHistories.Where(a => a.AuditConfigId == 2 && a.NewValue == "A" && a.IsCurrent).ToList();
            var VpEntities = _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.IsCurrent).ToList();

            var directors = from so in _ViewpointContext.vw_W360_StatutoryOfficers
                            join mf in _ViewpointContext.vw_W360_MasterFileSetup on so.AddrCode equals mf.RefCode
                            join deal in _ViewpointContext.vw_W360_Deal on so.EntCode equals deal.EntCode
                            //SR: only raise the alert for currently active directors that have a walkersidentifier (else we can't assign a card)
                            where so.OfficerType == "DIR" && so.Status == "A" && !string.IsNullOrEmpty(mf.WalkersIdentifier)
                            select new { entCode = so.EntCode, id = so.id, defaultUserName = mf.WalkersIdentifier };

            var query = from audit in auditHistories
                        join dir in directors on audit.ReferenceFieldValue equals dir.id
                        join ent in VpEntities on dir.entCode equals ent.EntCode
                        select new ExceptionViolation
                       {
                           ReferenceCode = ent.EntCode,
                           ReferenceName = ent.SearchName,
                           ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                           AdditionalReferences = new List<CardReference> { new CardReference { ReferenceCode = audit.id.ToString(), ReferenceObject = ReferenceObjectOptions.AuditHistory } },
                           DefaultUserName = dir.defaultUserName
                       };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);
            return results;
        }
        private List<ExceptionViolation> GetAlertRegistration()
        {
            List<string> excludedTypes = this.GetListSetting("Suit.AlertRegistrationExcludedTypes");            //the types of registrations that are excluded from this alert
            List<User> defaultUsers = GetUsersAllowed(Operation.AssignedAlertRegistrationCard);                 //the group of people to assign the card
            var registrations = _ViewpointContext.vw_W360_Registrations.ToList();                               //the registrations --so we can see what registration type it is
            var auditHistories = this._db.AuditHistories.Where(a => a.AuditConfigId == 1 && a.IsCurrent).ToList();  //the current entries in the audit log looking for changes for added registrations
            var VpEntities = _ViewpointContext.vw_W360_EntityAdmin.ToList();                                    //the entity so we know the name of the entity

            var query = from audit in auditHistories
                        join ent in VpEntities on audit.ReferenceFieldValue.Substring(0, audit.ReferenceFieldValue.IndexOf("-")) equals ent.EntCode
                        join reg in registrations on audit.ReferenceFieldValue equals reg.Id
                        where !excludedTypes.Contains(reg.BusinessTypeCode)
                        select new ExceptionViolation
                        {
                            ReferenceCode = ent.EntCode,
                            ReferenceName = ent.SearchName,
                            ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                            AdditionalReferences = new List<CardReference> { new CardReference { ReferenceCode = audit.id.ToString(), ReferenceObject = ReferenceObjectOptions.AuditHistory } }
                        };


            List<ExceptionViolation> results = query.ToList();
            results.ForEach(r => r.DefaultUsers = defaultUsers);

            return results;
        }
        private List<ExceptionViolation> GetAGMDatesInPast()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            var query = from mf in _ViewpointContext.vw_W360_MasterFileSetup
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                        join agm in _ViewpointContext.vw_W360_Entity_AGM on mf.RefCode equals agm.EntCode
                        where mf.EntityAdmin && ent.IsCurrent && ent.EntityTypeCode == "CL" && (agm.DateNextAGM < DateTime.Today || agm.DateDueAGM < DateTime.Today)
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode, ReferenceDetail = "The next AGM date is " + agm.DateNextAGM.Value.ToString() + " and the date due is " + agm.DateDueAGM.Value.ToString() };

            List<ExceptionViolation> results = query.ToList();

            GetViewpointUser(ref results);
            return results;
        }

        private List<ExceptionViolation> GetMultipleBillingContactsPerSubGroup()
        {
            List<ExceptionViolation> results = new List<ExceptionViolation>();

            string IndividualSubGroupCode = GetSetting("IndividualSubGroupCode");

            //get the sub groups
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.IsCurrent && !string.IsNullOrEmpty(e.MasterGroupCode) && !string.IsNullOrEmpty(e.SubGroupCode) && e.SubGroupCode != IndividualSubGroupCode)
                        join bc in _ViewpointContext.vw_W360_StatutoryOfficers.Where(o => o.OfficerType == "E27" && o.Status == "A") on ent.EntCode equals bc.EntCode
                        group ent by new { SubGroupCode = ent.SubGroupCode, BillingContactCode = bc.AddrCode } into sgc
                        group sgc by new { sgc.Key.SubGroupCode } into groups
                        where groups.Count() > 1
                        select groups;


            foreach (var group in query)
            {
                foreach (var sg in group)
                {
                    foreach (var entity in sg)
                    {
                        //if one entity has multiple billing contacts, the above will generate a duplicate for that entity.
                        //do a check first to ensure it doesn't already exist
                        if (!results.Any(e => e.ReferenceCode == entity.EntCode))
                            results.Add(new ExceptionViolation { ReferenceCode = entity.EntCode, ReferenceName = entity.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = entity.CseOfficerCode, ReferenceDetail = "The sub group, " + group.Key.SubGroupCode + ", has more than one billing contact." });
                    }
                }
            }

            GetViewpointUser(ref results);

            return results.Distinct().ToList();
        }

        private List<ExceptionViolation> GetMultipleGroupPerSharedBillingContacts()
        {
            List<ExceptionViolation> results = new List<ExceptionViolation>();

            string IndividualMasterGroupCode = GetSetting("IndividualMasterGroupCode");

            //get the master groups
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.IsCurrent && !string.IsNullOrEmpty(e.MasterGroupCode) && e.MasterGroupCode != IndividualMasterGroupCode)
                        join bc in _ViewpointContext.vw_W360_StatutoryOfficers.Where(o => o.OfficerType == "E27" && o.Status == "A") on ent.EntCode equals bc.EntCode
                        group ent by new { bcCode = bc.AddrCode, bcName = bc.OfficerName, mgCode = ent.MasterGroupCode } into bcg
                        group bcg by new { bcg.Key.bcCode, bcg.Key.bcName } into groups
                        where groups.Count() > 1
                        select groups;

            foreach (var bc in query)
            {
                foreach (var mg in bc)
                {
                    foreach (var entity in mg)
                    {
                        results.Add(new ExceptionViolation
                        {
                            ReferenceCode = entity.EntCode,
                            ReferenceName = entity.SearchName,
                            ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                            DefaultUserName = entity.CseOfficerCode,
                            ReferenceDetail = "The billing contact, " + bc.Key.bcName + ", is appointed to entities in multiple master groups."
                        });
                    }
                }
            }

            GetViewpointUser(ref results);
            return results;
        }

        private List<ExceptionViolation> GetCorisBalanceNotUpdated()
        {
            List<ExceptionViolation> results = new List<ExceptionViolation>();

            results.AddRange(GetCorisBalanceNotUpdatedInternal(
                CorisBalanceType.WPS,
                GetUsersAllowed(Operation.AssignedCorisBalanceNotUpdatedCard),
                int.Parse(GetSetting("Suit.CorisBalance.UpdateIntervalInHours"))
                ));

            results.AddRange(GetCorisBalanceNotUpdatedInternal(
                CorisBalanceType.LawFirm,
                GetUsersAllowed(Operation.AssignedCorisBalanceLawFirmNotUpdatedCard),
                int.Parse(GetSetting("Suit.CorisBalance.LawFirmUpdateIntervalInHours"))
                ));

            return results;
        }

        private List<ExceptionViolation> GetCorisBalanceNotUpdatedInternal(CorisBalanceType type, List<User> defaultUsers, int intervalInHours)
        {
            List<ExceptionViolation> results = new List<ExceptionViolation>();

            CorisBalance lastCheck = GetCorisBalance(type);

            if (lastCheck == null || GetWorkingHours(lastCheck.Created, DateTimeOffset.Now) > intervalInHours)
            {
                results.Add(new ExceptionViolation
                {
                    ReferenceCode = type.ToString(),
                    ReferenceName = "Coris Balance",
                    ReferenceObject = ReferenceObjectOptions.CorisBalance,
                    DefaultUsers = defaultUsers,
                    ReferenceDetail = "The Coris balance for " + type.ToString() + " has not been updated within the last " + intervalInHours.ToString() + "h."
                });
            }

            return results;
        }

        private List<ExceptionViolation> GetCorisBalanceIsTooLow()
        {
            List<ExceptionViolation> results = new List<ExceptionViolation>();

            results.AddRange(GetCorisBalanceIsTooLowInternal(
                CorisBalanceType.WPS,
                GetUsersAllowed(Operation.AssignedCorisBalanceTooLowCard),
                double.Parse(GetSetting("Suit.CorisBalance.Minimum"))
            ));

            results.AddRange(GetCorisBalanceIsTooLowInternal(
                CorisBalanceType.LawFirm,
                GetUsersAllowed(Operation.AssignedCorisBalanceLawFirmTooLowCard),
                double.Parse(GetSetting("Suit.CorisBalance.LawFirmMinimum"))
            ));

            return results;
        }

        private List<ExceptionViolation> GetCorisBalanceIsTooLowInternal(CorisBalanceType type, List<User> defaultUsers, double minimum)
        {
            List<ExceptionViolation> results = new List<ExceptionViolation>();

            CorisBalance lastCheck = GetCorisBalance(type);

            if (lastCheck == null || lastCheck.Balance < minimum)
            {
                results.Add(new ExceptionViolation
                {
                    ReferenceCode = type.ToString(),
                    ReferenceName = "Coris Balance",
                    ReferenceObject = ReferenceObjectOptions.CorisBalance,
                    DefaultUsers = defaultUsers,
                    ReferenceDetail = "The Coris balance for " + type.ToString() + " is less than " + string.Format("{0:$#,##0}", minimum) + "."
                });
            }

            return results;
        }

        private List<ExceptionViolation> GetSubGroupCodeIncorrect()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            const int NUMBER_OF_PREFIX_CHARS = 2;
            const int NUMBER_OF_SUFFIX_CHARS = 3;

            var query = from mf in _ViewpointContext.vw_W360_MasterFileSetup
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                        let mgClean = mf.MasterGroup.Replace(" ", "").ToUpper()
                        let mgPrefix = mgClean.Length <= (NUMBER_OF_PREFIX_CHARS + NUMBER_OF_SUFFIX_CHARS) ? mgClean : string.Concat(mgClean.Substring(0, NUMBER_OF_PREFIX_CHARS), mgClean.Substring(mgClean.Length - NUMBER_OF_SUFFIX_CHARS, NUMBER_OF_SUFFIX_CHARS))
                        let sgPrefix = ent.SubGroupCode.Substring(0, (NUMBER_OF_PREFIX_CHARS + NUMBER_OF_SUFFIX_CHARS)).ToUpper()
                        where mf.EntityAdmin && ent.IsCurrent && !string.IsNullOrEmpty(mf.MasterGroup) && !string.IsNullOrEmpty(ent.SubGroupCode) && mgPrefix != sgPrefix
                        select new ExceptionViolation
                        {
                            ReferenceCode = ent.EntCode,
                            ReferenceName = ent.SearchName,
                            ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                            DefaultUserName = ent.CseOfficerCode,
                            ReferenceDetail = "For this entity, the master group name is '" + mf.MasterGroup + "' and the sub group code is '" + ent.SubGroupCode + "'. The sub group code should begin with '" + mgPrefix + "'."
                        };

            List<ExceptionViolation> results = query.ToList();

            GetViewpointUser(ref results);
            return results;
        }

        private List<ExceptionViolation> GetIncorrectGroupForIndividual()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            string individualMasterGroupCode = GetSetting("IndividualMasterGroupCode");
            string individualSubGroupCode = GetSetting("IndividualSubGroupCode");

            var query = from mf in _ViewpointContext.vw_W360_MasterFileSetup
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                        where mf.EntityAdmin && ent.IsCurrent && ((ent.MasterGroupCode == individualMasterGroupCode && ent.SubGroupCode != individualSubGroupCode) || (ent.MasterGroupCode != individualMasterGroupCode && ent.SubGroupCode == individualSubGroupCode))
                        select new ExceptionViolation
                        {
                            ReferenceCode = ent.EntCode,
                            ReferenceName = ent.SearchName,
                            ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                            DefaultUserName = ent.CseOfficerCode,
                            ReferenceDetail = "For this entity, the master group code is '" + ent.MasterGroupCode + "' and the sub group code is '" + ent.SubGroupCode + "'."
                        };

            List<ExceptionViolation> results = query.ToList();

            GetViewpointUser(ref results);
            return results;
        }

        private List<ExceptionViolation> GetBusinessPurposeMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from cdd in _ViewpointContext.vw_W360_CDD
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on cdd.RefCode equals mf.RefCode
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                        where mf.EntityAdmin && ent.IsCurrent && (cdd.BusinessPurpose == null || (cdd.BusinessPurpose.Trim().Length - cdd.BusinessPurpose.Trim().Replace(" ", string.Empty).Length < 3)) //the business purpose must be at least 3 words - do this by counting spaces.
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetPartnerNotAssigned()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from mf in _ViewpointContext.vw_W360_MasterFileSetup
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                        where mf.EntityAdmin && ent.IsCurrent && string.IsNullOrEmpty(ent.PartnerCode)
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetSetupAdministratorGroup()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from mf in _ViewpointContext.vw_W360_MasterFileSetup
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                        where mf.EntityAdmin && ent.IsCurrent && mf.AdminGroup == "SETUP"
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetApplicantForBusinessMissing()
        {
            return GetMissingRelationships("K09");
        }

        private List<ExceptionViolation> GetClientContactMissing()
        {
            return GetMissingRelationships("E26");
        }

        private List<ExceptionViolation> GetBillingContactMissing()
        {
            return GetMissingRelationships("E27");
        }

        private List<ExceptionViolation> GetMultipleClientContact()
        {
            return GetMultipleRelationships("E26");
        }

        private List<ExceptionViolation> GetMultipleBillingContact()
        {
            return GetMultipleRelationships("E27");
        }

        private List<ExceptionViolation> GetMissingRelationships(string officerType)
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        join officers in _ViewpointContext.vw_W360_StatutoryOfficers.Where(x => x.OfficerType == officerType && x.Status == "A") on ent.EntCode equals officers.EntCode into gj
                        from sc in gj.DefaultIfEmpty()
                        where mf.EntityAdmin == true && ent.IsCurrent && sc == null
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetMultipleRelationships(string officerType)
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        let oCount = (
                            from o in _ViewpointContext.vw_W360_StatutoryOfficers
                            where ent.EntCode == o.EntCode && o.OfficerType == officerType && o.Status == "A"
                            select o
                        ).Count()
                        where mf.EntityAdmin == true && ent.IsCurrent && oCount > 1
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetSourceOfFundsMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            List<string> badWords = GetListSetting("Suit.SourceOfFundsMissing.BadWords");

            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        join cdd in _ViewpointContext.vw_W360_CDD on ent.EntCode equals cdd.RefCode
                        where mf.EntityAdmin == true && ent.IsCurrent && (string.IsNullOrEmpty(cdd.SourceofFunds) || badWords.Contains(cdd.SourceofFunds))
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetIncorporationNumberMismatch()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            //ignore Trust master file types
            List<string> mfTypesToExclude = GetListSetting("Suit.ExcludeTrusts.MasterFileTypes");

            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        join comp in _ViewpointContext.vw_W360_Compliance on ent.EntCode equals comp.AddrCode
                        where mf.EntityAdmin == true
                            && ent.IsCurrent
                            && !mfTypesToExclude.Contains(mf.RefType)
                            && (ent.IncorpNr ?? string.Empty).Trim() != (comp.IncorporationNo ?? string.Empty).Trim()
                        select new ExceptionViolation
                        {
                            ReferenceCode = ent.EntCode,
                            ReferenceName = ent.SearchName,
                            ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                            ReferenceDetail = "The statutory - location has '" + (ent.IncorpNr ?? string.Empty) + "' but compliance has '" + (comp.IncorporationNo ?? string.Empty) + "'",
                            DefaultUserName = ent.CseOfficerCode
                        };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetBillingFileMissing()
        {
            List<User> defaultUsers = GetUsersAllowed(Operation.AssignedBillingFileMissingCard);

            _ViewpointContext.Database.Log = Console.WriteLine;

            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        join bf in _ViewpointContext.vw_W360_BillingFile on ent.EntCode equals bf.AddrCode into oj
                        from ebf in oj.DefaultIfEmpty()
                        where mf.EntityAdmin == true && ent.IsCurrent && ebf == null
                        select new ExceptionViolation
                        {
                            ReferenceCode = ent.EntCode,
                            ReferenceName = ent.SearchName,
                            ReferenceObject = ReferenceObjectOptions.ViewpointEntity
                        };

            List<ExceptionViolation> results = query.ToList();

            results.ForEach(r => r.DefaultUsers = defaultUsers);

            return results;
        }

        private List<ExceptionViolation> GetCDDOfficerMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        join cdd in _ViewpointContext.vw_W360_CDD on ent.EntCode equals cdd.RefCode
                        where mf.EntityAdmin == true && ent.IsCurrent && string.IsNullOrEmpty(cdd.CDDOfficer)
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetReferralOfficeCodeMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        where mf.EntityAdmin == true && ent.IsCurrent && string.IsNullOrEmpty(ent.ReferralOffice)
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetCSEOfficerMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        where mf.EntityAdmin == true && ent.IsCurrent && string.IsNullOrEmpty(ent.CseOfficer)
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetRelationshipManagerMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        where mf.EntityAdmin == true && ent.IsCurrent && string.IsNullOrEmpty(ent.RelationshipMgr)
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetPlaceOfIncorporationMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            //ignore Trust master file types
            List<string> mfTypesToExclude = GetListSetting("Suit.ExcludeTrusts.MasterFileTypes");

            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        join comp in _ViewpointContext.vw_W360_Compliance on ent.EntCode equals comp.AddrCode
                        where
                            mf.EntityAdmin == true
                            && ent.IsCurrent
                            && string.IsNullOrEmpty(comp.PlaceOfIncorporation)
                            && !mfTypesToExclude.Contains(mf.RefType)
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetDateResignDirectorOfficerMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        join stat in _ViewpointContext.vw_W360_StatutoryOfficers on ent.EntCode equals stat.EntCode
                        where mf.EntityAdmin == true && ent.IsCurrent && !stat.DateResign.HasValue && (stat.OfficerType == "DIR" || stat.OfficerType == "OFR") && stat.Status == "R"
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetAddressRegisteredMembersMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        let addrCount = (
                            from ad in _ViewpointContext.vw_W360_Addresses
                            where ad.RefCode == ent.EntCode && ad.AddrType == "SM"
                            select ad
                        ).Count()
                        where mf.EntityAdmin == true && ent.IsCurrent && ent.EntityTypeCode == "CE" && addrCount == 0
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetShareholderNotUpdated()
        {
            List<string> shareholderAddrCodes = GetListSetting("Suit.ShareholderNotUpdated.AddrCodes");
            int updateInterval = GetIntSetting("Suit.ShareholderNotUpdated.UpdateIntervalInDays");

            DateTime cutoff = DateTime.Now.AddDays(-1 * updateInterval);

            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        join sc in _ViewpointContext.vw_W360_ShareCertificates on ent.EntCode equals sc.EntCode
                        where mf.EntityAdmin == true && ent.IsCurrent && shareholderAddrCodes.Contains(sc.AddrCode) && sc.BalanceShare > 0 && sc.IssueDate <= cutoff
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetFinancialYearEndMissing()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            //get the list of registration types for the exception
            List<string> businessTypeCodes = GetListSetting("Suit.FinancialYearEndMissing.BusinessTypeCodes");

            //return all administered entities that have active CIMA registrations and no financial year end
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        let rCount =
                        (
                            from reg in _ViewpointContext.vw_W360_Registrations
                            where ent.EntCode == reg.EntCode && reg.IsActive && businessTypeCodes.Contains(reg.BusinessTypeCode)
                            select reg
                        ).Count()
                        where ent.IsCurrent && rCount > 0 && ent.FinancialYearEnd == null
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);
            return results;
        }

        private List<ExceptionViolation> GetDealTypeBlankAdminGroupSPV()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        where mf.EntityAdmin == true && ent.IsCurrent && ((string.IsNullOrEmpty(ent.DealTypeCode) || ent.DealTypeCode == "OTH") && (mf.AdminGroup == "SPV"))
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetAdminGroupBlankDealTypeOther()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        where mf.EntityAdmin == true && ent.IsCurrent && ((mf.AdminGroup != "SPV") && (!string.IsNullOrEmpty(ent.DealTypeCode) && ent.DealTypeCode != "OTH"))
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetMissingSection10Document()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            List<string> entityTypes = GetListSetting("Suit.MissingSection10Document.EntityTypes");
            int delayInDays = GetIntSetting("Suit.MissingSection10Document.DelayInDays");

            DateTime minFormationDate = DateTime.Now.AddDays(-1 * delayInDays);

            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                        let secTens = (
                                from file in _ViewpointContext.vw_W360_DocIndex
                                where file.EntCode == ent.EntCode && file.FolderKey == "REGDOC" && file.Descr.Contains("Section 10")
                                select file
                            ).Count()
                        where mf.EntityAdmin == true && ent.IsCurrent && entityTypes.Contains(ent.EntityTypeCode) && ent.IncorpDate.HasValue && ent.IncorpDate.Value < minFormationDate && secTens == 0
                        select new ExceptionViolation { ReferenceCode = ent.EntCode, ReferenceName = ent.SearchName, ReferenceObject = ReferenceObjectOptions.ViewpointEntity, DefaultUserName = ent.CseOfficerCode };

            List<ExceptionViolation> results = query.ToList();
            GetViewpointUser(ref results);

            return results;
        }

        private List<ExceptionViolation> GetAlertOfficerResigned()
        {
            _ViewpointContext.Database.Log = Console.WriteLine;

            List<ExceptionViolation> result = new List<ExceptionViolation>();
            List<string> officerTypes = GetListSetting("Suit.AlertOfficerResigned.OfficerTypes");

            //go to the audit log to find the resigned officer
            var auditHistories = this._db.AuditHistories
                                .Where(a => a.AuditConfigId == 2 && a.NewValue == "R" && a.IsCurrent)
                                .ToList();

            //get a list of all officers and the associated entity
            var officerEntity = from o in _ViewpointContext.vw_W360_StatutoryOfficers
                                join mf in _ViewpointContext.vw_W360_MasterFileSetup on o.EntCode equals mf.RefCode
                                join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                                where
                                    mf.EntityAdmin == true &&
                                    ent.IsCurrent &&
                                    officerTypes.Contains(o.OfficerType)
                                select new { o, ent };

            var resignedOfficers = from a in auditHistories
                                   join oe in officerEntity on a.ReferenceFieldValue equals oe.o.id
                                   select new { a, oe };

            foreach (var resigned in resignedOfficers)
            {
                var stillActive = from o in officerEntity
                                  where
                                      o.o.Status == "A" && //appointment is still active
                                      o.o.AddrCode == resigned.oe.o.AddrCode && //same officer
                                      o.o.OfficerType == resigned.oe.o.OfficerType && //same officer type
                                      o.ent.MasterGroupCode == resigned.oe.ent.MasterGroupCode && //same master group
                                      o.ent.IsCurrent
                                  select o.ent;

                //see if there are any other active appointments
                //ensure that they haven't been reappointed to the same entity
                if (stillActive.Count() > 0 && !stillActive.Select(ent => ent.EntCode).Contains(resigned.oe.ent.EntCode))
                {
                    result.Add(
                            new ExceptionViolation
                            {
                                ReferenceCode = resigned.oe.ent.EntCode,
                                ReferenceName = resigned.oe.ent.SearchName,
                                ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                                AdditionalReferences = new List<CardReference> { new CardReference { ReferenceCode = resigned.a.id.ToString(), ReferenceObject = ReferenceObjectOptions.AuditHistory } },
                                DefaultUserName = resigned.oe.ent.CseOfficerCode,
                                ReferenceDetail = string.Concat("<b>", resigned.oe.o.OfficerName, " is still appointed as ", resigned.oe.o.OfficerTypeText, " to the following entities:</b><ul>", string.Concat(stillActive.ToList().Select(s => string.Concat("<li>", s.SearchName, "</li>")).OrderBy(s => s).ToArray()), "</ul>")
                            });
                }
            }

            GetViewpointUser(ref result);
            return result;
        }

        public CorisBalance GetCorisBalance(CorisBalanceType type)
        {
            return _db.CorisBalances.Where(o => o.Type == type).OrderByDescending(o => o.Created).FirstOrDefault();
        }

        public void SetCorisBalance(CorisBalanceType type, double balance, string createdBy)
        {
            _db.CorisBalances.Add(new CorisBalance { Type = type, Balance = balance, CreatedBy = createdBy, Created = DateTimeOffset.Now });
            _db.SaveChanges();
        }

        public List<Card> GetCorisBalanceCards(CorisBalanceType type)
        {
            return _db.Cards
                .Include(c => c.CardReferences)
                .Where(c => c.Status == CardStatusOptions.Open && c.CardReferences.Any(r => r.ReferenceObjectString == ReferenceObjectOptions.CorisBalance.ToString() && r.ReferenceCode == type.ToString()))
                .ToList();
        }

        /// <summary>
        /// Returns the number of hours between two dates, excluding weekends.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        private int GetWorkingHours(DateTimeOffset from, DateTimeOffset to)
        {
            var hourDifference = (int)to.Subtract(from).TotalHours;
            return Enumerable
                .Range(1, hourDifference)
                .Select(x => from.AddHours(x))
                .Count(x => x.DayOfWeek != DayOfWeek.Saturday && x.DayOfWeek != DayOfWeek.Sunday);
        }

        /// <summary>
        /// Determines whether the specified value matches the pattern of a valid email address.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private bool IsEmailAddressValid(string email)
        {
            if (!string.IsNullOrEmpty(email))
                email = email.Trim();

            var attr = new System.ComponentModel.DataAnnotations.EmailAddressAttribute();
            return attr.IsValid(email);
        }

        /// <summary>
        /// Check to see if the name returns any results from the CIMA 'Search for entity' page.
        /// NOTE: This method seems to fail on the desktop due to proxy issues however it should work on the server.
        /// </summary>
        /// <param name="name">Name of the entity to search for</param>
        /// <returns>True if the search page returned any entities. False otherwise.</returns>
        private bool IsFoundOnCIMAWebsite(string name)
        {
            //search settings
            string category = GetSetting("Suit.MissingFromCIMAWebsite.Category");
            string URL = GetSetting("Suit.MissingFromCIMAWebsite.Url");

            //form constants
            string FORMVIEWSTATE = "__VIEWSTATE";
            string FORMEVENTVALIDATION = "__EVENTVALIDATION";
            string FORMSEARCHVALUE = "ctl00$MainContent$CtSearchAuthorizations1$txtSearchValue";
            string FORMSEARCHCATEGORY = "ctl00$MainContent$CtSearchAuthorizations1$ddlSearchCategory";

            //result element
            string PAGERESULTSELEMENT = "ctl00_MainContent_CtSearchAuthorizations1_gvSearchResults";

            byte[] response;
            string responseString;

            using (WebClient webClient = new WebClient())
            {
                //Request the page a first time to get the form variables
                response = webClient.DownloadData(URL);

                System.Collections.Specialized.NameValueCollection reqparm = new System.Collections.Specialized.NameValueCollection();
                reqparm.Add(FORMVIEWSTATE, ExtractFormData(Encoding.ASCII.GetString(response), FORMVIEWSTATE, "value=\""));
                reqparm.Add(FORMEVENTVALIDATION, ExtractFormData(Encoding.ASCII.GetString(response), FORMEVENTVALIDATION, "value=\""));
                reqparm.Add(FORMSEARCHVALUE, name);
                reqparm.Add(FORMSEARCHCATEGORY, category);

                //Request the page a second time but this time supply the search paramters
                webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                response = webClient.UploadValues(URL, "POST", reqparm);
            }

            responseString = System.Text.Encoding.UTF8.GetString(response);

            //check if the reponse contains a table element with entity details
            return responseString.Contains(PAGERESULTSELEMENT);
        }

        /// <summary>
        /// Extract a key/value pair from a string. Used by the CIMA website search scraper.
        /// </summary>
        /// <param name="s">String to search</param>
        /// <param name="name">Key</param>
        /// <param name="delimiter">Key/Value delimiter</param>
        /// <returns></returns>
        private string ExtractFormData(string s, string name, string delimiter)
        {
            int viewStateNamePosition = s.IndexOf(name);
            int viewStateValuePosition = s.IndexOf(delimiter, viewStateNamePosition);
            int viewStateStartPosition = viewStateValuePosition + delimiter.Length;
            int viewStateEndPosition = s.IndexOf("\"", viewStateStartPosition);

            return s.Substring(viewStateStartPosition, viewStateEndPosition - viewStateStartPosition);
        }



        #endregion

        #region Viewpoint Entity

        /// <summary>
        /// Returns a count of current entities 
        /// </summary>
        /// <param name="regOfficeEntity">optional: restrict count to a legal entity providing ro services</param>
        /// <returns></returns>
        public int GetEntityCount(string regOfficeEntity)
        {
            string roAddressCode = GetSetting("Address.RegisteredOffice.TypeCode");

            var query = from mf in _ViewpointContext.vw_W360_MasterFileSetup
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                        join addr in _ViewpointContext.vw_W360_Addresses.Where(x => x.AddrType == roAddressCode) on ent.EntCode equals addr.RefCode into gj
                        from addrorempty in gj.DefaultIfEmpty()
                        where ent.EntityStatus.StartsWith("Active")
                        select new { mf, ent, addrorempty };

            if (!string.IsNullOrEmpty(regOfficeEntity))
            {
                string addressToMatch = _db.CorisAgents.Where(a => a.AgentName == regOfficeEntity).Select(a => a.RegOfficeAddress).FirstOrDefault();
                query = query.Where(t => t.addrorempty != null && t.addrorempty.AdLine == addressToMatch);
            }

            return query.Count();
        }

        /// <summary>
        /// Returns a list of entities that are awaiting action by a cdd officer
        /// </summary>
        /// <returns></returns>
        public List<ComplianceAwaitingActionEntity> GetComplianceAwaitingAction(List<string> cddStatusCodes, string cseOfficerCode, string relationshipMgrCode, string regOfficeEntity, string cddCollectorCode)
        {
            //List<string> riskLevelCodes = GetListSetting("Widget.ComplianceNotApproved.RiskLevelCodes");
            string roAddressCode = GetSetting("Address.RegisteredOffice.TypeCode");

            _ViewpointContext.Database.Log = Console.WriteLine;

            var query = from mf in _ViewpointContext.vw_W360_MasterFileSetup
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                        join cdd in _ViewpointContext.vw_W360_CDD.Where(
                            cdd => cddStatusCodes.Contains(cdd.CDDStatusCode)
                            //|| riskLevelCodes.Contains(cdd.RiskLevelCode)
                            ) on mf.RefCode equals cdd.RefCode
                        join addr in _ViewpointContext.vw_W360_Addresses.Where(x => x.AddrType == roAddressCode) on ent.EntCode equals addr.RefCode into gj
                        from addrorempty in gj.DefaultIfEmpty()
                        join cddcollector in _ViewpointContext.vw_W360_StatutoryOfficers.Where(x => x.OfficerType == "M96" && x.Status == "A") on ent.EntCode equals cddcollector.EntCode into gj2
                        from cddcollectororempty in gj2.DefaultIfEmpty()
                        join cddcollectorentity in _ViewpointContext.vw_W360_MasterFileSetup on cddcollectororempty.AddrCode equals cddcollectorentity.RefCode into gj3
                        from cddcollectorentityorempty in gj3.DefaultIfEmpty()
                        where ent.IsCurrent || ent.EntityStatusCode == "13" //VH: For the purposes of compliance Widgit only the entity status "TBC - Transfer out should be included
                        select new { mf, ent, cdd, addrorempty, cddcollectorentityorempty };

            if (!string.IsNullOrEmpty(cseOfficerCode))
                query = query.Where(t => t.ent.CseOfficerCode == cseOfficerCode);

            if (!string.IsNullOrEmpty(relationshipMgrCode))
                query = query.Where(t => t.ent.RelationshipMgrCode == relationshipMgrCode);

            if (!string.IsNullOrEmpty(regOfficeEntity))
            {
                string addressToMatch = _db.CorisAgents.Where(a => a.AgentName == regOfficeEntity).Select(a => a.RegOfficeAddress).FirstOrDefault();
                query = query.Where(t => t.addrorempty != null && t.addrorempty.AdLine == addressToMatch);
            }

            if (!string.IsNullOrEmpty(cddCollectorCode))
                query = query.Where(t => t.cddcollectorentityorempty != null && t.cddcollectorentityorempty.WalkersIdentifier == cddCollectorCode);

            return query.Select(
                q => new ComplianceAwaitingActionEntity
                {
                    EntCode = q.ent.EntCode,
                    EntityName = q.ent.SearchName,
                    DateEntered = q.mf.DateEntered,
                    CDDStatus = q.ent.CDDStatus,
                    CDDOfficer = q.cdd.CDDOfficer,
                    CDDCollector = q.cddcollectorentityorempty.EntityName
                }).ToList();
        }

        public List<vw_W360_LookValues> GetVPLookupValues(string tabCode, string name)
        {
            return _ViewpointContext.vw_W360_LookValues.Where(l => l.TabCode == tabCode && l.Name.Contains(name)).OrderBy(l => l.Name).ToList();
        }

        public List<vw_W360_LookValues> GetVPLookupValues(string tabCode)
        {
            return _ViewpointContext.vw_W360_LookValues.Where(l => l.TabCode == tabCode).OrderBy(l => l.Name).ToList();
        }

        public Dictionary<string, string> GetVPRelationshipManagers(string name)
        {
            return _ViewpointContext.vw_W360_EntityAdmin
                .Where(o => o.RelationshipMgr.Contains(name))
                .GroupBy(o => new { o.RelationshipMgr, o.RelationshipMgrCode })
                .ToDictionary(g => g.Key.RelationshipMgrCode, g => g.Key.RelationshipMgr);
        }

        public List<EntityLookup> GetVPEntities(string searchTerm)
        {
            List<EntityLookup> result = _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.IsCurrent && (e.EntCode.Contains(searchTerm) | e.RefName.Contains(searchTerm))).Select(e => new EntityLookup { EntCode = e.EntCode, EntName = e.RefName }).ToList();
            return result;
        }
        public Dictionary<string, string> GetVPPartners(string name)
        {
            return _ViewpointContext.vw_W360_EntityAdmin
                .Where(o => o.Partner.Contains(name))
                .GroupBy(o => new { o.Partner, o.PartnerCode })
                .ToDictionary(g => g.Key.PartnerCode, g => g.Key.Partner);
        }

        public Dictionary<string, string> GetVPCSEs(string name)
        {
            return _ViewpointContext.vw_W360_EntityAdmin
                .Where(o => o.CseOfficer.Contains(name))
                .GroupBy(o => new { o.CseOfficer, o.CseOfficerCode })
                .ToDictionary(g => g.Key.CseOfficerCode, g => g.Key.CseOfficer);
        }

        private List<Walkers.Project360.Model.SearchResult> SearchForViewpointEntity(string q, SearchResultType t)
        {
            return _ViewpointContext
                .vw_W360_EntityAdmin
                .Where(o => o.EntCode.Contains(q) || o.SearchName.Contains(q))
                .Select(o => new Walkers.Project360.Model.SearchResult { Id = o.EntCode, Title = o.SearchName, Type = t })
                .ToList();
        }

        public vw_W360_MasterFileSetup GetVPMasterFileSetup(string refCode)
        {
            if (!string.IsNullOrEmpty(refCode))
            {
                return _ViewpointContext.vw_W360_MasterFileSetup.Where(o => o.RefCode == refCode).Single();
            }
            else
                throw new ArgumentException("refCode");
        }

        public vw_W360_EntityAdmin GetVPEntityAdmin(string refCode)
        {
            if (!string.IsNullOrEmpty(refCode))
            {
                return _ViewpointContext.vw_W360_EntityAdmin.Where(o => o.EntCode == refCode).FirstOrDefault();
            }
            else
                throw new ArgumentException("refCode");
        }

        public vw_W360_Deal GetVPDeal(string refCode)
        {
            if (!string.IsNullOrEmpty(refCode))
            {
                return _ViewpointContext.vw_W360_Deal.Where(o => o.EntCode == refCode).FirstOrDefault();
            }
            else
                throw new ArgumentException("refCode");
        }

        public IQueryable<vw_W360_EntityAdmin> GetVPEntityAdmins()
        {
            return _ViewpointContext.vw_W360_EntityAdmin;
        }

        public List<vw_W360_EntityAdmin> GetVPEntityAdmins(string masterGroupCode)
        {
            if (!string.IsNullOrEmpty(masterGroupCode))
            {
                var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                            join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                            where mf.MasterGroupCode == masterGroupCode && !ent.EntityStatus.StartsWith("Closed")
                            select ent;

                return query.ToList();
            }
            else
                throw new ArgumentException("masterGroupCode");
        }

        public Dictionary<string, int> GetVPEntityCountByRiskLevel(string masterGroupCode)
        {
            var query = from mf in _ViewpointContext.vw_W360_MasterFileSetup
                        join ent in _ViewpointContext.vw_W360_EntityAdmin on mf.RefCode equals ent.EntCode
                        join cdd in _ViewpointContext.vw_W360_CDD on mf.RefCode equals cdd.RefCode
                        where ent.IsCurrent && !string.IsNullOrEmpty(cdd.RiskLevel) && ent.MasterGroupCode == masterGroupCode
                        group cdd by new { cdd.RiskLevel } into riskLevelGroup
                        select riskLevelGroup;

            return query.ToDictionary(q => q.Key.RiskLevel, q => q.Count());
        }

        public Dictionary<string, string> GetVPSubGroups(string masterGroupCode)
        {
            if (!string.IsNullOrEmpty(masterGroupCode))
            {
                var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                            join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                            join sg in _ViewpointContext.vw_W360_MasterFileSetup on ent.SubGroupCode equals sg.RefCode
                            where mf.MasterGroupCode == masterGroupCode
                            group sg by new { sg.RefCode, sg.EntityName } into subgroups
                            select subgroups;

                return query.ToDictionary(q => q.Key.RefCode, q => q.Key.EntityName);
            }
            else
                throw new ArgumentException("masterGroupCode");
        }

        public List<User> GetRelationshipManagers(string masterGroupCode)
        {
            List<User> result = new List<User>();

            if (!string.IsNullOrEmpty(masterGroupCode))
            {
                var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                            join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                            where mf.MasterGroupCode == masterGroupCode && !string.IsNullOrEmpty(ent.RelationshipMgrCode) && ent.IsCurrent
                            group ent by new { ent.RelationshipMgrCode, ent.RelationshipMgr } into subgroups
                            select subgroups;

                foreach (var rm in query.ToList())
                {
                    result.Add(GetViewpointUserOrDefault(rm.Key.RelationshipMgrCode, rm.Key.RelationshipMgr));
                }

                return result;
            }
            else
                throw new ArgumentException("masterGroupCode");
        }

        public int GetVPEntityAdminCountByCSE(string cseOfficerCode, string statusPrefix)
        {
            return _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.EntityStatus.StartsWith(statusPrefix) && e.CseOfficerCode == cseOfficerCode).Count();
        }

        public int GetVPRegistrationCountByMG(string masterGroupCode, string authority)
        {
            var query = from ent in _ViewpointContext.vw_W360_EntityAdmin
                        join reg in _ViewpointContext.vw_W360_Registrations on ent.EntCode equals reg.EntCode
                        join regType in _ViewpointContext.vw_W360_Registration_Types on reg.BusinessTypeCode equals regType.ElCode
                        where ent.MasterGroupCode == masterGroupCode && ent.IsCurrent && reg.IsActive
                        select new { ent, reg, regType };

            if (!string.IsNullOrEmpty(authority))
                query = query.Where(q => q.regType.Authority == authority);

            return query.Count();
        }

        public int GetVPEntityAdminCountByMG(string masterGroupCode, string statusPrefix)
        {
            return _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.EntityStatus.StartsWith(statusPrefix) && e.MasterGroupCode == masterGroupCode).Count();
        }

        public List<vw_W360_Addresses> GetVPAddresses(string refCode)
        {
            if (!string.IsNullOrEmpty(refCode))
            {
                return _ViewpointContext.vw_W360_Addresses.Where(o => o.RefCode == refCode).ToList();
            }
            else
                throw new ArgumentException("refCode");
        }

        public List<vw_W360_EmailAddresses> GetVPEmailAddresses(string refCode)
        {
            if (!string.IsNullOrEmpty(refCode))
            {
                return _ViewpointContext.vw_W360_EmailAddresses.Where(o => o.MasterfileCode == refCode).ToList();
            }
            else
                throw new ArgumentException("refCode");
        }

        /// <summary>
        /// Return a list of master groups that have OPEN entities
        /// </summary>
        /// <returns></returns>
        public List<vw_W360_MasterGroups> GetVPMasterGroups()
        {
            var q = from mg in _ViewpointContext.vw_W360_MasterGroups
                    let eCount =
                        (
                            from ent in _ViewpointContext.vw_W360_EntityAdmin
                            where ent.MasterGroupCode == mg.GroupCode && !ent.EntityStatus.StartsWith("Closed")
                            select ent.EntCode
                        ).Count()
                    where eCount > 0
                    select mg;

            return q.OrderBy(g => g.Name).ToList();
        }

        /// <summary>
        /// Return a list of master groups that match the supplied term. Resulting groups must have at least one open entity.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<vw_W360_MasterGroups> GetVPMasterGroupsByName(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                return this.GetVPMasterGroups().Where(o => o.Name.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            else throw new ArgumentException(name);
        }

        private List<Walkers.Project360.Model.SearchResult> SearchForVPMasterGroups(string q, SearchResultType t)
        {
            return _ViewpointContext
                .vw_W360_MasterGroups
                .Where(o => o.GroupCode.Contains(q) || o.Name.Contains(q))
                .Select(o => new Walkers.Project360.Model.SearchResult { Id = o.GroupCode, Title = o.Name, Type = t })
                .ToList();
        }

        public vw_W360_MasterGroups GetVPMasterGroup(string masterGroupCode)
        {
            return _ViewpointContext.vw_W360_MasterGroups.Where(g => g.GroupCode == masterGroupCode).FirstOrDefault();
        }

        public vw_W360_CDD GetVPClientDueDilligence(string refCode)
        {
            if (!string.IsNullOrEmpty(refCode))
            {
                return _ViewpointContext.vw_W360_CDD.Where(o => o.RefCode == refCode).FirstOrDefault();
            }
            else
                throw new ArgumentException("refCode");
        }

        public List<vw_W360_Registrations> GetVPRegistrations(string refCode)
        {
            if (!string.IsNullOrEmpty(refCode))
            {
                return _ViewpointContext.vw_W360_Registrations.Where(o => o.EntCode == refCode).ToList();
            }
            else
                throw new ArgumentException("refCode");
        }

        public List<vw_W360_ShareCapital> GetVPShareCapital(string entCode)
        {
            if (!string.IsNullOrEmpty(entCode))
            {
                return _ViewpointContext.vw_W360_ShareCapital.Where(o => o.EntCode == entCode).ToList();
            }
            else
                throw new ArgumentException("entCode");
        }

        public List<vw_W360_ShareCapital> GetVPShareCapital(string entCode, string shareClass)
        {
            if (!string.IsNullOrEmpty(entCode))
            {
                if (!string.IsNullOrEmpty(shareClass))
                {
                    return _ViewpointContext.vw_W360_ShareCapital.Where(o => o.EntCode == entCode && o.ShareClass == shareClass).ToList();
                }
                else
                    throw new ArgumentException("shareClass");
            }
            else
                throw new ArgumentException("entCode");
        }

        public List<vw_W360_StatutoryOfficers> GetStautoryOfficers(string entCode)
        {
            if (!string.IsNullOrEmpty(entCode))
            {
                return _ViewpointContext.vw_W360_StatutoryOfficers.Where(x => x.EntCode == entCode && x.Status == "A").ToList();
            }
            else
                throw new ArgumentException("entCode");
        }

        public List<vw_W360_FixedAssets> GetFixedAssets(string entCode)
        {
            if (!string.IsNullOrEmpty(entCode))
            {
                return _ViewpointContext.vw_W360_FixedAssets.Where(x => x.EntCode == entCode).ToList();
            }
            else
                throw new ArgumentException("entCode");
        }

        public List<vw_W360_DealDates> GetDealDates(string entCode)
        {
            if (!string.IsNullOrEmpty(entCode))
            {
                return _ViewpointContext.vw_W360_DealDates.Where(x => x.EntCode == entCode).OrderBy(x => x.DealDateName).ToList();
            }
            else
                throw new ArgumentException("entCode");
        }

        public List<vw_W360_Regulatory> GetRegulatory(string entCode)
        {
            if (!string.IsNullOrEmpty(entCode))
            {
                return _ViewpointContext.vw_W360_Regulatory.Where(x => x.EntCode == entCode).ToList();
            }
            else
                throw new ArgumentException("entCode");
        }

        public List<vw_W360_Registration_Types> GetVPRegistrationTypes(string name)
        {
            return _ViewpointContext.vw_W360_Registration_Types.Where(t => t.Name.Contains(name)).OrderBy(t => t.Name).ToList();
        }

        public List<vw_W360_Registration_Types> GetVPRegistrationTypes()
        {
            return _ViewpointContext.vw_W360_Registration_Types.OrderBy(t => t.Name).ToList();
        }

        public List<vw_W360_EntityTypes> GetVPEntityTypes(string name)
        {
            return _ViewpointContext.vw_W360_EntityTypes.Where(t => t.Name.Contains(name) || t.Jursidiction.Contains(name)).ToList();
        }

        public List<vw_W360_EntityTypes> GetVPEntityTypes()
        {
            return _ViewpointContext.vw_W360_EntityTypes.ToList();
        }

        #endregion

        #region Card
        public Card GetCard(int id)
        {
            return _db.Cards
                .Include(c => c.Suit)
                .Include(c => c.Users)
                .Include(c => c.Users.Select(u => u.User))
                .Include(c => c.Events)
                .Include(c => c.CardReferences)
                .Where(s => s.Id == id).FirstOrDefault();
        }
        public List<Card> GetCards(User user)
        {
            if (user != null)
            {
                return _db.Cards
                    .Include(c => c.Suit)
                    .Include(c => c.CardReferences)
                    .Where(c => c.Users.Any(u => u.User.Id == user.Id) && c.Status == CardStatusOptions.Open)
                    .ToList();
            }
            else throw new ArgumentNullException("user");
        }

        public List<Card> GetCards(vw_W360_MasterFileSetup viewpointEntity)
        {
            if (viewpointEntity != null && !string.IsNullOrEmpty(viewpointEntity.RefCode))
            {
                return _db.Cards
                     .Include(c => c.Suit)
                     .Include(c => c.CardReferences)
                     .Where(r => r.CardReferences.Any(c => c.ReferenceObjectString == ReferenceObjectOptions.ViewpointEntity.ToString() && c.ReferenceCode == viewpointEntity.RefCode))
                    .ToList();
            }
            else
                throw new ArgumentException("viewpointEntity");
        }

        /// <summary>
        /// Get a list of all open cards
        /// </summary>
        /// <returns></returns>
        public List<Card> GetOpenCards()
        {
            return _db.Cards
                  .Include(c => c.Suit)
                  .Include(c => c.Users)
                  .Include(c => c.Users.Select(u => u.User))
                  .Include(c => c.CardReferences)
                  .Where(c => c.Status == CardStatusOptions.Open)
                  .ToList();
        }

        /// <summary>
        /// Get a list of all open cards By Deck Id
        /// </summary>
        /// <returns></returns>
        public List<Card> GetOpenCardsByDeckId(int? DeckId)
        {
            return (from c in _db.Cards
                   join s in _db.Suits on c.Suit.Id equals s.Id
                   where (c.Status == CardStatusOptions.Open) && (s.Deck.Id == DeckId)
                   select c).ToList();
        }


        /// <summary>
        /// Return the current number of open cards with a given escalationstatus
        /// </summary>
        /// <param name="EscalationStatus"></param>
        /// <returns></returns>
        public int GetOpenCardsCount(CardEscalationStatusOptions EscalationStatus)
        {
            if (EscalationStatus == CardEscalationStatusOptions.Overdue)
                return _db.Cards.Where(c => c.Status == CardStatusOptions.Open && DateTimeOffset.Now > c.DueDate).Count();

            else if (EscalationStatus == CardEscalationStatusOptions.Due)
                return _db.Cards.Where(c => c.Status == CardStatusOptions.Open && DateTimeOffset.Now <= c.DueDate && DateTimeOffset.Now > c.EscalateDate).Count();

            else if (EscalationStatus == CardEscalationStatusOptions.InProgress)
                return _db.Cards.Where(c => c.Status == CardStatusOptions.Open && DateTimeOffset.Now <= c.EscalateDate).Count();

            else
                throw new ArgumentOutOfRangeException();
        }

        /// <summary>
        /// Return the number of open cards at a point in time
        /// </summary>
        /// <param name="EffectiveDate"></param>
        /// <returns></returns>
        public int GetOpenCardsCount(DateTimeOffset EffectiveDate)
        {
            return _db.Cards
                   .Where(c => (c.Status == CardStatusOptions.Open || c.Status == CardStatusOptions.Completed) && c.DateCreated <= EffectiveDate && (c.DateCompleted == null || c.DateCompleted > EffectiveDate))
                   .Count();
        }

        public List<Card> GetSuitCards(Suit suit)
        {
            if (suit != null)
            {
                return _db.Cards
                    .Include(c => c.Users)
                    .Include(c => c.Events)
                    .Include(c => c.CardReferences)
                    .Where(s => s.Suit.Id == suit.Id && (s.Status == CardStatusOptions.Open || s.Status == CardStatusOptions.Ignored))
                    .ToList();
            }
            else throw new ArgumentNullException("suit card");
        }
        public void AddCardUsers(Card newCard)
        {
            if (newCard.Users != null)
            {
                foreach (var cardUser in newCard.Users)
                {
                    _db.CardUsers.Add(cardUser);
                    System.Data.Entity.Infrastructure.DbEntityEntry dbEntityEntry = _db.Entry(cardUser);
                    if (dbEntityEntry.State == EntityState.Detached)
                    {
                        _db.CardUsers.Attach(cardUser);
                    }
                }
            }
        }
        public Card AddNewCard()
        {
            Card result = new Card();
            _db.Cards.Add(result);
            return result;
        }
        public CardUser AddNewCardUser()
        {
            CardUser cardUser = new CardUser();
            _db.CardUsers.Add(cardUser);
            return cardUser;
        }
        public void AddCard(Card newCard)
        {
            //need to add the card users so that the navigation property gets updated
            AddCardUsers(newCard);
            _db.Cards.Add(newCard);

        }
        public void SaveRequestCards(List<RequestCard> cards)
        {
            if (cards != null)
            {
                foreach (var requestCard in cards)
                {
                    _db.Cards.Add(requestCard.Card);
                    _db.RequestCards.Add(requestCard);
                }
            }
            _db.SaveChanges();
        }
        internal void AttachCard(Card card)
        {
            System.Data.Entity.Infrastructure.DbEntityEntry dbEntityEntry = _db.Entry(card);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                _db.Cards.Attach(card);
            }
            if (card.Suit != null)
            {
                System.Data.Entity.Infrastructure.DbEntityEntry dbEntityParentEntry = _db.Entry(card.Suit);
                if (dbEntityParentEntry.State == EntityState.Detached)
                {
                    _db.Suits.Attach(card.Suit);
                }
            }
        }
        public void SaveCards(List<Card> cards)
        {
            if (cards != null)
            {
                foreach (var card in cards)
                {
                    this.SaveCard(card);
                }
            }
        }
        public void SaveCard(Card card)
        {
            this.AttachCard(card);

            try
            {
                _db.SaveChanges();

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Diagnostics.Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
        }

        private List<Walkers.Project360.Model.SearchResult> SearchForCard(string q, SearchResultType t)
        {
            return _db.Cards
                .Include(c => c.Suit)
                .Where(o => o.Suit.SuitName.Contains(q) || o.ReferenceName.Contains(q) || o.ReferenceCode.Contains(q))
                .ToList()
                .Select(o => new Walkers.Project360.Model.SearchResult { Id = o.Id.ToString(), Title = o.Title, Type = t })
                .ToList();
        }

        #endregion

        #region Request

        public List<ServiceOffered> GetServices()
        {
            return _db.ServicesOffered.Where(s => s.Enabled == true).ToList();
        }

        public List<Request> GetOpenRequests()
        {
            var query = _db.Requests
                .Include(c => c.Services).Include(c => c.Services.Select(u => u.Service))
                .Include(r => r.Entities)
                .Where(r => r.Status == RequestStat.Open);

            return query.ToList();
        }

        public List<Request> GetRequests(User user)
        {
            var query = _db.Requests
                .Include(c => c.Services).Include(c => c.Services.Select(u => u.Service))
                .Include(r => r.Entities)
                .Where(r => r.Status == RequestStat.Open && r.Requesters.Any(u => u.UserId == user.Id));

            return query.ToList();
        }

        public List<RequestCard> GetRequestCards(User user)
        {
            var query = _db.RequestCards
                .Include(r => r.Request)
                .Include(r => r.Card)
                .Include(r => r.Card.Suit)
                .Where(r => r.Card.Status == CardStatusOptions.Open && r.Request.Requesters.Any(u => u.UserId == user.Id));

            return query.ToList();
        }

        public int GetRequestIdByCard(int cardId)
        {
            var result = _db.RequestCards.Where(r => r.CardId == cardId).FirstOrDefault();
            return (result ?? new RequestCard { CardId = 0, RequestId = 0 }).RequestId;

        }
        public Request GetRequest(int id)
        {
            return _db.Requests.Where(r => r.id == id)
                            .Include(c => c.Services).Include(c => c.Services.Select(u => u.Service))
                            .Include(r => r.Entities)
                            .Include(r => r.Requesters).Include(r => r.Requesters.Select(u => u.User))
                            .Include(r => r.Cards).Include(r => r.Cards.Select(u => u.Card))
                            .FirstOrDefault();

        }

        public List<Request> GetNewRequests()
        {
            var results = _db.Requests.Where(r => r.Status == RequestStat.Open && r.Cards.Count == 0)
                            .Include(c => c.Services).Include(c => c.Services.Select(u => u.Service))
                            .Include(r => r.Entities)
                            .Include(r => r.Requesters).Include(r => r.Requesters.Select(u => u.User))
                            .ToList();
            foreach (var request in results)
            {
                request.AttachmentMetadata = GetFileMetadata(request.id);
            }
            return results;
        }
        public int? GetCardRequestId(int cardId)
        {
            int? result = null;

            var query = _db.RequestCards.Where(c => c.CardId == cardId).FirstOrDefault();
            if (query != null)
            {
                result = query.RequestId;
            }
            return result;
        }
        public List<FileMetadata> GetCardAttachmentMetadata(int cardId)
        {
            List<FileMetadata> result;
            var requestId = GetCardRequestId(cardId);
            if (requestId.HasValue)
            {
                result = GetFileMetadata(requestId.Value);
            }
            else
            {
                result = new List<FileMetadata>();
            }
            return result;
        }
        public List<FileMetadata> GetFileMetadata(int requestId)
        {
            var query = _db.FileStore.Where(f => f.RefCode == requestId.ToString()).Select(m => new FileMetadata { id = m.id, FileName = m.FileName });
            return query.ToList();
        }
        public List<FileStore> GetRequestFiles(int requestId)
        {
            var query = _db.FileStore.Where(f => f.RefCode == requestId.ToString());
            return query.ToList();
        }
        public FileStore GetFile(int fileId)
        {
            var query = _db.FileStore.Where(f => f.id == fileId).FirstOrDefault();
            return query;
        }
        public void UpdateRequest(Request request)
        {
            if (request != null)
            {
                var oldRequest = _db.Requests.First(r => r.id == request.id);
                string readonlyComments = oldRequest.Comments;

                _db.Entry(oldRequest).CurrentValues.SetValues(request);
                oldRequest.Comments = readonlyComments;

                _db.SaveChanges();
            }
        }
        public void SaveRequest(Request request)
        {
            if (request != null)
            {
                _db.Requests.Add(request);
                foreach (var service in request.Services)
                { _db.ServiceRequests.Add(service); }

                foreach (var ent in request.Entities) { _db.RequestEntities.Add(ent); }

                foreach (var usr in request.Requesters) { _db.Requesters.Add(usr); }


                _db.SaveChanges();
                if (request.AttachedFiles != null)
                {
                    foreach (var file in request.AttachedFiles)
                    {
                        file.RefCode = request.id.ToString();
                        _db.FileStore.Add(file);
                    }
                    _db.SaveChanges();
                }
            }
        }
        public EntityAdmin GetCardData(string entCode)
        {

            var result = _ViewpointContext.vw_W360_EntityAdmin.Where(e => e.EntCode == entCode).Select(e => new EntityAdmin { CseCode = e.CseOfficerCode, EntCode = entCode, EntName = e.SearchName, RelationshipManagerCode = e.RelationshipMgrCode }).FirstOrDefault();
            result.CseUser = GetViewpointUser(result.CseCode);
            result.RelationshipManagerUser = GetViewpointUser(result.RelationshipManagerCode);
            return result;


        }

        #endregion

        #region File Drop
        public List<CorisEntityStatusRow> ProcessCorisFile(string createdBy, DataSet ds)
        {
            List<CorisEntityStatusRow> Imd = new List<CorisEntityStatusRow>();
            if (ds != null)
            {
                StringBuilder inst = new StringBuilder();
                try
                {
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        inst.Append(column.ColumnName);
                    }
                }
                catch
                {
                    Imd = null;
                }
                CorisEntityStatusRow cesr = new CorisEntityStatusRow();
                var inst2 = cesr.GetColumnSignature();
                if (inst.ToString() == inst2)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Imd.Add(new CorisEntityStatusRow
                        {
                            CreatedBy = createdBy,
                            PAR_IDENTIFIER = dr["PAR_IDENTIFIER"].ToString().Trim(),
                            PAR_AGENT_IDENTIFIER = dr["PAR_AGENT_IDENTIFIER"].ToString().Trim(),
                            PAR_NAME = dr["PAR_NAME"].ToString().Trim(),
                            PAR_AGENT = dr["PAR_AGENT"].ToString().Trim(),
                            AGENT_NAME = dr["AGENT_NAME"].ToString().Trim(),
                            PAR_COT_CODE = dr["PAR_COT_CODE"].ToString().Trim(),
                            PAR_FILENO = dr["PAR_FILENO"].ToString().Trim(),
                            PAR_STATUS = dr["PAR_STATUS"].ToString().Trim(),
                            PAR_RESERVE_STATUS = dr["PAR_RESERVE_STATUS"].ToString().Trim(),
                            PAR_RESERVE_DATE = dr["PAR_RESERVE_DATE"].ToString().Trim(),
                            PAR_TYPE = dr["PAR_TYPE"].ToString().Trim(),
                            PAR_INCORPORATION = dr["PAR_INCORPORATION"].ToString().Trim(),
                            PAR_FORMATION_DATE = dr["PAR_FORMATION_DATE"].ToString().Trim(),
                            PAR_REGISTRATION_DATE = dr["PAR_REGISTRATION_DATE"].ToString().Trim(),
                            PAR_CLIENT_REF = dr["PAR_CLIENT_REF"].ToString().Trim()
                        });
                    }
                    Imd.RemoveAll(item => item.PAR_AGENT_IDENTIFIER.Length == 0);
                }
                else { Imd = null; return Imd; }
            }
            return Imd;
        }

        public void SaveCESFile(List<CorisEntityStatusRow> w)
        {
            //clear table
            _db.CorisEntityStatusRows.RemoveRange(_db.CorisEntityStatusRows);
            //add new rows
            _db.CorisEntityStatusRows.AddRange(w);

            _db.SaveChanges();
        }

        public List<CompaniesByAgent> ProcessAgentFile(string createdBy, DataSet ds)
        {
            List<CompaniesByAgent> Amd = new List<CompaniesByAgent>();
            if (ds != null)
            {
                StringBuilder inst = new StringBuilder();
                try
                {
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        inst.Append(column.ColumnName);
                    }
                }
                catch
                {
                    Amd = null;
                }
                CompaniesByAgent cesr = new CompaniesByAgent();
                var inst2 = cesr.GetColumnSignature();
                if (inst.ToString() == inst2)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Amd.Add(new CompaniesByAgent
                        {

                            Createdby = createdBy,
                            SDATE = dr["SDATE"].ToString().Trim(),
                            AGENT = dr["AGENT"].ToString().Trim(),
                            FILENO = dr["FILENO"].ToString().Trim(),
                            COMPANY = dr["COMPANY"].ToString().Trim(),
                            TYPE = dr["TYPE"].ToString().Trim(),
                            STATUS = dr["STATUS"].ToString().Trim(),
                            REG_DATE = dr["REG_DATE"].ToString().Trim(),
                            SHARES = dr["SHARES"].ToString().Trim(),
                            SHARE_CODE = dr["SHARE_CODE"].ToString().Trim(),
                            SHARE_AMOUNT = dr["SHARE_AMOUNT"].ToString().Trim(),
                            CI_SHARE_AMT = dr["CI_SHARE_AMT"].ToString().Trim()
                        });
                    }
                    Amd.RemoveAll(item => item.FILENO.Length == 0);
                    _db.CompaniesByAgent.AddRange(Amd);
                    _db.SaveChanges();
                }
                else { Amd = null; return Amd; }
            }
            else
            {
                Amd = null;
            }

            return Amd;
        }

        public void SaveAgentFile(List<CompaniesByAgent> f)
        {
            //clear table
            _db.CompaniesByAgent.RemoveRange(_db.CompaniesByAgent);
            //add new rows
            _db.CompaniesByAgent.AddRange(f);

            _db.SaveChanges();
        }

        #endregion

        #region Aderant
        public InteractionMetric GetInteractionMetric(int clientId)
        {//This is just a stub until the real retrieval exists; also change the default values in the InteractionMetric class
            return new InteractionMetric();
        }
        public FinancialMetric GetFinancialMetric(int clientId)
        {//This is just a stub until the real retrieval exists; also change the default values in the FinancialMetric class
            return new FinancialMetric();
        }
        public InteractionMetric GetMatterInteractionMetric(int id)
        {//This is just a stub until the real retrieval exists; also change the default values in the InteractionMetric class
            return new InteractionMetric();
        }
        public FinancialMetric GetMatterFinancialMetric(int id)
        {//This is just a stub until the real retrieval exists; also change the default values in the FinancialMetric class
            return new FinancialMetric();
        }


        public PieChartResult GetPieChart(PieMetric metric, string profitCenter, string office)
        {
            PieChartResult result;
            switch (metric)
            {
                case PieMetric.AR:
                    result = GetPieAR(profitCenter, office);
                    break;
                case PieMetric.Billed:
                    result = GetPieBilled(profitCenter, office);
                    break;
                case PieMetric.WIP:
                    result = GetWip(profitCenter, office);
                    break;
                default:
                    throw new Exception("Metric not found");

            }
            return result;
        }

        private PieChartResult GetWip(string profitCenter, string office)
        {
            throw new NotImplementedException();
        }

        private PieChartResult GetPieBilled(string profitCenter, string office)
        {
            throw new NotImplementedException();
        }

        private PieChartResult GetPieAR(string profitCenter, string office)
        {
            PieChartResult result;
            result = new PieChartResult { Amount = 100M, Currency = "USD", Header = "Cayman Monthly A/R", Title = "$100k", Percent = 85, OfficeCode = office, ProfitCenterCode = profitCenter };
            return result;
        }


        public List<AderantOffice> GetAderantOffices()
        {
            var result = _AderantContext.AderantOffices.Where(c => c.IsCurrent == true);
            return result.ToList();
        }
        public AderantProfitCenter GetAderantProfitCenter(string code)
        {
            var result = _AderantContext.AderantProfitCenters.Where(p => p.ProfCtrCode == code).FirstOrDefault();
            return result;
        }

        public IQueryable<AderantClient> GetAderantClients()
        {
            return _AderantContext.AderantClients.Where(c => c.IsCurrent == true);
        }



        public AderantClient GetAderantClient(int clientId)
        {
            var result = _AderantContext.AderantClients.Where(c => c.Id == clientId).FirstOrDefault();
            result.InteractionMetric = this.GetInteractionMetric(clientId);
            result.FinancialMetric = this.GetFinancialMetric(clientId);
            return result;
        }
        public List<LatestItem> GetAderantLatestClients(string profitCenter)
        {
            var result = _AderantContext.AderantClients.Where(m => m.ProfCtrCode == profitCenter)
                            .OrderByDescending(m => m.OpenDate)
                            .Take(20)
                            .Select(d => new LatestItem { FirstTitle = d.Name + " (" + d.Code.Trim() + ")", SecondTitle = string.Empty, DoneDate = d.OpenDate ?? DateTime.MinValue, ColorLabel = d.OfficeLabel });
            return result.ToList();
        }
        public List<AderantMatter> GetAderantMatters(int clientId)
        {
            var result = _AderantContext.AderantMatters.Where(m => m.ClientId == clientId);
            return result.ToList();
        }

        public List<AderantMatter> GetAderantMatters(string name)
        {
            var result = _AderantContext.AderantMatters.Where(m => (m.Name.Contains(name) || m.Code.Contains(name)) && m.IsCurrent).OrderBy(m => m.Name);
            return result.ToList();
        }

        public AderantMatter GetAderantMatter(int id)
        {
            var result = _AderantContext.AderantMatters.Where(m => m.Id == id).FirstOrDefault();
            result.InteractionMetric = this.GetMatterInteractionMetric(id);
            result.FinancialMetric = this.GetMatterFinancialMetric(id);
            return result;
        }
        public List<AderantClientEvent> GetAderantClientEvents(int clientId)
        {
            var result = _AderantContext.AderantClientEvents.Where(m => m.Id == clientId);
            return result.ToList();
        }
        public List<LatestItem> GetAderantLatestMatters(string profitCenter)
        {
            var result = _AderantContext.AderantMatters.Where(m => m.ProfCtrCode == profitCenter)
                            .OrderByDescending(m => m.OpenDate)
                            .Take(20)
                            .Join(_AderantContext.AderantClients, m => m.ClientId, c => c.Id, (m, c) => new { Matter = m, Client = c })
                            .Select(d => new LatestItem { FirstTitle = d.Matter.Name, SecondTitle = d.Client.Name + " (" + d.Client.Code.Trim() + ")", DoneDate = d.Matter.OpenDate ?? DateTime.MinValue, ColorLabel = d.Matter.OfficeLabel });
            return result.ToList();
        }
        public List<LatestItem> GetAderantLatestMatters(int clientId)
        {
            var result = _AderantContext.AderantMatters.Where(m => m.ClientId == clientId)
                            .OrderByDescending(m => m.OpenDate)
                            .Take(10)
                            .Join(_AderantContext.AderantClients, m => m.ClientId, c => c.Id, (m, c) => new { Matter = m, Client = c })
                            .Select(d => new LatestItem { FirstTitle = d.Matter.Name, SecondTitle = d.Client.Name + " (" + d.Client.Code.Trim() + ")", DoneDate = d.Matter.OpenDate ?? DateTime.MinValue, ColorLabel = d.Matter.OfficeLabel });
            return result.ToList();
        }
        public List<LatestItem> GetAderantLatestMatterInvoices(int matterId)
        {
            //var result = _AderantContext.AderantMatters.Where(m => m.ClientId == clientId)
            //                .Take(10)
            //                .Join(_AderantContext.AderantClients, m => m.ClientId, c => c.Id, (m, c) => new { Matter = m, Client = c })
            //                .Select(d => new LatestItem { FirstTitle = d.Matter.Name, SecondTitle = d.Client.Name + " (" + d.Client.Code + ")", DoneDate = d.Matter.OpenDate ?? DateTime.MinValue, ColorLabel = d.Matter.OfficeLabel });
            List<LatestItem> result = new List<LatestItem>();
            result.Add(new LatestItem { FirstTitle = "Invoice Amount: 12,000 USD", SecondTitle = "Status: Open", DoneDate = Convert.ToDateTime("10/10/2014") });
            result.Add(new LatestItem { FirstTitle = "Invoice Amount: 7,500 USD", SecondTitle = "Status: Closed", DoneDate = Convert.ToDateTime("10/1/2013") });
            result.Add(new LatestItem { FirstTitle = "Invoice Amount: 8,000 USD", SecondTitle = "Status: Closed", DoneDate = Convert.ToDateTime("10/4/2014") });
            return result.ToList();
        }
        public List<MonthlyCount> GetAderantMonthlyData(string profitCenter, string domain)
        {
            List<MonthlyCount> result;
            switch (domain)
            {
                case "OpenedMatters":
                    result = GetAderantMonthlyMatters(profitCenter);
                    break;
                case "NewClients":
                    result = GetAderantNewClients(profitCenter);
                    break;
                default:
                    throw new Exception("Monthly data not mapped.");
            }
            return result;
        }
        public List<MonthlyCount> GetAderantNewClients(string profitCenter)
        {
            var sinceDate = DateTime.Now.AddYears(-1);
            DateTime yearOfMonths = new DateTime(sinceDate.Year, sinceDate.Month, 1);
            var result = _AderantContext.AderantClients.Where(m => m.ProfCtrCode == profitCenter)
                            .Where(m => m.OpenDate >= yearOfMonths)
                            .GroupBy(m => new { m.ProfCtrCode, m.MonthOpened, m.MonthOpenedName, m.YearOpened })
                            .Select(d => new MonthlyCount { MonthName = d.Key.MonthOpenedName, MonthlyValue = d.Count(), Month = d.Key.MonthOpened ?? DateTime.MinValue.Month, Year = d.Key.YearOpened ?? DateTime.MinValue.Year });
            return result.ToList();
        }
        public List<MonthlyCount> GetAderantMonthlyMatters(string profitCenter)
        {
            var sinceDate = DateTime.Now.AddYears(-1);
            DateTime yearOfMonths = new DateTime(sinceDate.Year, sinceDate.Month, 1);
            var result = _AderantContext.AderantMatters.Where(m => m.ProfCtrCode == profitCenter)
                            .Where(m => m.OpenDate >= yearOfMonths)
                            .GroupBy(m => new { m.ProfCtrCode, m.MonthOpened, m.MonthOpenedName, m.YearOpened })
                            .Select(d => new MonthlyCount { MonthName = d.Key.MonthOpenedName, MonthlyValue = d.Count(), Month = d.Key.MonthOpened ?? DateTime.MinValue.Month, Year = d.Key.YearOpened ?? DateTime.MinValue.Year });
            return result.ToList();
        }

        public List<TimelineEntry> GetAderantClientTimeline(int clientId)
        {
            var result = _AderantContext.AderantClientEvents.Where(m => m.Id == clientId).Select(m => new TimelineEntry { Source = "AderantClient", EventType = m.EventType, DoneDate = m.DoneDate ?? DateTime.MinValue });
            return result.ToList();
        }

        public List<AssociatedUser> GetAderantClientUsers(int clientId)
        {
            var result = _AderantContext.AderantClientUsers.Where(m => m.ClientId == clientId).Select(m => new AssociatedUser { AssociationType = (AssociationType)m.AssociationType, Reference = m.Login }).ToList();

            foreach (var item in result)
            {
                item.User = this.GetUser(item.Reference);
            }
            return result;
        }

        public List<TimelineEntry> GetAderantMatterTimeline(int matterId)
        {
            var result = _AderantContext.AderantMatterEvents.Where(m => m.MatterId == matterId).Select(m => new TimelineEntry { Source = "AderantMatter", EventType = m.EventType, DoneDate = m.DoneDate ?? DateTime.MinValue });
            return result.ToList();
        }

        public List<AssociatedUser> GetAderantMatterUsers(int matterId)
        {
            var result = _AderantContext.AderantMatterUsers.Where(m => m.MatterId == matterId).Select(m => new AssociatedUser { AssociationType = (AssociationType)m.AssociationType, Reference = m.Login }).ToList();

            foreach (var item in result)
            {
                item.User = this.GetUser(item.Reference);
            }
            return result;
        }

        public AderantInvoiceFile GetAderantInvoiceFile(int number)
        {
            return _AderantContext.AderantInvoiceFiles.Where(f => f.Number == number).OrderByDescending(f => f.Id).FirstOrDefault();
        }

        public byte[] GetAderantInvoiceFileContent(AderantInvoiceFile f)
        {
            if (f != null && !string.IsNullOrEmpty(f.FilePath))
            {
                if (File.Exists(f.FilePath))
                    return File.ReadAllBytes(f.FilePath);
                else
                    throw new FileNotFoundException();
            }
            else
                throw new ArgumentException();
        }

        #endregion

        #region MVCForum
        public List<BulletinBoardPost> GetAnnouncementsFeed(int limit)
        {
            var client = new WebClient();
            client.UseDefaultCredentials = false;
            client.Credentials = CredentialCache.DefaultCredentials;
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            var URL = GetSetting("rss.WalkersHUB");
            var xmlData = client.DownloadString(URL);

            XDocument xml = XDocument.Parse(xmlData);

            var announcements = (from story in xml.Descendants("item")
                                 select new BulletinBoardPost
                                 {
                                     Title = ((string)story.Element("title")),
                                     CreatedDate = ((DateTime)story.Element("pubDate")),
                                     Url = ((string)story.Element("link")),
                                     Category = ((string)story.Element("category")),
                                     CreatedBy = ((string)story.Element("pubBy"))
                                 })
                                 .Take(limit);

            return announcements.ToList();
        }

        public List<BulletinBoardPost> GetAlertFeed(int limit)
        {
            List<BulletinBoardPost> results = new List<BulletinBoardPost>();
            var client = new WebClient();
            client.UseDefaultCredentials = false;
            client.Credentials = CredentialCache.DefaultCredentials;
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            var URL = GetSetting("rss.Alerts");
            if (!string.IsNullOrEmpty(URL))
            {
                var xmlData = client.DownloadString(URL);

                XDocument xml = XDocument.Parse(xmlData);

                var alerts = (from story in xml.Descendants("item")
                              select new BulletinBoardPost
                              {
                                  Title = ((string)story.Element("title")),
                                  CreatedDate = ((DateTime)story.Element("pubDate")),
                                  Url = ((string)story.Element("link")),
                                  Category = ((string)story.Element("category")),
                                  CreatedBy = ((string)story.Element("pubBy"))
                              })
                                     .Take(limit);
                results = alerts.ToList();
            }
            return results;
        }
        #endregion

        #region Test Methods
        public List<TestRowForException> GetMissingTestRows()
        {
            return _db.TestRows.Where(t => t.FieldCheckExistName == null || t.FieldCheckExistName == string.Empty).ToList();
        }
        #endregion

        #region Search
        public List<Walkers.Project360.Model.SearchResult> Search(string q, SearchResultType t)
        {
            switch (t)
            {
                case SearchResultType.Walkers360Card:
                    return SearchForCard(q, t);

                case SearchResultType.ViewPointEntity:
                    return SearchForViewpointEntity(q, t);

                case SearchResultType.ViewPointMasterGroup:
                    return SearchForVPMasterGroups(q, t);

                default: throw new NotImplementedException();
            }
        }

        #endregion

        #region Legal Reference
        public List<LegalReference> GetLegalReferences(int User_Id)
        {
            return _db.LegalReferences.Where(r => r.User.Id == User_Id).OrderBy(r => r.Title).ToList();
        }

        public bool AddOrRemoveLegalReference(int User_Id, string Title, string Url)
        {
            bool result;

            result = _db.LegalReferences.Any(r => r.User.Id == User_Id && r.URL == Url);

            if (result)
                _db.LegalReferences.RemoveRange(_db.LegalReferences.Where(r => r.User.Id == User_Id && r.URL == Url));
            else
                _db.LegalReferences.Add(new LegalReference { Title = Title, URL = Url, User = GetUser(User_Id) });

            _db.SaveChanges();

            return result;
        }
        #endregion

        #region Deal

        public void DismissDealDate(string dealdateid, User user)
        {
            var q = _ViewpointContext.vw_W360_DealDates.Single(dd => dd.Id == dealdateid);

            DealDateDismissed o = new DealDateDismissed
            {
                EntCode = q.EntCode,
                DealDateCode = q.DealDateCode,
                DealDate = q.DealDate.Value,
                DismissedBy = user.DisplayName,
                DismissedDate = DateTimeOffset.Now
            };

            _db.DealDateDismissals.Add(o);
            _db.SaveChanges();
        }

        public List<Tuple<vw_W360_MasterFileSetup, vw_W360_DealDates>> GetDealDateReminders()
        {
            var entities = from ent in _ViewpointContext.vw_W360_EntityAdmin
                           join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                           where mf.EntityAdmin == true && ent.IsCurrent
                           select mf.RefCode;

            return GetDealDateReminders(entities.ToList());
        }

        public List<Tuple<vw_W360_MasterFileSetup, vw_W360_DealDates>> GetDealDateRemindersForCSE(string login)
        {
            var entities = from ent in _ViewpointContext.vw_W360_EntityAdmin
                           join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                           where mf.EntityAdmin == true && ent.IsCurrent && ent.CseOfficerCode == login
                           select mf.RefCode;

            return GetDealDateReminders(entities.ToList());
        }

        public List<Tuple<vw_W360_MasterFileSetup, vw_W360_DealDates>> GetDealDateRemindersForRM(string login)
        {
            var entities = from ent in _ViewpointContext.vw_W360_EntityAdmin
                           join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                           where mf.EntityAdmin == true && ent.IsCurrent && ent.RelationshipMgrCode == login
                           select mf.RefCode;

            return GetDealDateReminders(entities.ToList());
        }

        public List<Tuple<vw_W360_MasterFileSetup, vw_W360_DealDates>> GetDealDateRemindersForDirector(string login)
        {
            var entities = from ent in _ViewpointContext.vw_W360_EntityAdmin
                           join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                           join so in _ViewpointContext.vw_W360_StatutoryOfficers on mf.RefCode equals so.EntCode
                           join somf in _ViewpointContext.vw_W360_MasterFileSetup on so.AddrCode equals somf.RefCode
                           where mf.EntityAdmin == true && ent.IsCurrent && so.Status == "A" && somf.WalkersIdentifier == login
                           group mf by mf into g
                           select g.Key.RefCode;

            return GetDealDateReminders(entities.ToList());
        }


        private List<Tuple<vw_W360_MasterFileSetup, vw_W360_DealDates>> GetDealDateReminders(List<string> entCodes)
        {
            if (entCodes == null || entCodes.Count() == 0)
                return new List<Tuple<vw_W360_MasterFileSetup, vw_W360_DealDates>>();

            //get all existing deal dates for the supplied entCodes
            var dealDates = from mf in _ViewpointContext.vw_W360_MasterFileSetup
                            join deal in _ViewpointContext.vw_W360_Deal on mf.RefCode equals deal.EntCode
                            join dealdt in _ViewpointContext.vw_W360_DealDates on deal.EntCode equals dealdt.EntCode
                            where entCodes.Contains(deal.EntCode) && dealdt.DealDate.HasValue
                            select new { mf, deal, dealdt };

            //get the list of all valid intersections with reminders
            var matches = _db.DealDateMatches.Where(dd => dd.ReminderStored.HasValue).ToList();

            //get the previously dismissed dates to ignore
            var dismissals = _db.DealDateDismissals.Where(d => entCodes.Contains(d.EntCode)).ToList();

            var query = from dd in dealDates.ToList()
                        join m in matches on new { dd.dealdt.DealDateCode, dd.deal.DealTypeCode, dd.deal.DealStatusCode } equals new { m.DealDateCode, m.DealTypeCode, m.DealStatusCode }
                        join dis in dismissals on new { dd.dealdt.EntCode, dd.dealdt.DealDateCode, DealDate = dd.dealdt.DealDate.Value } equals new { dis.EntCode, dis.DealDateCode, dis.DealDate } into oj
                        from dismiss in oj.DefaultIfEmpty()
                        where dismiss == null && DateTime.Now.Add(m.Reminder.Value) > dd.dealdt.DealDate.Value
                        select new Tuple<vw_W360_MasterFileSetup, vw_W360_DealDates>(dd.mf, dd.dealdt);

            return query.ToList();
        }

        public List<ExceptionViolation> GetMissingDealDates()
        {
            List<ExceptionViolation> results = new List<ExceptionViolation>();

            //get a list of all current deals
            var dealsQuery = from ent in _ViewpointContext.vw_W360_EntityAdmin
                             join mf in _ViewpointContext.vw_W360_MasterFileSetup on ent.EntCode equals mf.RefCode
                             join deal in _ViewpointContext.vw_W360_Deal on ent.EntCode equals deal.EntCode
                             where mf.EntityAdmin == true && ent.IsCurrent
                             select new { ent, deal };

            var deals = dealsQuery.ToList();

            //get a list of all current deal dates
            var existingDealDates = _ViewpointContext.vw_W360_DealDates.Where(dd => dd.DealDate != null).ToList();

            //get the names for all possible deal dates
            var dealDateLookup = GetVPDealDateLookup();

            //get the list of valid intersections
            var matches = _db.DealDateMatches.Where(m => m.IsRequired).ToList();

            //work some magic to get a list of missing deals
            var query = from d in deals
                        join m in matches on new { d.deal.DealTypeCode, d.deal.DealStatusCode } equals new { m.DealTypeCode, m.DealStatusCode }
                        join l in dealDateLookup on m.DealDateCode equals l.FldKey
                        join ddt in existingDealDates on new { d.deal.EntCode, m.DealDateCode } equals new { ddt.EntCode, ddt.DealDateCode } into oj
                        from dd in oj.DefaultIfEmpty()
                        where dd == null || dd.DealDate == null
                        group new { d, l } by new { d.ent.EntCode, d.ent.SearchName, d.ent.CseOfficerCode, d.ent.RelationshipMgrCode } into entity
                        select entity;

            foreach (var ent in query)
            {
                results.Add(new ExceptionViolation
                {
                    ReferenceCode = ent.Key.EntCode,
                    ReferenceName = ent.Key.SearchName,
                    DefaultUserName = (new string[] { ent.Key.CseOfficerCode, ent.Key.RelationshipMgrCode }).JoinUnique(),
                    ReferenceObject = ReferenceObjectOptions.ViewpointEntity,
                    ReferenceDetail = string.Concat("<b>Missing Dates:</b><ul>", string.Concat(ent.ToList().Select(s => string.Concat("<li>", s.l.FldCaption, "</li>")).OrderBy(s => s).ToArray()), "</ul>")
                });
            }

            GetViewpointUser(ref results);

            return results;
        }

        /// <summary>
        /// get a list of all current deal dates
        /// </summary>
        /// <returns></returns>
        public List<vw_W360_UserFieldSetup> GetVPDealDateLookup()
        {
            return _ViewpointContext.vw_W360_UserFieldSetup.Where(l => l.FldType == 12).OrderBy(l => l.FldCaption).ToList();
        }

        public void DeleteDealDateMatches(string dealDateCode)
        {
            var ddm = _db.DealDateMatches.Where(d => d.DealDateCode == dealDateCode);

            _db.DealDateMatches.RemoveRange(ddm);
            _db.SaveChanges();
        }

        public void SaveDealDateMatches(List<DealDateMatch> dealDateMatches)
        {
            _db.DealDateMatches.AddRange(dealDateMatches);
            _db.SaveChanges();
        }

        public List<DealDateMatch> GetDealDateMatches(string dealDateCode)
        {
            return _db.DealDateMatches.Where(dd => dd.DealDateCode == dealDateCode).ToList();
        }



        #endregion

        #region General

        public void Save(object ob)
        {
            _db.SaveChanges();
        }

        /// <summary>
        /// Return a setting for a given key
        /// </summary>
        /// <param name="Key">The setting identifier</param>
        /// <returns></returns>
        public string GetSetting(string Key)
        {
            return _db.Settings.Where(s => s.Key == Key).Select(s => s.Value).SingleOrDefault();
        }

        /// <summary>
        /// Return a delimited setting for a given key as a list of strings
        /// </summary>
        /// <param name="Key">The setting identifier</param>
        /// <param name="Delimiter">The character used to delimit items</param>
        /// <returns></returns>
        public List<string> GetListSetting(string Key, char Delimiter = ',')
        {
            string setting = GetSetting(Key);
            return setting.SplitAndTrim();
        }

        public int GetIntSetting(string Key)
        {
            int result;
            string setting = GetSetting(Key);
            bool parsed = int.TryParse(setting, out result);
            if (!parsed)
            {
                throw new SettingsPropertyWrongTypeException(Key);
            }
            return result;
        }
        #endregion

        #region Emails

        public string GetCallBackUrl(string baseUrl, string portalCallBackPath)
        {
            return (baseUrl ?? "https://localhost:44300") + portalCallBackPath;
        }
        public string GetEmailMessage(string baseUrl,  string emailHeader, string emailFooter, string portalCallBackPath)
        {
            var callbackUrl = this.GetCallBackUrl(baseUrl, portalCallBackPath);
            //return emailHeader + " <a href=\"" + callbackUrl + "\">"+callbackUrl +"</a> " + emailFooter;
            return string.Format("<p>{0}</p><p><a href=\"{1}\">{1}</a></p><p>{2}</p>", emailHeader, callbackUrl, emailFooter);
        }

        public Task SendAsyncSMTP(Microsoft.AspNet.Identity.IdentityMessage identityMessage)
        {
            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage(this.GetSetting("Portal.SupportEmailAddress"), identityMessage.Destination, identityMessage.Subject, identityMessage.Body);
            var result = this.SendAsyncSMTP(mailMessage, null);
            return result;
        }
        public async Task<bool> SendAsyncSMTP(System.Net.Mail.MailMessage mail, User currentUser)
        {
            bool result = false;
            System.Net.Mail.SmtpClient client = null;
            try
            {
                // Configure the client:

                client = new System.Net.Mail.SmtpClient(this.GetSetting("smtpClientHost"));

                client.Port = this.GetIntSetting("smtpClientPort");

                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                System.Net.NetworkCredential credentials = System.Net.CredentialCache.DefaultNetworkCredentials;

                //client.EnableSsl = true;

                client.Credentials = credentials;

                // Send:            
                mail.IsBodyHtml = true;

                await client.SendMailAsync(mail);


                result = true;
            }
            catch (Exception ex)
            {
                ex.LogException();
                this.SendErrorMessage(mail, this.SendMessage, currentUser);
            }
            finally
            {
                if (client != null)
                {
                    client.Dispose();
                }
                if (mail != null)
                {
                    mail.Dispose();
                }

            }

            return result;
        }
        public void SendErrorMessage(System.Net.Mail.MailMessage mail, SendMessageDelegate sendMessageMethod, User currentUser)
        {
            if (currentUser != null)
            {
                MessageHtml message = new MessageHtml();
                message.To = currentUser;
                //message.From = mail.From;
                message.Subject = "Email error: Email failed to send.";
                message.Body = mail.Body;
                sendMessageMethod.Invoke(message);
            }
        }
        #endregion

        #region Client Portal

        public ICollection<vw_C360_MasterGroup> GetEntityMasterGroups()
        {
            return _ViewpointContext.vw_C360_MasterGroup.ToList();
        }
        public ICollection<vw_C360_MasterGroup> GetEntityMasterGroups(string term)
        {
            return _ViewpointContext.vw_C360_MasterGroup.Where(o => o.Name.Contains(term)).ToList();
        }
        public ICollection<vw_C360_SubGroup> GetEntitySubGroupsByName(string term)
        {
            return _ViewpointContext.vw_C360_SubGroup.Where(o => o.Name.Contains(term)).ToList();
        }
        public ICollection<vw_C360_SubGroup> GetEntitySubGroups(string entityGroupCode)
        {
            return _ViewpointContext.vw_C360_SubGroup.Where(o => o.MasterGroupCode == entityGroupCode).ToList();
        }
        public ICollection<vw_C360_Entity> GetEntityBySubGroup(string entityGroupCode, int limit)
        {
            return _ViewpointContext.vw_C360_Entity.Where(o => o.SubGroupCode == entityGroupCode).OrderBy(o => o.Name).Take(limit).ToList();
        }

        private IQueryable<vw_C360_Entity> GetEntities(string entityGroupCode, EntityGroupType type, bool onlyActive)
        {
            var query = from ent in _ViewpointContext.vw_C360_Entity
                        select ent;

            switch (type)
            {
                case EntityGroupType.MasterGroup: query = query.Where(e => e.MasterGroupCode == entityGroupCode); break;
                case EntityGroupType.SubGroup: query = query.Where(e => e.SubGroupCode == entityGroupCode); break;
                default: throw new ArgumentOutOfRangeException("type");
            }

            if (onlyActive)
            {
                query = query.Where(e => e.EntityStatusGroup == "Active");
            }

            return query;
        }

        private IQueryable<EntityRegistration> GetEntityRegistrations(string entityGroupCode, EntityGroupType type, bool onlyActive)
        {
            var query = from ent in _ViewpointContext.vw_C360_Entity
                        join reg in _ViewpointContext.vw_C360_Registration on ent.EntCode equals reg.EntCode
                        select new EntityRegistration { Entity = ent, Registration = reg };

            switch (type)
            {
                case EntityGroupType.MasterGroup: query = query.Where(e => e.Entity.MasterGroupCode == entityGroupCode); break;
                case EntityGroupType.SubGroup: query = query.Where(e => e.Entity.SubGroupCode == entityGroupCode); break;
                default: throw new ArgumentOutOfRangeException("type");
            }

            if (onlyActive)
            {
                query = query.Where(e => e.Entity.EntityStatusGroup == "Active" && e.Registration.IsActive);
            }

            return query;
        }

        List<vw_C360_Entity> IClientPortalRepository.GetEntityByTypeDetails(string entityGroupCode, EntityGroupType entityGroupType, string type)
        {
            return GetEntities(entityGroupCode, entityGroupType, true)
                    .Where(e => e.EntityType == type)
                    .ToList();
        }
        Dictionary<string, int> IClientPortalRepository.GetEntityByTypeSummary(string entityGroupCode, EntityGroupType entityGroupType)
        {
            return GetEntities(entityGroupCode, entityGroupType, true)
                        .GroupBy(e => e.EntityType)
                        .ToDictionary(g => g.Key, g => g.Count());
        }

        List<vw_C360_Entity> IClientPortalRepository.GetRegistrationByTypeDetails(string entityGroupCode, EntityGroupType entityGroupType, string type)
        {
            return GetEntityRegistrations(entityGroupCode, entityGroupType, true)
                    .Where(er => er.Registration.BusinessType == type)
                    .Select(er => er.Entity)
                    .ToList();
        }
        Dictionary<string, int> IClientPortalRepository.GetRegistrationByTypeSummary(string entityGroupCode, EntityGroupType entityGroupType)
        {
            return GetEntityRegistrations(entityGroupCode, entityGroupType, true)
                        .GroupBy(i => i.Registration.BusinessType)
                        .ToDictionary(g => g.Key, g => g.Count());
        }

        List<vw_C360_Entity> IClientPortalRepository.GetEntityByStatusDetails(string entityGroupCode, EntityGroupType entityGroupType, string status)
        {
            return GetEntities(entityGroupCode, entityGroupType, false)
                        .Where(er => er.EntityStatusGroup == status)
                        .ToList();
        }
        Dictionary<string, int> IClientPortalRepository.GetEntityByStatusSummary(string entityGroupCode, EntityGroupType entityGroupType)
        {
            return GetEntities(entityGroupCode, entityGroupType, onlyActive: false)
                .Where(e => !string.IsNullOrEmpty(e.EntityStatusGroup))
                .GroupBy(g => g.EntityStatusGroup)
                .ToDictionary(gg => gg.Key, gg => gg.Count());
        }

        public List<AgeBucket> GetAgeBuckets()
        {
            List<AgeBucket> result = new List<AgeBucket>();

            result.Add(new AgeBucket { Name = "0-30 Days", Min = 0, Max = 30 });
            result.Add(new AgeBucket { Name = "31-90 Days", Min = 31, Max = 90 });
            result.Add(new AgeBucket { Name = "91-180 Days", Min = 91, Max = 180 });
            result.Add(new AgeBucket { Name = "181-365 Days", Min = 181, Max = 365 });
            result.Add(new AgeBucket { Name = "366+ Days", Min = 365, Max = int.MaxValue });
            result.Add(new AgeBucket { Name = "Total", Min = 0, Max = int.MaxValue });

            return result;
        }

        Dictionary<string, int> IClientPortalRepository.GetAgedARSummary(string entityGroupCode, EntityGroupType entityGroupType)
        {
            List<AgeBucket> buckets = GetAgeBuckets();

            Random r = new Random();

            return buckets.ToDictionary(b => b.Name, b => r.Next(100000));
        }



        ICollection<Deadline> IClientPortalRepository.GetEntityGroupDeadlines(string entityGroupCode, EntityGroupType entityGroupType)
        {
            var entities = GetEntityRegistrations(entityGroupCode, entityGroupType, onlyActive: true);

            var dt = _db.DeadlineTypes.Include(d => d.Registrations).Include(d => d.EntityTypes).Where(d => d.Enabled);

            var result = GetEntityDeadlineInternal(entities.ToList(), dt.ToList());

            return result.Where(d => d.PublishDate.HasValue).ToList();
        }

        ICollection<Deadline> IClientPortalRepository.GetEntityGroupDeadline(string entityGroupCode, EntityGroupType entityGroupType, int deadlineTypeId, DateTime date)
        {
            var entities = GetEntityRegistrations(entityGroupCode, entityGroupType, onlyActive: true);

            var dt = _db.DeadlineTypes.Include(d => d.Registrations).Include(d => d.EntityTypes).Where(t => t.Enabled && t.Id == deadlineTypeId);

            var result = GetEntityDeadlineInternal(entities.ToList(), dt.ToList());

            return result.Where(d => d.PublishDate.HasValue && d.PublishDate.Value == date).ToList();
        }

        /// <summary>
        /// Return a list of deadlines for the given deadline types and entities.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="deadlineTypes"></param>
        /// <returns></returns>
        private ICollection<Deadline> GetEntityDeadlineInternal(List<EntityRegistration> entities, List<DeadlineType> deadlineTypes)
        {
            var q = from d in deadlineTypes
                    from e in entities
                    where
                        d.EntityTypes.Select(o => o.EntityTypeCode).Contains(e.Entity.EntityTypeCode) &&
                        d.Registrations.Select(r => r.RegistrationTypeCode).Contains(e.Registration.BusinessTypeCode)
                    group e by new { e.Entity.EntCode, d } into g
                    select new Deadline { Entity = g.First().Entity, Type = g.Key.d };

            return q.ToList();
        }

        public List<DeadlineType> GetDeadlineTypes()
        {
            return _db.DeadlineTypes.ToList();
        }

        public DeadlineType GetDeadlineType(int id)
        {
            return _db.DeadlineTypes
                .Include(t => t.Registrations)
                .Include(t => t.EntityTypes)
                .Include(t => t.EntityStatuses)
                .First(t => t.Id == id);
        }

        public void SaveDeadlineType(DeadlineType entity)
        {
            if (entity.Id == 0)
            {
                //if this is a new item, add it
                _db.DeadlineTypes.Add(entity);
            }
            else
            {
                //if this is an existing item, update the original
                var entityInDb = GetDeadlineType(entity.Id);
                _db.Entry(entityInDb).CurrentValues.SetValues(entity);

                //we brute force clear and add the registration types
                //this can be made more elegant if the child object becomes more complex
                entityInDb.Registrations.Clear();
                if (entity.Registrations != null)
                {
                    foreach (var t in entity.Registrations)
                        entityInDb.AddRegistration(t.RegistrationTypeCode);
                }

                entityInDb.EntityTypes.Clear();
                if (entity.EntityTypes != null)
                {
                    foreach (var t in entity.EntityTypes)
                        entityInDb.AddEntityType(t.EntityTypeCode, t.Jurisdiction);
                }
            }

            _db.SaveChanges();
        }

        public void DeleteDeadlineType(int id)
        {
            _db.DeadlineTypes.Remove(_db.DeadlineTypes.Single(t => t.Id == id));
            _db.SaveChanges();
        }

        User IClientPortalRepository.Link360User(ApplicationUser portalUser)
        {// get the 360 user linked to this Client portal logon 
            User results;
            //for now just make a new user -- could go find a user to connect in future logic
            results = new User() { Email = portalUser.Email, DisplayName = portalUser.Email, Username = portalUser.Email };
            return results;
        }
        void IClientPortalRepository.AddNewClientPortalUser(ApplicationUser portalUser)
        {
            var user = this.GetUser(portalUser.UserName); //get the saved version of the user so that the add will work properly

            user.AssignDefaultRoles(this.GetRole, RoleDefaultType.ClientPortal);
            if (user.Roles != null)
            {
                foreach (var item in user.Roles)
                {
                    System.Data.Entity.Infrastructure.DbEntityEntry dbEntityParentEntry = _db.Entry(item);

                    switch (dbEntityParentEntry.State)
                    {
                        case EntityState.Detached:
                            _db.RoleUsers.Add(item);
                            // _db.RoleUsers.Attach(item);
                            break;
                        case EntityState.Added:
                            _db.RoleUsers.Add(item);
                            break;
                    }
                }
            }
            this.Save(user);
        }
        public void DisableClientPortalUser(int userId)
        {
            //var user = this.GetUser(userId);
            var role = this.GetRole("Client Portal User");
            role.RemoveUser(userId);
            this.Save(role);
        }
        public void AddPortalGroupVisibility(User user, string selectedGroups, EntityGroupType groupType)
        {
            this.AddPortalGroupVisibility(user.Id, selectedGroups, groupType);
        }
        public void AddPortalGroupVisibility(int userId, string selectedGroups, EntityGroupType groupType)
        {
            foreach (var code in this.GetGroupFromCommaDelimitedId(selectedGroups))
            {

                PortalUserGroupVisibility visibleGroup = new PortalUserGroupVisibility() { EntityGroupType = groupType, GroupCode = code, UserId = userId };
                _db.PortalUserGroupVisibilities.Add(visibleGroup);
                this.Save(visibleGroup);
            }
        }
        private void AddPortalGroupVisibility(PortalUserGroupVisibility groupVisibility) { _db.PortalUserGroupVisibilities.Add(groupVisibility); }
        public void ReassignPortalGroupVisibility(int userId, string selectedGroups, EntityGroupType groupType)
        {
            var selected = this.GetGroupFromCommaDelimitedId(selectedGroups);
            var current = this.GetPortalUserGroupVisibility(userId, groupType);
            //find new records and add them
            var newVisibility = selected.Where(s => !current.Select(c => c.GroupCode).Contains(s)).ToList();
            foreach (var groupVisibility in newVisibility)
            {
                this.AddPortalGroupVisibility(new PortalUserGroupVisibility { GroupCode = groupVisibility, EntityGroupType = groupType, UserId = userId });
            }

            var removed = current.Where(c => !selected.Contains(c.GroupCode)).ToList();
            foreach (var deleteVisibility in removed)
            {
                _db.PortalUserGroupVisibilities.Remove(deleteVisibility);
            }
            _db.SaveChanges();
        }
        public List<string> GetGroupFromCommaDelimitedId(string CommaDelimitedIds)
        {
            List<string> result = new List<string>();

            if (!string.IsNullOrWhiteSpace(CommaDelimitedIds))
            {
                List<string> ids = CommaDelimitedIds.SplitAndTrim();
                foreach (string item in ids)
                {

                    result.Add(item);

                }
            }
            return result;
        }
        public ICollection<PortalUserGroupVisibility> GetPortalUserVisibilityWithGroupName(EntityGroupType groupType)
        {
            var mGroups = this.GetEntityGroups(groupType);
            var results = from v in this.GetPortalUserGroupVisibility(groupType)
                          join g in mGroups on v.GroupCode equals g.GroupCode
                          select new PortalUserGroupVisibility { GroupName = g.GroupName, EntityGroupType = v.EntityGroupType, GroupCode = v.GroupCode, UserId = v.UserId, User = v.User };

            return results.ToList();
        }
        public ICollection<PortalUserGroupVisibility> GetPortalUserVisibilityWithGroupName(int userId, EntityGroupType groupType)
        {
            var mGroups = this.GetEntityGroups(groupType);
            var results = from v in this.GetPortalUserGroupVisibility(userId, groupType)
                          join g in mGroups on v.GroupCode equals g.GroupCode
                          select new PortalUserGroupVisibility { GroupName = g.GroupName, EntityGroupType = v.EntityGroupType, GroupCode = v.GroupCode, UserId = v.UserId, User = v.User };

            return results.ToList();
        }

        /// <summary>
        /// Determine if a group can be seen by a given user
        /// </summary>    
        public bool IsPortalUserGroupVisible(int userId, string groupCode, EntityGroupType groupType)
        {
            bool result = false;

            if (groupCode != null)
            {
                //check if they have been explicitly granted access to the group
                result = _db.PortalUserGroupVisibilities.Any(u => u.UserId == userId && u.GroupCode == groupCode && u.EntityGroupType == groupType);

                if (!result && groupType == EntityGroupType.SubGroup)
                {
                    //access to a sub group is inherited from the master group     
                    var mgCode = _ViewpointContext.vw_C360_SubGroup.Where(g => g.SubGroupCode == groupCode).Select(g => g.MasterGroupCode).FirstOrDefault();
                    result = IsPortalUserGroupVisible(userId, mgCode, EntityGroupType.MasterGroup);
                }
            }

            return result;
        }

        /// <summary>
        /// Determine if an entity can be seen by a given user
        /// </summary>    
        public bool IsPortalUserEntityVisible(int userId, string entCode)
        {
            //access to an entity is inherited from the sub group   
            var sgCode = _ViewpointContext.vw_C360_Entity.Where(e => e.EntCode == entCode).Select(e => e.SubGroupCode).FirstOrDefault();
            return IsPortalUserGroupVisible(userId, sgCode, EntityGroupType.SubGroup);
        }

        public ICollection<PortalUserGroupVisibility> GetPortalUserGroupVisibility(int userId, EntityGroupType groupType)
        {
            var results = _db.PortalUserGroupVisibilities.Where(u => u.UserId == userId && u.EntityGroupType == groupType);
            return results.ToList();
        }
        public ICollection<PortalUserGroupVisibility> GetPortalUserGroupVisibility(EntityGroupType groupType)
        {
            var results = _db.PortalUserGroupVisibilities.Where(u => u.EntityGroupType == groupType);
            return results.ToList();
        }
        public ICollection<PortalUserGroupVisibility> GetPortalUserGroupVisibility(int userId)
        {
            var results = _db.PortalUserGroupVisibilities.Where(u => u.UserId == userId);
            return results.ToList();
        }

        public ICollection<string> GetPortalUserEntityVisibility(int userId)
        {
            List<string> results = new List<string>();
            var entities = _ViewpointContext.vw_C360_Entity;
            var groupVisibility = this.GetPortalUserGroupVisibility(userId).ToList();

            var masterGroupEnts = from groups in groupVisibility
                                  join ent in entities on new { groups.GroupCode, groups.EntityGroupType } equals new { GroupCode = ent.MasterGroupCode, EntityGroupType = EntityGroupType.MasterGroup }
                                  select ent.EntCode;
            results.AddRange(masterGroupEnts.ToList());

            var subGroupEnts = from groups in groupVisibility
                               join ent in entities on new { groups.GroupCode, groups.EntityGroupType } equals new { GroupCode = ent.SubGroupCode, EntityGroupType = EntityGroupType.SubGroup }
                               select ent.EntCode;
            results.AddRange(subGroupEnts.ToList());

            return results.ToList();
        }

        public ICollection<vw_C360_Entity> GetPortalUserEntityVisibility(int userId, string searchTerm)
        {
            List<vw_C360_Entity> result;

            List<string> mg = GetPortalUserGroupVisibility(userId, EntityGroupType.MasterGroup).Select(g => g.GroupCode).ToList();
            List<string> sg = GetPortalUserGroupVisibility(userId, EntityGroupType.SubGroup).Select(g => g.GroupCode).ToList();

            result = _ViewpointContext.vw_C360_Entity
                .Where(e => e.Name.Contains(searchTerm) && (mg.Contains(e.MasterGroupCode) || sg.Contains(e.SubGroupCode)))
                .OrderBy(e => e.Name)
                .ToList();

            return result;
        }

        public EntityGroup GetEntityGroup(string entityGroupCode, EntityGroupType type)
        {
            EntityGroup result;

            switch (type)
            {
                case EntityGroupType.MasterGroup:
                    var mg = _ViewpointContext.vw_C360_MasterGroup.Single(g => g.MasterGroupCode == entityGroupCode);
                    result = new EntityGroup { GroupName = mg.Name, GroupCode = mg.MasterGroupCode, Type = type };
                    break;
                case EntityGroupType.SubGroup:
                    var sg = _ViewpointContext.vw_C360_SubGroup.Single(g => g.SubGroupCode == entityGroupCode);
                    result = new EntityGroup { GroupName = sg.Name, GroupCode = sg.SubGroupCode, Type = type };
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return result;
        }

        public ICollection<vw_C360_InvoiceHeader> GetGroupInvoices(string groupName, EntityGroupType groupType)
        {
            var results = from inv in _ViewpointContext.vw_C360_InvoiceHeader
                          join ent in _ViewpointContext.vw_C360_Entity on inv.RefCode equals ent.EntCode
                          where groupName == (groupType == EntityGroupType.MasterGroup ? ent.MasterGroupCode : (groupType == EntityGroupType.SubGroup ? ent.SubGroupCode : string.Empty))
                          select inv;
            return results.ToList();
        }

        public ICollection<EntityGroup> GetEntityGroups(EntityGroupType type)
        {
            List<EntityGroup> results = new List<EntityGroup>();
            if (type == EntityGroupType.MasterGroup)
            {
                var masterGroups = from mg in _ViewpointContext.vw_C360_MasterGroup select new EntityGroup { GroupCode = mg.MasterGroupCode, GroupName = mg.Name, Type = EntityGroupType.MasterGroup };
                results.AddRange(masterGroups.Distinct().ToList());
            }
            if (type == EntityGroupType.SubGroup)
            {
                var subGroups = from sg in _ViewpointContext.vw_C360_SubGroup
                                group sg by sg.SubGroupCode into grp
                                select new EntityGroup { GroupCode = grp.Key, GroupName = grp.Max(g => g.Name), Type = EntityGroupType.SubGroup }; results.AddRange(subGroups.ToList());
            }
            return results;
        }

        /// <summary>
        /// Return a list of entity groups that a user can see
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ICollection<EntityGroup> GetEntityUserGroups(int userId)
        {
            List<EntityGroup> result = new List<EntityGroup>();
            List<PortalUserGroupVisibility> groups = new List<PortalUserGroupVisibility>();

            groups.AddRange(this.GetPortalUserVisibilityWithGroupName(userId, EntityGroupType.MasterGroup));
            groups.AddRange(this.GetPortalUserVisibilityWithGroupName(userId, EntityGroupType.SubGroup));

            if (groups.Count > 0)
            {

                foreach (var item in groups)
                {
                    result.Add(new EntityGroup { GroupCode = item.GroupCode, GroupName = item.GroupName, Type = item.EntityGroupType });

                    if (item.EntityGroupType == EntityGroupType.MasterGroup)
                    {

                        var subGroups = from sg in _ViewpointContext.vw_C360_SubGroup
                                        where sg.MasterGroupCode == item.GroupCode
                                        select new EntityGroup { GroupCode = sg.SubGroupCode, GroupName = sg.Name, Type = EntityGroupType.SubGroup };

                        result.AddRange(subGroups.ToList());
                    }
                }
            }
            return result;
        }

        public ICollection<EntityGroup> GetAllEntityGroups(string id)
        {
            List<EntityGroup> result = new List<EntityGroup>();
            var subGroups = from sg in _ViewpointContext.vw_C360_SubGroup
                            where sg.MasterGroupCode == id
                            select new EntityGroup { GroupCode = sg.SubGroupCode, GroupName = sg.Name, Type = EntityGroupType.SubGroup };

            result.AddRange(subGroups.ToList());

            return result;
        }

        public FileStore GetCompressedFile(string entityGroupCode, EntityGroupType entityGroupType)
        {
            FileStore result = new FileStore { FileName = string.Concat("W360-Invoices-", entityGroupCode, ".zip"), MimeType = "application/zip" };

            var invoices = GetGroupInvoices(entityGroupCode, entityGroupType);

            using (MemoryStream zipStream = new MemoryStream())
            {
                using (ZipArchive zip = new ZipArchive(zipStream, ZipArchiveMode.Create, true))
                {
                    foreach (var invoice in invoices)
                    {
                        var file = GetFile(invoice.RefCode, invoice.InvoiceNr.ToString());
                        if (file != null && file.Count() > 0)
                        {
                            zip.CreateEntryFromFile(file.First().FileName, file.First().OutputFileName );
                        }
                    }
                }

                result.FileContent = zipStream.ToArray();
            }

            return result;
        }

        public IEnumerable<FileStore> GetFile(string refCode, string identifier)
        {
            string directoryName = this.GetSetting("InvoiceDirectoryPath");
            IEnumerable<FileStore> files = this.GetFileFromDirectory(refCode, identifier, directoryName, true);

            return files;
        }

        private string GetSearchString(string refCode, string identifier)
        {
            const string wildcard = "*";
            const string delimiter = " ";
            return wildcard + delimiter + refCode + delimiter + wildcard + delimiter + identifier + delimiter + wildcard;
        }

        public IEnumerable<FileStore> GetFileFromDirectory(string refCode, string identifier, string directoryName, bool stopAfterFirstFound)
        {
            List<FileStore> result = new List<FileStore>();

            try
            {
                string searchString = this.GetSearchString(refCode, identifier);
                var files = Directory.EnumerateFiles(directoryName, searchString, SearchOption.AllDirectories);
                foreach (var filename in files)
                {
                    result.Add(new FileStore { FileName = filename, MimeType = "application/pdf", RefCode = refCode, OutputFileName = string.Concat("W360-Invoice-", refCode, "-", identifier, ".pdf") });
                    if (stopAfterFirstFound)
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.LogException();
            }

            return result;
        }

        /// <summary>
        /// Return a remote file using the current network credentials
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public FileStore GetFileFromUrl(string url)
        {
            FileStore result = null;

            if (!string.IsNullOrEmpty(url))
            {
                try
                {
                    using (WebClient client = new WebClient())
                    {
                        client.Credentials = CredentialCache.DefaultCredentials;

                        result = new FileStore();
                        result.FileContent = client.DownloadData(url);
                        result.MimeType = client.ResponseHeaders[HttpResponseHeader.ContentType];
                        result.FileName = "file";

                        // Try to extract the filename from the Content-Disposition header
                        if (!String.IsNullOrEmpty(client.ResponseHeaders["Content-Disposition"]))
                            result.FileName = client.ResponseHeaders["Content-Disposition"].Substring(client.ResponseHeaders["Content-Disposition"].IndexOf("filename=") + 10).Replace("\"", "");

                        else
                            result.FileName = "file";
                    }
                }
                catch (Exception ex)
                {
                    ex.LogException();
                    //return null which should redirect to a page not found
                    result = null;
                }
            }

            return result;
        }

        public List<OfficerSummary> GetOfficerSummary(string entityGroupCode, EntityGroupType entityGroupType, string officerType)
        {
            var entities = this.GetEntities(entityGroupCode, entityGroupType, false);

            var q = from e in entities
                    join o in _ViewpointContext.vw_C360_Officer on e.EntCode equals o.EntCode
                    where o.OfficerType == officerType
                    let isActive = (o.Status == "A" && e.EntityStatusGroup != "Closed")
                    group new { o, isActive } by o.OfficerEntCode into g
                    select new OfficerSummary
                    {
                        OfficerName = g.FirstOrDefault().o.OfficerName,
                        OfficerType = g.FirstOrDefault().o.OfficerTypeText,
                        OfficerTypeCode = g.FirstOrDefault().o.OfficerType,
                        OfficerEntCode = g.FirstOrDefault().o.OfficerEntCode,
                        Active = g.Count(w => w.isActive),
                        Resigned = g.Count(w => !w.isActive)
                    };

            return q.Where(e => e.Active > 0).OrderBy(e => e.OfficerName).ToList();
        }

        public List<OfficerAppointment> GetOfficerDetails(string entityGroupCode, EntityGroupType entityGroupType, string officerType, string officerEntCode)
        {
            var entities = this.GetEntities(entityGroupCode, entityGroupType, false);

            var q = from e in entities
                    join o in _ViewpointContext.vw_C360_Officer on e.EntCode equals o.EntCode
                    where o.OfficerType == officerType && o.OfficerEntCode == officerEntCode
                    orderby e.Name
                    select new OfficerAppointment { Officer = o, Entity = e };

            return q.ToList();
        }

        public vw_C360_Entity GetEntity(string entCode)
        {
            return _ViewpointContext.vw_C360_Entity.Where(e => e.EntCode == entCode).FirstOrDefault();
        }

        public vw_C360_MasterGroup GetEntityMasterGroup(string entityGroupCode)
        {
            return _ViewpointContext.vw_C360_MasterGroup.Where(t => t.MasterGroupCode == entityGroupCode).FirstOrDefault();
        }

        public vw_C360_SubGroup GetEntitySubgroup(string entityGroupCode)
        {
            return _ViewpointContext.vw_C360_SubGroup.Where(t => t.SubGroupCode == entityGroupCode).FirstOrDefault();
        }

        public List<vw_C360_Addresses> GetEntityAddresses(string entCode)
        {
            return _ViewpointContext.vw_C360_Addresses.Where(a => a.RefCode == entCode).ToList();
        }

        public List<vw_C360_Registration> GetEntityRegistrations(string entCode)
        {
            return _ViewpointContext.vw_C360_Registration.Where(r => r.EntCode == entCode).ToList();
        }

        public List<vw_C360_ShareClass> GetEntityShareClasses(string entCode)
        {
            return _ViewpointContext.vw_C360_ShareClass.Where(s => s.EntCode == entCode).ToList();
        }

        public List<vw_C360_Shareholders> GetEntityShareholders(string entCode)
        {
            return _ViewpointContext.vw_C360_Shareholders.Where(s => s.EntCode == entCode).ToList();
        }

        public List<vw_C360_Officer> GetEntityOfficers(string entCode)
        {
            return _ViewpointContext.vw_C360_Officer.Where(o => o.EntCode == entCode).ToList();
        }

								public iCal GetiCalEvent(string title, DateTime duedate, string UUID)
								{
												var des = _db.DeadlineTypes.Where(t => t.Enabled && t.Id.ToString() == UUID).Select(t => t.Description).SingleOrDefault();
												//HTML to PLAIN TEXT
												string htmlTagPattern = "<.*?>";
												var regexCss = new Regex("(\\<script(.+?)\\</script\\>)|(\\<style(.+?)\\</style\\>)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
												des = des.Replace(System.Environment.NewLine, string.Empty);
												des = regexCss.Replace(des, string.Empty);
												des = Regex.Replace(des, htmlTagPattern, string.Empty);
												des = Regex.Replace(des, @"^\s+$[\r\n]*", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
												des = Regex.Replace(des, "<.*?>", string.Empty);
												des = des.Replace("\\<span.*?\\>.*?\\<\\/span\\>", "");
												des = des.Replace("&nbsp;", string.Empty);
												des = System.Text.RegularExpressions.Regex.Replace(des, @"\s+", " ");
												string url = GetSetting("Portal.BaseUrl");
												var description = des + " " + url;

												iCal results = new iCal();
												var iCal = new iCalendar
												{
																Method = "PUBLISH",
																Version = "2.0"
												};

												var evt = iCal.Create<Event>();
												evt.Summary = title;
												evt.Start = new iCalDateTime(duedate);
												evt.Duration = TimeSpan.FromHours(10);
												evt.Description = description;
												evt.Location = "Walkers 360 System Reminder";
												evt.IsAllDay = true;
												evt.UID = String.IsNullOrEmpty(UUID) ? new Guid().ToString() : UUID;
												evt.Alarms.Add(new Alarm
												{
																Duration = new TimeSpan(0, 15, 0),
																Trigger = new Trigger(new TimeSpan(0, 15, 0)),
																Action = AlarmAction.Display,
																Description = "Reminder"
												});

												ISerializationContext ctx = new SerializationContext();
												ISerializerFactory factory = new DDay.iCal.Serialization.iCalendar.SerializerFactory();
												// Get a serializer for our object
												IStringSerializer serializer = factory.Build(iCal.GetType(), ctx) as IStringSerializer;

												string output = serializer.SerializeToString(iCal);
												var contentType = "text/calendar";
												var bytes = Encoding.UTF8.GetBytes(output);
												var FIO = "X-MS-OLK-FORCEINSPECTOROPEN = TRUE";

												results.bytes = bytes;
												results.contentType = contentType;
												results.DownloadFileName = "Walkers 360 Calendar.ics";
												results.FIO = FIO;

												return results;
								}
        #endregion

        #region IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    //dispose of objects here
                    _db.Dispose();
                    _ViewpointContext.Dispose();
                    _AderantContext.Dispose();

                    if (_clearview != null)
                        _clearview.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
