﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Walkers.Project360.Model;

namespace Walkers.Project360.CardGames
{
    public class ScheduledProcessEvaluator: ISuitEvalBehavior
    {
        public  void Evaluate(Suit suit, ISuitRepository repository)
        {
            repository.RunProcess(suit);
         }

       
    
    }

    
}
