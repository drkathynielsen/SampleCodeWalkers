﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Walkers.Project360.Model;

namespace Walkers.Project360.CardGames
{
    /// <summary>
    /// Evaluator behavior used for  unit test
    /// </summary>
    public class MissingTestValueEvaluator: ExceptionEvaluator
    {
        public override void Evaluate(Suit suit, ISuitRepository repository)
        {
            base.Evaluate(suit, repository);
        }

        public override List<ExceptionViolation> GetViolators(Suit suit, ISuitRepository repository)
        {
            var results = from row in ((Walkers.Project360.DataAccess.Repository)repository).GetMissingTestRows()
                          select new ExceptionViolation { ReferenceCode = row.Id.ToString(), ReferenceName = "a Name I hard coded in", ReferenceObject = ReferenceObjectOptions.TestData };
            return results.ToList();
        }
    
    }

    
}
