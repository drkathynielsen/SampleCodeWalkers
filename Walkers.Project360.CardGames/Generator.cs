﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Walkers.Project360.Model;
using Walkers.Project360.DataAccess;
using MCS.McsLogging;

namespace Walkers.Project360.CardGames
{
    public class Generator
    {
        public void Start(DateTime currentTime)
        {
            Repository repo = new Repository();

            List<Suit> toProcess = GetSuitsToProcess( repo.GetSuits().ToList(), currentTime).ToList();
            foreach(var suit in toProcess)
            {   //don't run the error checking suit until last
                if (suit.EvalBehaviorName != "ErrorDuringSuitAutomation")
                {
                    try
                    {
                        //check to see if we disabled the suit while this loop is running
                        if (repo.GetSuit(suit.Id).Enabled)
                            ProcessSuit(suit, repo);
                    }
                    catch (Exception ex)
                    {
                        suit.AutomationException = ex;
                        ex.LogException();
                    }
                }
            }
            //run the suit that makes a card for any erroring suits
            Suit errorSuit = toProcess.Where(s=> s.EvalBehaviorName == "ErrorDuringSuitAutomation").FirstOrDefault();
            if (errorSuit != null)
            {
                ProcessSuit(errorSuit, repo);
            }
            else
            {
                var ex = new InvalidOperationException("ErrorDuringSuitAutomation doesn't exist");
                ex.LogException();
            }
        }

        public List<Suit> GetSuitsToProcess(List<Suit> suits, DateTime currentTime)
        {//selects only those suits that are due to be run based on frequency of running and are enabled
            List<Suit> result = suits.Where(s=> s.Enabled && s.NextRunDate <= currentTime && s.IsRunning == false).ToList()  ;
            return result;
        }
        

        public void ProcessSuit(Suit suit, ISuitRepository repository)
        {
            //make sure there is an eval behavior
             if (suit.EvalBehavior == null)
            {
                SimpleSuitEvalBehaviorFactory factory = new SimpleSuitEvalBehaviorFactory();
                suit.EvalBehavior = factory.GetSuitBehavior(suit);
            }
            //then run it
            if (suit.EvalBehavior != null)
            {
                try
                {
                    repository.SaveRunningSuit(suit);

                    suit.EvalBehavior.Evaluate(suit, repository);
                }
                catch (Exception ex)
                {
                    suit.AutomationException = ex;
                    ex.LogException();
                }
                finally
                {
                    repository.SaveFinishedSuit(suit);
                }
            }
        }
    }

    public class SimpleSuitEvalBehaviorFactory
    {
        public ISuitEvalBehavior GetSuitBehavior(Suit suit)
        {
           
            //most suits can use the basic Exception evaluator 
            ISuitEvalBehavior result=null;
            switch (suit.Type)
            {
                case SuitType.Request:
                    result = new RequestSuitEvaluator();
                    break;
                case SuitType.ScheduledProcess:
                    result = new ScheduledProcessEvaluator();
                    break;
                default:
                    if (suit.EvalBehaviorName == "MissingTestValueEvaluator")
                    {
                        result = new MissingTestValueEvaluator();
                    }
                    else
                    {
                        result = new BasicExceptionEvaluator();
                    }
                    break;
            }
            
            return result;
        }
    }
}
