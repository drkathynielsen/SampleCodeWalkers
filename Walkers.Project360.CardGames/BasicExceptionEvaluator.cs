﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Walkers.Project360.Model;

namespace Walkers.Project360.CardGames
{
    public class BasicExceptionEvaluator: ExceptionEvaluator
    {
        
            public override List<ExceptionViolation> GetViolators(Suit suit, ISuitRepository repository)
            {
                return repository.GetViolations(suit);
            }
        
    }
}
