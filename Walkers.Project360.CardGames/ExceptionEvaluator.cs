﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Walkers.Project360;
using Walkers.Project360.Model;
using Walkers.Project360.DataAccess;
using MCS.McsLogging;

namespace Walkers.Project360.CardGames
{
  


    public abstract class ExceptionEvaluator : ISuitEvalBehavior
    {   //This is an evaluation behavior that will be used for many exceptions
        //Evaluation is the behavior that checks whether there are new card to be made for exceptions 
        //      and auto completes any that have been resolved
        //if a suit doesn't follow the rule of being able to identify completed and new cards in this way, then make and assign a different ISuitEvalBehavior behavior
        private List<Card> existingSuitCards;
        private List<Card> completedCards;
        private List<ExceptionViolation> newViolations;
        private List<Card> newCards= new List<Card>();
        private List<Card> updatedCards;

        public virtual void Evaluate(Suit suit, ISuitRepository repository)
        {   //uses the template pattern because most steps will be the same for all exceptions
            //the main thing that may differ is the method that for getting the current list of violations to the rule of the suit
            User systemUser = repository.GetSystemUser();

            List<ExceptionViolation> exceptions = GetViolators(suit, repository);
            
            existingSuitCards = GetExistingSuitCards(suit, repository);
            
            updatedCards = GetUpdatedCards(exceptions, existingSuitCards);

            SaveUpdatedCards(updatedCards, repository);    
            
            completedCards = FindCompletedCards(exceptions, existingSuitCards);
            
            CompleteCards(ref completedCards, repository, systemUser);
            
            newViolations = FindNewViolators(exceptions, existingSuitCards);
            
            newCards = CreateExceptionCards(newViolations, suit, repository, systemUser );
        }

        public abstract List<ExceptionViolation> GetViolators(Suit suit, ISuitRepository repository);

       #region template methods that are steps that will likely be shared across suits

        public virtual List<Card> GetExistingSuitCards(Suit suit, ISuitRepository repo)
        {
            return repo.GetSuitCards(suit);
        }
        public virtual void MakeCardCreateEvent(List<Card> newCards, ISuitRepository repository, User systemUser)
        {

            foreach (var card in newCards)
            {
                card.AddEvent(CardEventType.Created, systemUser, "Card created.", null);
                repository.SaveCard(card);
            }
        }
        public virtual List<Card> FindCompletedCards(List<ExceptionViolation> exceptions, List<Card> existingSuitCards)
        {//get the existing cards that are no longer exceptions
           var result = from existingSuitCard in existingSuitCards
                         join exception in exceptions on existingSuitCard.UniqueId equals exception.UniqueIdentifier into gj
                        from sc in gj.DefaultIfEmpty()
                        where sc == null
                        select existingSuitCard;

            return result.ToList();
         }
        /// <summary>
        /// get the exception that are new
        /// </summary>
        /// <param name="exceptions"></param>
        /// <param name="existingSuitCards"></param>
        /// <returns></returns>
        public virtual List<ExceptionViolation> FindNewViolators(List<ExceptionViolation> exceptions, List<Card> existingSuitCards)
        {
            var result = from  exception in exceptions
                         join existingSuitCard in existingSuitCards on   exception.UniqueIdentifier equals existingSuitCard.UniqueId into gj
                         from sc in gj.DefaultIfEmpty()
                         where sc == null
                         select exception;

            return result.ToList();
        }
        /// <summary>
        /// get the cards that already exist and are still violations
        /// </summary>
        /// <param name="exceptions"></param>
        /// <param name="existingSuitCards"></param>
        /// <returns></returns>
        public virtual List<Card> FindRemaining(List<ExceptionViolation> exceptions, List<Card> existingSuitCards)
        {//
            var result = from exception in exceptions
                         join existingSuitCard in existingSuitCards on exception.UniqueIdentifier equals existingSuitCard.UniqueId
                         select existingSuitCard;

            return result.ToList();
        }
        public virtual void SaveUpdatedCards(List<Card> cards, ISuitRepository repository)
        {
            if (cards != null && cards.Count > 0)
            {
                try
                {
                    repository.SaveCards(cards);
                }
                catch (Exception ex)
                {
                    Card firstCard = cards.First();
                    firstCard.Suit.AutomationException = ex;
                    ex.LogException("Error occurred with updating cards in suit " + cards.First().Suit.Id + " for: " + firstCard.Suit.EvalBehaviorName);
                }
            }
        }
        public virtual List<Card> GetUpdatedCards(List<ExceptionViolation> violationRefresh, List<Card> existingSuitCards)
        {//
            List<Card> result = new List<Card>();
            var usersChanged = from refresh in violationRefresh
                               join existingSuitCard in existingSuitCards on refresh.UniqueIdentifier equals existingSuitCard.UniqueId
                               let currentUsers = existingSuitCard.Users.OrderBy(u =>u.User.Id).Select(u => u.User.Id)
                               let newUsers = refresh.DefaultUsers.OrderBy(u => u.Id).Select(u => u.Id)          
                               where !existingSuitCard.Events.Any(e => e.Type == CardEventType.Reassigned) && !currentUsers.SequenceEqual(newUsers)
                               select new { existingSuitCard, refresh, currentUsers, newUsers };

            foreach (var update in usersChanged)
            {
                update.existingSuitCard.ReassignUsers(update.refresh.DefaultUsers);
                result.Add(update.existingSuitCard);
            }

            var infoChanged =  from violation in violationRefresh
                               join existingSuitCard in existingSuitCards on violation.UniqueIdentifier equals existingSuitCard.UniqueId
                               where existingSuitCard.ReferenceName != violation.ReferenceName
                                    || existingSuitCard.ReferenceDetail != violation.ReferenceDetail
                               select new { existingSuitCard, violation };

            foreach (var update in infoChanged)
            {
                update.existingSuitCard.ReferenceName = update.violation.ReferenceName;
                update.existingSuitCard.ReferenceDetail = update.violation.ReferenceDetail;
                result.Add(update.existingSuitCard);
            }
            return result;
        }
        public virtual void CompleteCards(ref List<Card> completedCards, ISuitRepository repository, User systemUser)
        {
            foreach (var card in completedCards)
            {
                try
                {
                    card.Complete(systemUser);
                    
                    repository.SaveCard(card);
                }
                catch (Exception ex)
                {
                    card.Suit.AutomationException = ex;
                    ex.LogException("Error occurred with completing card id: " + card.Id + " Titled:" + card.Title);
                }
           }
        }
        public virtual List<Card> CreateExceptionCards(List<ExceptionViolation> newViolations, Suit suit, ISuitRepository repo, User systemUser)
        {
            List<Card> result = new List<Card>();
            foreach (var violation in newViolations)
            {
                
                Card newCard = new Card
                {
                    ReferenceCode = violation.ReferenceCode,
                    DateCreated = DateTime.Now,
                    Suit = suit,
                    ReferenceName = violation.ReferenceName,
                    ReferenceObject = violation.ReferenceObject,
                    ReferenceDetail = violation.ReferenceDetail,
                    CreatedBy = violation.CreatedByName,
                    EscalateDate = suit.GetEscalateDate(DateTime.Now),
                    DueDate = suit.GetDueDate(DateTime.Now)
                };
                if (violation.AdditionalReferences != null && violation.AdditionalReferences.Count > 0)
                {
                    foreach (var newRef in violation.AdditionalReferences.ToList())
                    {
                        newCard.CardReferences.Add(new CardReference { Card = newCard, IsPrimary = false, ReferenceCode = newRef.ReferenceCode, ReferenceName = newRef.ReferenceName, ReferenceDetail = newRef.ReferenceDetail, ReferenceObject = newRef.ReferenceObject });
                    }
                }
               try
                { 
                   if (violation.DefaultUsers != null)
                    {
                        foreach (var user in violation.DefaultUsers)
                        {
                            newCard.AssignUser(user);
                            //newCard.AssignUser(violation.DefaultUser);
                        }
                    }
                    //Add cards to the context and then save to persist
                    repo.AddCard(newCard);
                    newCard.AddEvent(CardEventType.Created, systemUser, "Card created.", null);
                    repo.SaveCard(newCard);
                    //add to my list of  cards to have access to the cards outside this method
                    result.Add(newCard);
                }
                catch (Exception ex)
                {
                   newCard.Suit.AutomationException = ex; 
                   ex.LogException("Error occurred with creating card id: " + newCard.Id + " Titled:" + newCard.Title); 
                }
            }
            return result;
        }
        #endregion
    }

    public class UnitTestedEvaluator : ExceptionEvaluator
    {//an evaluator used for the unit tests
        public override void Evaluate(Suit suit, ISuitRepository repository)
        {
            base.Evaluate(suit, repository);
        }
        public override List<ExceptionViolation> GetViolators(Suit suit, ISuitRepository repository)
        {
            List<User> users = new List<User>();
            users.Add(repository.GetViewpointUser("SRABEY"));
            List<ExceptionViolation> results = new List<ExceptionViolation>();
            results.Add(new ExceptionViolation { ReferenceCode = "abc", DefaultUsers = users, ReferenceName = "name of entity", ReferenceObject = ReferenceObjectOptions.TestData });
            results.Add(new ExceptionViolation { ReferenceCode = "xyt", DefaultUsers = users, ReferenceName = "name of entity", ReferenceObject = ReferenceObjectOptions.TestData });
            return results;
        }

    }
}
