﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Walkers.Project360.CardGames;
using MCS.McsLogging;

namespace Walkers.Project360.AutoGenerateCards
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Generator process = new Generator();
                process.Start(DateTime.Now);
            }
            catch( Exception ex){
                ex.LogException();
            }
        }
    }
}
