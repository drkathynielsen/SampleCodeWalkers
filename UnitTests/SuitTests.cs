﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Walkers.Project360.CardGames;
using Walkers.Project360.Model;
using System.Collections.Generic;
using Walkers.Project360.DataAccess.Migrations;
using Walkers.Project360.DataAccess;
using System.Data.Entity.Migrations;

namespace Walkers.Project360.UnitTest
{
    [TestClass]
    public class SuitTests
    {
        /// <summary>
        ///  Seed the database for the data-driven unit tests.
        ///  NOTE: Unit test should never be run against the production evironment
        /// </summary>
        /// <param name="testContext"></param>
        [ClassInitialize]
        public static void InitTestClass(TestContext testContext)
        {
            SuitsToSeed util = new SuitsToSeed();
            util.RunSuitSeeds();

            var context = new EntityModel();
            //using (var context = new EntityModel())
            //{
                context.Suits.AddOrUpdate(s => s.EvalBehaviorName, new Suit { SuitName = "Missing Test Table Value", Type = SuitType.Exception, EvalBehaviorName = "MissingTestValueEvaluator", LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5), Enabled = true, SuitDescription = "Missing value Exception: This is some text that can say what the exception is.", SuitResolution = "Fill in a value in the test table to resolve this exception. This note could have instructions as needed for resolving the exception."});
                context.Suits.AddOrUpdate(s => s.EvalBehaviorName, new Suit { SuitName = "Test Suit", Type = SuitType.Exception, EvalBehaviorName = "Doesn'tExist", LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(0), Enabled = false});

                context.CorisEntityStatusRows.AddOrUpdate(r => r.PAR_IDENTIFIER, new CorisEntityStatusRow { Created = DateTime.Now, CreatedBy = "System", PAR_IDENTIFIER = "Test1", PAR_AGENT_IDENTIFIER = "XYZ", PAR_NAME = "TEST 2", PAR_AGENT = "N", AGENT_NAME = "Walkers Nominees Limited", PAR_COT_CODE = "EX", PAR_FILENO = "289485", PAR_STATUS = "AC", PAR_TYPE = "C", PAR_INCORPORATION = "Cayman Islands", PAR_FORMATION_DATE = "7/3/2014 0:00", PAR_RESERVE_DATE = "7/3/2014 16:20", PAR_CLIENT_REF = "WC100002" });
                context.CorisEntityStatusRows.AddOrUpdate(r => r.PAR_IDENTIFIER, new CorisEntityStatusRow { Created = DateTime.Now, CreatedBy = "System", PAR_IDENTIFIER = "Test2", PAR_AGENT_IDENTIFIER = "XYZ", PAR_NAME = "TEST 2", PAR_AGENT = "N", AGENT_NAME = "Acme Corp", PAR_COT_CODE = "EX", PAR_FILENO = "289485", PAR_STATUS = "AC", PAR_TYPE = "C", PAR_INCORPORATION = "Cayman Islands", PAR_FORMATION_DATE = "7/3/2014 0:00", PAR_RESERVE_DATE = "7/3/2014 16:20", PAR_CLIENT_REF = "WC100003" });
                context.CorisEntityStatusRows.AddOrUpdate(r => r.PAR_IDENTIFIER, new CorisEntityStatusRow { Created = DateTime.Now, CreatedBy = "System", PAR_IDENTIFIER = "Test3", PAR_AGENT_IDENTIFIER = "XYZ", PAR_NAME = "TEST 2", PAR_AGENT = "N", AGENT_NAME = "Walkers Corporate Limited", PAR_COT_CODE = "EX", PAR_FILENO = "289485", PAR_STATUS = "AC", PAR_TYPE = "C", PAR_INCORPORATION = "Cayman Islands", PAR_FORMATION_DATE = "7/3/2014 0:00", PAR_RESERVE_DATE = "7/3/2014 16:20", PAR_CLIENT_REF = "NOTHINGTOSEEHERE" });

                context.TestRows.Add(new TestRowForException { Id = 1, FieldCheckExistName = "has a value", FieldCompareName = 133 });
                context.TestRows.Add(new TestRowForException { Id = 2, FieldCompareName = 244 });

                context.TestCompareRows.Add(new TestCompareForException { Id = 1, FieldCompareName = 133 });
                context.TestCompareRows.Add(new TestCompareForException { Id = 2, FieldCompareName = 200 });

                context.SaveChanges();
            //}
        }
        [TestMethod]
        public void RunSeed()
        {
            SuitsToSeed util = new SuitsToSeed();
            util.RunSuitSeeds();
        }
        #region Getting Suits to Process
        [TestMethod]
        public void TestNextRunDate()
        {
            Suit mySuit = new Suit { LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5) };
            Assert.AreEqual(mySuit.NextRunDate, Convert.ToDateTime("1/1/2005 00:05"));
        }

        [TestMethod]
        public void TestGetEscalateDateDefault()
        {
            Suit mySuit = new Suit { };
            Assert.AreEqual(Convert.ToDateTime("3/1/2011 00:05"), mySuit.GetEscalateDate(Convert.ToDateTime("1/1/2011 00:05")));
        }

        [TestMethod]
        public void TestGetDueDateDefault()
        {// no information is entered for DaysTillDue or TillDueStored
            Suit mySuit = new Suit { };
            Assert.AreEqual(Convert.ToDateTime("4/1/2011 00:05"), mySuit.GetDueDate(Convert.ToDateTime("1/1/2011 00:05")));
        }

        [TestMethod]
        public void TestGetDueDateSupplied()
        {

            Suit mySuit = new Suit { DaysTillDue = TimeSpan.FromMinutes(5) };
            Assert.AreEqual(Convert.ToDateTime("1/1/2005 00:10"), mySuit.GetDueDate(Convert.ToDateTime("1/1/2005 00:05")));

            Suit mySuit2 = new Suit { DaysTillDue = TimeSpan.FromDays(15) };
            Assert.AreEqual(Convert.ToDateTime("16/1/2005 00:07"), mySuit2.GetDueDate(Convert.ToDateTime("1/1/2005 00:07")));

        }
        [TestMethod]
        public void TestSuitsToRunAreEnabled()
        {
            Generator cardGenerator = new Generator();
            List<Suit> suits = new List<Suit>();
            suits.Add(new Suit { Id = 1, LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5), Enabled = true });
            suits.Add(new Suit { Id = 2, LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5), Enabled = false });

            List<Suit> results = cardGenerator.GetSuitsToProcess(suits, DateTime.Now);
            Assert.IsNotNull(results.Find(s => s.Id == 1), "suites must be enabled");
            Assert.IsNull(results.Find(s => s.Id == 2), "suites must be enabled");

        }

        [TestMethod]
        public void TestSuitsToRunAreDue()
        {
            Generator cardGenerator = new Generator();
            List<Suit> suits = new List<Suit>();
            suits.Add(new Suit { Id = 1, LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5), Enabled = true });
            suits.Add(new Suit { Id = 2, LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(10), Enabled = false });

            List<Suit> results = cardGenerator.GetSuitsToProcess(suits, Convert.ToDateTime("1/1/2005 00:07"));
            Assert.IsNotNull(results.Find(s => s.Id == 1), "suites must be due");
            Assert.IsNull(results.Find(s => s.Id == 2), "suites must be due");

        }

        [TestMethod]
        public void TestSuitsToRunAreNotRunning()
        {
            Generator cardGenerator = new Generator();
            List<Suit> suits = new List<Suit>();
            suits.Add(new Suit { Id = 1, LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5), Enabled = true, IsRunning = true });
            suits.Add(new Suit { Id = 2, LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5), Enabled = true, IsRunning = false });

            List<Suit> results = cardGenerator.GetSuitsToProcess(suits, DateTime.Now);
            Assert.IsNull(results.Find(s => s.Id == 1), "suites must not be already running");
            Assert.IsNotNull(results.Find(s => s.Id == 2), "suites must not be already running");

        }
        [Ignore]
        [TestMethod]
        public void TestSuitToDebugPersisted()
        {//just for debugging purposes
            Generator cardGenerator = new Generator();

            cardGenerator.Start(Convert.ToDateTime("11/1/2005 00:00"));
        }
        #endregion

        #region Processing Suit Cards
        [TestMethod]
        public void TestGetCompletedCards()
        {
            //test data
            List<Card> existing = new List<Card>();
            existing.Add(new Card { Id = 1, ReferenceCode = "abc" });
            existing.Add(new Card { Id = 2, ReferenceCode = "mmm" });

            List<ExceptionViolation> violations = new List<ExceptionViolation>();
            violations.Add(new ExceptionViolation { ReferenceCode = "abc" });
            violations.Add(new ExceptionViolation { ReferenceCode = "xyt" });

            //method under test
            ExceptionEvaluator evaluator = new UnitTestedEvaluator();
            List<Card> result = evaluator.FindCompletedCards(violations, existing);

            //gets the completed one
            Assert.IsNotNull(result.Find(r => r.ReferenceCode == "mmm"));
        }

        [TestMethod]
        public void TestGetCompletedCardsMultipleReferences()
        {
            //test data
            List<Card> existing = new List<Card>();
            existing.Add(new Card { Id = 1, ReferenceCode = "abc" });
            Card card = new Card { Id = 2, ReferenceCode = "matches" };
            card.CardReferences.Add(new CardReference { Card = card, ReferenceCode = "xof", ReferenceName = "nno" });
            existing.Add(card);

            List<ExceptionViolation> violations = new List<ExceptionViolation>();
            violations.Add(new ExceptionViolation { ReferenceCode = "abc", AdditionalReferences = new List<CardReference> { new CardReference { ReferenceCode = "different", ReferenceName = "red" } } });
            violations.Add(new ExceptionViolation { ReferenceCode = "matches", AdditionalReferences = new List<CardReference> { new CardReference { ReferenceCode = "xof", ReferenceName = "nno" } } });

            //method under test
            ExceptionEvaluator evaluator = new UnitTestedEvaluator();
            List<Card> result = evaluator.FindCompletedCards(violations, existing);

            //gets the completed one --id = 1 is the one that is complete because there is more references on the violation abc
            Assert.IsNotNull(result.Find(r => r.Id == 1));
            //doesn't get the other one because it matches
            Assert.IsNull(result.Find(r => r.Id == 2)); 
        }
        [TestMethod]
        public void TestCardCompletionDatePopulated()
        {
            List<Card> existing = new List<Card>();
            existing.Add(new Card { ReferenceCode = "abc" });
            //existing.Add(new Card {  ReferenceCode = "mmm" });

            Walkers.Project360.DataAccess.Repository repository = new DataAccess.Repository();
            User systemUser = repository.GetSystemUser();

            //method under test
            ExceptionEvaluator evaluator = new UnitTestedEvaluator();
            evaluator.CompleteCards(ref existing, repository, systemUser);
            var results = existing.FindAll(c => c.DateCompleted == null);
            Assert.AreEqual(0, results.Count);

        }
        [TestMethod]
        public void TestGetNewExceptions()
        {
            //test data
            List<Card> existing = new List<Card>();
            existing.Add(new Card { Id = 1, ReferenceCode = "abc" });
            existing.Add(new Card { Id = 2, ReferenceCode = "mmm" });

            List<ExceptionViolation> violations = new List<ExceptionViolation>();
            violations.Add(new ExceptionViolation { ReferenceCode = "abc" });
            violations.Add(new ExceptionViolation { ReferenceCode = "xyt" });

            //method under test
            ExceptionEvaluator evaluator = new UnitTestedEvaluator();
            List<ExceptionViolation> result = evaluator.FindNewViolators(violations, existing);

            //gets the new one
            Assert.IsNotNull(result.Find(r => r.ReferenceCode == "xyt"));
        }
        [TestMethod]
        public void TestGetNewExceptionsMultipleReferences()
        {
            //test data
            List<Card> existing = new List<Card>();
            string sameRef = "abc"; //These should be in reverse alpha order to test that the concatenation comparison is ordered properly
            string sameAlt = "aaa";
            Card card = new Card { Id = 1, ReferenceCode = sameRef };
            card.CardReferences.Add(new CardReference { Card = card, ReferenceCode = sameAlt });
            existing.Add(card);

            string secondRef = "jjj";
            string diffAlt = "xcv";
            Card card2 = new Card { Id = 2, ReferenceCode = secondRef };
            card2.CardReferences.Add(new CardReference { Card = card, ReferenceCode = "no match" });
            existing.Add(card2);

            List<ExceptionViolation> violations = new List<ExceptionViolation>();
            violations.Add(new ExceptionViolation { ReferenceCode = sameRef, AdditionalReferences = new List<CardReference> { new CardReference { ReferenceCode = sameAlt } } });
            CardReference newCardRef = new CardReference { ReferenceCode = diffAlt };
            violations.Add(new ExceptionViolation { ReferenceCode = secondRef, AdditionalReferences = new List<CardReference> { newCardRef } });

            //method under test
            ExceptionEvaluator evaluator = new UnitTestedEvaluator();
            List<ExceptionViolation> result = evaluator.FindNewViolators(violations, existing);

            //gets the new one
            Assert.IsNotNull(result.Find(r => r.AdditionalReferences.Contains(newCardRef)));
            //doesn't get matching
            Assert.IsNull(result.Find(r => r.ReferenceCode == sameRef));
        }
        [TestMethod]
        public void TestAddingNewCards()
        {//adds a card without a user, tests that the added suit was persisted to database
            List<ExceptionViolation> violations = new List<ExceptionViolation>();
            violations.Add(new ExceptionViolation { ReferenceCode = "abc" });
            violations.Add(new ExceptionViolation { ReferenceCode = "xyt" });

            Walkers.Project360.DataAccess.Repository repository = new DataAccess.Repository();

            //method under test
            ExceptionEvaluator evaluator = new UnitTestedEvaluator();
            Suit suit = repository.GetSuit("Doesn'tExist");
            //evaluator.CreateExceptionCards(violations, new Suit { Id = 1, LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5), Enabled = true }, repository);
            List<Card> newCards = evaluator.CreateExceptionCards(violations, suit, repository, repository.GetSystemUser());

            Assert.AreEqual(2, newCards.Count, "method returned new cards in a list");

            Card savedCard = repository.GetCard(newCards.Find(t => t.ReferenceCode == "abc").Id);
            Assert.AreEqual(suit, savedCard.Suit);
        }

        [TestMethod]
        public void TestAssignedUsersToNewCards()
        {
            List<ExceptionViolation> violations = new List<ExceptionViolation>();
            Walkers.Project360.DataAccess.Repository repository = new DataAccess.Repository();

            User testUser = repository.GetViewpointUser("SRABEY");
            List<User> testUsers = new List<User> { testUser };
            violations.Add(new ExceptionViolation { ReferenceCode = "ddd", DefaultUsers = testUsers });


            //method under test
            ExceptionEvaluator evaluator = new UnitTestedEvaluator();
            Suit suit = repository.GetSuit("Doesn'tExist");
            //evaluator.CreateExceptionCards(violations, new Suit { Id = 1, LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5), Enabled = true }, repository);
            List<Card> newCards = evaluator.CreateExceptionCards(violations, suit, repository, repository.GetSystemUser());

            Assert.AreEqual(1, newCards.Count, "method returned new card in a list");

            Card savedCard = repository.GetCard(newCards.Find(t => t.ReferenceCode == "ddd").Id);
            Assert.IsTrue(savedCard.IsUserAssigned(testUser));

            //test that another user did not get assigned
            Assert.IsFalse(savedCard.IsUserAssigned(new User { Id = 3, Username = "suzy" }));
        }



        [TestMethod]
        public void TestUpdateCardInfoUsers()
        {//changed the Users on one card; kept the same on another
            Repository repository = new Repository();
            User testUser = repository.GetViewpointUser("SRABEY");
            User systemUser = repository.GetSystemUser();
            List<User> testDiffUsers = new List<User> { systemUser };
            List<CardEvent> cardEvents = new List<CardEvent>();

            Card card1 = new Card { ReferenceCode = "1234", ReferenceDetail = "", ReferenceName = "pinapple", Events = cardEvents };
            card1.AssignUser(testUser);

            List<Card> cards = new List<Card> { card1, new Card { ReferenceCode = "1224", ReferenceDetail = "", ReferenceName = "banana", Events = cardEvents, Users = new List<CardUser>() } };
            List<ExceptionViolation> dataRefresh = new List<ExceptionViolation> { new ExceptionViolation { ReferenceCode = "1234", ReferenceDetail = "", ReferenceName = "pinapple", DefaultUsers=testDiffUsers } 
                                                , new ExceptionViolation {ReferenceCode = "1224", ReferenceDetail = "", ReferenceName = "banana" }};

            BasicExceptionEvaluator evaluator = new BasicExceptionEvaluator();
            List<Card> results = evaluator.GetUpdatedCards(dataRefresh, cards);
            Assert.AreEqual(1, results.Count);
            var resultCard = results.Find(t => t.ReferenceCode == "1234");

            Assert.IsTrue(resultCard.IsUserAssigned(systemUser.Id));
        }
        [TestMethod]
        public void TestUpdateCardInfoUsersOrder()
        {//users appear in a different order in the list
            Repository repository = new Repository();
            User testUser = repository.GetViewpointUser("SRABEY");
            User systemUser = repository.GetSystemUser();
            List<User> testDiffUsers = new List<User> { systemUser, testUser };
            List<CardEvent> cardEvents = new List<CardEvent>();

            Card card1 = new Card { ReferenceCode = "1234", ReferenceDetail = "", ReferenceName = "pinapple", Events = cardEvents };
            card1.AssignUser(testUser);
            card1.AssignUser(systemUser);
            List<Card> cards = new List<Card> { card1, new Card { ReferenceCode = "1224", ReferenceDetail = "", ReferenceName = "banana", Events = cardEvents, Users = new List<CardUser>() } };
            List<ExceptionViolation> dataRefresh = new List<ExceptionViolation> { new ExceptionViolation { ReferenceCode = "1234", ReferenceDetail = "", ReferenceName = "pinapple", DefaultUsers=testDiffUsers } 
                                                , new ExceptionViolation {ReferenceCode = "1224", ReferenceDetail = "", ReferenceName = "banana" }};

            BasicExceptionEvaluator evaluator = new BasicExceptionEvaluator();
            List<Card> results = evaluator.GetUpdatedCards(dataRefresh, cards);
            Assert.AreEqual(0, results.Count);

        }
        [TestMethod]
        public void TestUpdateCardInfoUsersAlreadyReassigned()
        {//changed the Users on one card; kept the same on another; but the user had already been manually reassigned
            Repository repository = new Repository();
            User testUser = repository.GetViewpointUser("SRABEY");
            User systemUser = repository.GetSystemUser();
            List<User> testDiffUsers = new List<User> { systemUser };
            List<CardEvent> cardEvents = new List<CardEvent>();

            Card card1 = new Card { ReferenceCode = "1234", ReferenceDetail = "", ReferenceName = "pinapple" };
            card1.AssignUser(testUser);
            card1.AddEvent(CardEventType.Reassigned, systemUser, "Reassigned", null);
            List<Card> cards = new List<Card> { card1, new Card { ReferenceCode = "1224", ReferenceDetail = "", ReferenceName = "banana", Events = cardEvents, Users = new List<CardUser>() } };
            List<ExceptionViolation> dataRefresh = new List<ExceptionViolation> { new ExceptionViolation { ReferenceCode = "1234", ReferenceDetail = "", ReferenceName = "pinapple", DefaultUsers=testDiffUsers } 
                                                , new ExceptionViolation {ReferenceCode = "1224", ReferenceDetail = "", ReferenceName = "banana" }};

            BasicExceptionEvaluator evaluator = new BasicExceptionEvaluator();
            List<Card> results = evaluator.GetUpdatedCards(dataRefresh, cards);
            Assert.AreEqual(0, results.Count);
        }

        [Ignore()]//"Saves data to database. only run when needed"
        [TestMethod]
        public void TestSavingCardUpdatesWithUserChanges()
        {//adds a user; subtracts a user, swaps users & then tests that the changes persisted to database
            Walkers.Project360.DataAccess.Repository repository = new DataAccess.Repository();
            Suit suit = repository.GetSuit("Doesn't Exist");
            User testUser = repository.GetViewpointUser("SRABEY");
            User systemUser = repository.GetSystemUser();
            List<User> testDiffUsers = new List<User> { systemUser };
            List<CardEvent> cardEvents = new List<CardEvent>();

            Card card22 = new Card { ReferenceCode = "99122x", ReferenceName = "coco", Suit = suit, Events = cardEvents };
            card22.AssignUser(testUser);
            Card card100 = new Card { ReferenceCode = "99100x", ReferenceName = "entity", Suit = suit, Events = cardEvents };
            card100.AssignUser(testUser);

            List<Card> cards = new List<Card> { new Card { ReferenceCode = "99123x", ReferenceDetail = "", ReferenceName = "apple", Suit=suit, Events=cardEvents, Users= new List<CardUser>() }
                                            ,  card22
                                            , card100};
            List<ExceptionViolation> dataRefresh = new List<ExceptionViolation> { new ExceptionViolation { ReferenceCode = "99123x", ReferenceDetail = "", ReferenceName = "apple" , DefaultUsers= testDiffUsers} 
                                                , new ExceptionViolation {ReferenceCode = "99122x",  ReferenceName = "coco" ,  DefaultUsers= testDiffUsers}
                                                , new ExceptionViolation {ReferenceCode = "99100x", ReferenceName = "entity" }};
            BasicExceptionEvaluator evaluator = new BasicExceptionEvaluator();
            foreach (var card in cards) { repository.AddCard(card); }
            repository.SaveCards(cards);



            //method under test
            //Suit suit = repository.GetSuit(1);
            List<Card> results = evaluator.GetUpdatedCards(dataRefresh, cards);
            repository.SaveCards(results);
            Assert.AreEqual(3, results.Count, "method returned updated cards in a list");

            Card savedCard = repository.GetCard(results.Find(t => t.ReferenceCode == "99123x").Id);
            Assert.IsTrue(savedCard.IsUserAssigned(systemUser.Id));

            Card savedCard22 = repository.GetCard(results.Find(t => t.ReferenceCode == "99122x").Id);
            Assert.IsTrue(savedCard22.IsUserAssigned(systemUser.Id));
            Assert.IsFalse(savedCard22.IsUserAssigned(testUser.Id));

            Card savedCard100 = repository.GetCard(results.Find(t => t.ReferenceCode == "99100x").Id);
            Assert.IsFalse(savedCard100.IsUserAssigned(testUser));
        }
        [TestMethod]
        public void TestUpdateCardInfoEmpty()
        {
            List<Card> cards = new List<Card> { new Card { ReferenceCode = "123", ReferenceDetail = "blah", ReferenceName = "apple", Events = new List<CardEvent>(), Users = new List<CardUser>() } };
            List<ExceptionViolation> dataRefresh = new List<ExceptionViolation> { new ExceptionViolation { ReferenceCode = "123", ReferenceDetail = "blah", ReferenceName = "apple" } };
            BasicExceptionEvaluator evaluator = new BasicExceptionEvaluator();
            Assert.AreEqual(0, evaluator.GetUpdatedCards(dataRefresh, cards).Count);
        }
        [TestMethod]
        public void TestUpdateCardInfoName()
        {//changed the Name
            List<Card> cards = new List<Card> { new Card { ReferenceCode = "123", ReferenceDetail = "blah", ReferenceName = "apple", Events = new List<CardEvent>(), Users = new List<CardUser>() }
                                            , new Card { ReferenceCode = "122", ReferenceDetail = "blahzers", ReferenceName = "coco", Events = new List<CardEvent>(), Users = new List<CardUser>() } };
            List<ExceptionViolation> dataRefresh = new List<ExceptionViolation> { new ExceptionViolation { ReferenceCode = "123", ReferenceDetail = "blah", ReferenceName = "orange" } 
                                                , new ExceptionViolation {ReferenceCode = "122", ReferenceDetail = "blahzers", ReferenceName = "coco" }};
            BasicExceptionEvaluator evaluator = new BasicExceptionEvaluator();
            List<Card> results = evaluator.GetUpdatedCards(dataRefresh, cards);
            Assert.AreEqual(1, results.Count);
            Assert.AreEqual("orange", results.Find(t => t.ReferenceCode == "123").ReferenceName);
        }
        [TestMethod]
        public void TestUpdateCardInfoDetail()
        {//changed the Detail
            List<Card> cards = new List<Card> { new Card { ReferenceCode = "123", ReferenceDetail = "blah", ReferenceName = "apple", Events = new List<CardEvent>(), Users = new List<CardUser>()  }
                                            , new Card { ReferenceCode = "122", ReferenceDetail = "blahzers", ReferenceName = "coco", Events = new List<CardEvent>(), Users = new List<CardUser>() } };
            List<ExceptionViolation> dataRefresh = new List<ExceptionViolation> { new ExceptionViolation { ReferenceCode = "123", ReferenceDetail = "blah it and that too", ReferenceName = "apple" } 
                                                , new ExceptionViolation {ReferenceCode = "122", ReferenceDetail = "blahzers", ReferenceName = "coco" }};
            BasicExceptionEvaluator evaluator = new BasicExceptionEvaluator();
            List<Card> results = evaluator.GetUpdatedCards(dataRefresh, cards);
            Assert.AreEqual(1, results.Count);
            Assert.AreEqual("blah it and that too", results.Find(t => t.ReferenceCode == "123").ReferenceDetail);
        }

        [TestMethod]
        public void TestUpdateCardAssignedUser()
        {
            //change the cards assigned user from testUser to testUser2
            Walkers.Project360.DataAccess.Repository repository = new DataAccess.Repository();

            User testUser = repository.GetViewpointUser("SRABEY");
            User testUser2 = repository.GetViewpointUser("ADOLAN");

            Card testCard = new Card { ReferenceCode = "123", ReferenceDetail = "blah", ReferenceName = "apple", Events = new List<CardEvent>() };

            testCard.AssignUser(testUser);

            List<Card> cards = new List<Card> { testCard };

            List<ExceptionViolation> dataRefresh = new List<ExceptionViolation> { new ExceptionViolation { ReferenceCode = "123", ReferenceDetail = "blah", ReferenceName = "apple", DefaultUsers = new List<User> { testUser2 } } };

            BasicExceptionEvaluator evaluator = new BasicExceptionEvaluator();
            List<Card> results = evaluator.GetUpdatedCards(dataRefresh, cards);

            Assert.AreEqual(1, results.Count); //ensure that we only updated one card
            Assert.IsTrue(testCard.IsUserAssigned(testUser2)); //ensure the new user is assigned to the card
        }

        [TestMethod]
        public void TestUpdateCardAssignedUserWithReassign()
        {
            //the card has been manually reassigned, we shouldn't update the user in this case
            Walkers.Project360.DataAccess.Repository repository = new DataAccess.Repository();

            User testUser = repository.GetViewpointUser("SRABEY");
            User testUser2 = repository.GetViewpointUser("ADOLAN");

            Card testCard = new Card { ReferenceCode = "123", ReferenceDetail = "blah", ReferenceName = "apple", Events = new List<CardEvent>() };

            testCard.AssignUser(testUser);
            testCard.AddEvent(CardEventType.Reassigned, testUser, "Reassign Test", string.Empty);

            List<Card> cards = new List<Card> { testCard };

            List<ExceptionViolation> dataRefresh = new List<ExceptionViolation> { new ExceptionViolation { ReferenceCode = "123", ReferenceDetail = "blah", ReferenceName = "apple", DefaultUsers = new List<User> { testUser2 } } };

            BasicExceptionEvaluator evaluator = new BasicExceptionEvaluator();
            List<Card> results = evaluator.GetUpdatedCards(dataRefresh, cards);

            Assert.AreEqual(0, results.Count); //ensure that there were no updates
            Assert.IsTrue(testCard.IsUserAssigned(testUser)); //ensure the assigned user is unchanged
        }

        [Ignore()]//"Saves data to database. only run when needed"
        [TestMethod]
        public void TestSavingCardUpdates()
        {//adds a card without a user, tests that the added suit was persisted to database
            Walkers.Project360.DataAccess.Repository repository = new DataAccess.Repository();
            Suit suit = repository.GetSuit("Doesn't Exist");
            List<Card> cards = new List<Card> { new Card { ReferenceCode = "99123ab", ReferenceDetail = "blah", ReferenceName = "apple", Events = new List<CardEvent>(), Users = new List<CardUser>(), Suit=suit }
                                            , new Card { ReferenceCode = "99122ab", ReferenceDetail = "blahzers", ReferenceName = "coco", Events = new List<CardEvent>(), Users = new List<CardUser>(), Suit=suit } 
                                            , new Card { ReferenceCode = "99100ab", ReferenceName = "entity", Suit=suit, Events = new List<CardEvent>(), Users = new List<CardUser>() }};
            List<ExceptionViolation> dataRefresh = new List<ExceptionViolation> { new ExceptionViolation { ReferenceCode = "99123ab", ReferenceDetail = "blah it and that too", ReferenceName = "apple" } 
                                                , new ExceptionViolation {ReferenceCode = "99122ab", ReferenceDetail = "blahzers", ReferenceName = "coconut" }
                                                , new ExceptionViolation {ReferenceCode = "99100ab", ReferenceName = "entity 00"}};
            BasicExceptionEvaluator evaluator = new BasicExceptionEvaluator();
            foreach (var card in cards) { repository.AddCard(card); }
            repository.SaveCards(cards);



            //method under test
            //Suit suit = repository.GetSuit(1);
            List<Card> results = evaluator.GetUpdatedCards(dataRefresh, cards);
            repository.SaveCards(results);
            Assert.AreEqual(3, results.Count, "method returned updated cards in a list");


            Card savedCard = repository.GetCard(results.Find(t => t.ReferenceCode == "99123ab").Id);
            Assert.AreEqual(suit, savedCard.Suit);
        }

        [TestMethod]
        public void TestExtendCardDueDate()
        {
            const int DAYS_TO_EXTEND = 1;

            Suit suit = new Suit { Id = 1, SuitName = "Missing Test Table Value", Type = SuitType.Exception, EvalBehaviorName = "MissingTestValueEvaluator", LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5), Enabled = true, SuitDescription = "Missing value Exception: This is some text that can say what the exception is.", SuitResolution = "Fill in a value in the test table to resolve this exception. This note could have instructions as needed for resolving the exception." };
            Card card = new Card { Suit = suit, ReferenceCode = "testCode", ReferenceName = "Entity Name", DueDate = suit.GetDueDate(DateTimeOffset.Now), EscalateDate = suit.GetEscalateDate(DateTimeOffset.Now) };

            DateTimeOffset ed = card.EscalateDate;
            DateTimeOffset dd = card.DueDate;

            card.ExtendDueDate(TimeSpan.FromDays(DAYS_TO_EXTEND));

            Assert.AreEqual(ed.AddDays(DAYS_TO_EXTEND), card.EscalateDate);
            Assert.AreEqual(dd.AddDays(DAYS_TO_EXTEND), card.DueDate);
        }

        #endregion

        #region CardTitle

        [TestMethod]
        public void TestCardTitleDynamic()
        {
            Suit suit = new Suit { Id = 1, SuitName = "Missing Test Table Value", Type = SuitType.Exception, EvalBehaviorName = "MissingTestValueEvaluator", LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5), Enabled = true, SuitDescription = "Missing value Exception: This is some text that can say what the exception is.", SuitResolution = "Fill in a value in the test table to resolve this exception. This note could have instructions as needed for resolving the exception." };
            Card card = new Card { Suit = suit, ReferenceCode = "testCode", ReferenceName = "Entity Name" };
            Assert.AreEqual("Missing Test Table Value Exception on Entity Name (testCode)", card.Title);
        }
        [TestMethod]
        public void TestCardTitleFixed()
        {
            Suit suit = new Suit { Id = 1, SuitName = "Missing Test Table Value", Type = SuitType.Exception, EvalBehaviorName = "MissingTestValueEvaluator", LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5), Enabled = true, SuitDescription = "Missing value Exception: This is some text that can say what the exception is.", SuitResolution = "Fill in a value in the test table to resolve this exception. This note could have instructions as needed for resolving the exception." };

            Card card = new Card { Suit = suit, ReferenceCode = "testCode", ReferenceName = "Entity Name" };
            card.OverrideTitle("Overridden Title");
            Assert.AreEqual("Overridden Title", card.Title);
        }

        #endregion

        #region CardReference

        [TestMethod]
        public void TestCardReferenceCode()
        {
            var result = new Card();
            result.ReferenceCode = "123";
            Assert.AreEqual("123", result.ReferenceCode);
            result.ReferenceName = "xyz";
            Assert.AreEqual("xyz", result.ReferenceName);
        }

        [TestMethod]
        public void TestCardUniqueId()
        {
            var card = new Card();
            card.ReferenceCode = "123";
            Assert.AreEqual(card.UniqueId,"123");

            card.CardReferences.Add(new CardReference { Card= card, ReferenceCode= "qrq" });
            Assert.IsTrue(card.UniqueId.Contains("123"));
            Assert.IsTrue(card.UniqueId.Contains("qrq"));
        }
        #endregion

        #region Retrieving Cards
        [TestMethod]
        public void GettingCardUsers()
        {
            Walkers.Project360.DataAccess.Repository repository = new DataAccess.Repository();
            Card FirstCard = repository.GetCard(1);
        }

        [TestMethod]
        public void GettingCardsByUser()
        {   //make a new card and see if it is returned in the list of cards for that user
            List<ExceptionViolation> violations = new List<ExceptionViolation>();
            Walkers.Project360.DataAccess.Repository repository = new DataAccess.Repository();

            User testUser = repository.GetViewpointUser("SRABEY");
            List<User> testUsers = new List<User> { testUser };
            violations.Add(new ExceptionViolation { ReferenceCode = "uuu", DefaultUsers = testUsers });


            ExceptionEvaluator evaluator = new UnitTestedEvaluator();
            Suit suit = repository.GetSuit("Doesn'tExist");
            //evaluator.CreateExceptionCards(violations, new Suit { Id = 1, LastExecuted = Convert.ToDateTime("1/1/2005 00:00"), ExecuteFrequencyMinutes = TimeSpan.FromMinutes(5), Enabled = true }, repository);
            List<Card> newCards = evaluator.CreateExceptionCards(violations, suit, repository, repository.GetSystemUser());

            //method under test
            List<Card> myCards = repository.GetCards(testUser);

            Assert.IsTrue(myCards.Find(t => t.ReferenceCode == "uuu").IsUserAssigned(testUser));

        }
        #endregion

        #region Generate Cards By Suit

        public void ExecuteSuitByName(string evalBehaviorName)
        {
            using (Repository repo = new Repository())
            {
                Generator generator = new Generator();
                Suit suit = repo.GetSuit(evalBehaviorName);

                generator.ProcessSuit(suit, repo);

                if (suit.AutomationException != null)
                    throw suit.AutomationException;
            }
        }
        [TestMethod]
        public void RunAuditDataChanges()
        {
            ExecuteSuitByName("AuditDataChanges");
        }
        [TestMethod]
        public void GenerateAlertRegistration()
        {
            ExecuteSuitByName("AlertRegistration");
        }
        [TestMethod]
        public void GenerateNonLimitedRecourseMissingContract()
        {
            ExecuteSuitByName("NonLimitedRecourseMissingContract");
        }
        [TestMethod]
        public void GenerateShareCapitalMismatch()
        {
            ExecuteSuitByName("ShareCapitalMismatch");
        }

        [TestMethod]
        public void GenerateShareCapitalCurrencyMismatch()
        {
            ExecuteSuitByName("ShareCapitalCurrencyMismatch");
        }
        [TestMethod]
        public void GenerateMissingOfficerAppointDate()
        {
            ExecuteSuitByName("MissingOfficerAppointDate");
        }

        [TestMethod]
        public void GenerateInvalidEntityEmailAddress()
        {
            ExecuteSuitByName("InvalidEntityEmailAddress");
        }

        [TestMethod]
        public void GenerateMissingBillingEmailAddress()
        {
            ExecuteSuitByName("MissingBillingEmailAddress");
        }

        [TestMethod]
        public void GenerateInGroupNoSubGroup()
        {
            ExecuteSuitByName("InGroupNoSubGroup");
        }

        [TestMethod]
        public void GenerateInSubGroupNoMasterGroup()
        {
            ExecuteSuitByName("InSubGroupNoMasterGroup");
        }

        [TestMethod]
        public void GenerateMailingAddressMissing()
        {
            ExecuteSuitByName("MailingAddressMissing");
        }

        [TestMethod]
        public void GenerateRoAddressMissing()
        {
            ExecuteSuitByName("RoAddressMissing");
        }

        [TestMethod]
        public void GenerateMultipleEmailAddress()
        {
            ExecuteSuitByName("MultipleEmailAddress");
        }

        [TestMethod]
        public void GenerateRoMismatch()
        {
            ExecuteSuitByName("RoMismatch");
        }

        [TestMethod]
        public void GenerateIncorporationNumberMissing()
        {
            ExecuteSuitByName("IncorporationNumberMissing");
        }

        [TestMethod]
        public void GenerateIncorporationDateMissing()
        {
            ExecuteSuitByName("IncorporationDateMissing");
        }

        [TestMethod]
        public void GenerateErrorDuringSuitAutomation()
        {
            ExecuteSuitByName("ErrorDuringSuitAutomation");
        }

        [TestMethod]
        public void GenerateLongRunningSuit()
        {
            ExecuteSuitByName("LongRunningSuit");
        }

        [TestMethod]
        public void GenerateMissingFromCIMAWebsite()
        {
            // SR: fails on desktop due to proxy issues
            // ExecuteSuitByName("MissingFromCIMAWebsite");
        }

        [TestMethod]
        public void GenerateIncorporationNumberMismatchCoris()
        {
            ExecuteSuitByName("IncorporationNumberMismatchCoris");
        }

        [TestMethod]
        public void GenerateEntityNameMismatchCoris()
        {
            ExecuteSuitByName("EntityNameMismatchCoris");
        }

        [TestMethod]
        public void GenerateMissingROCoris()
        {
            ExecuteSuitByName("MissingROCoris");
        }

        [TestMethod]
        public void GenerateCorisMissingViewpoint()
        {
            ExecuteSuitByName("CorisMissingViewpoint");
        }

        [TestMethod]
        public void GenerateStatusMismatchCoris()
        {
            ExecuteSuitByName("StatusMismatchCoris");
        }

        [TestMethod]
        public void GenerateTransferOutMismatchCoris()
        {
            ExecuteSuitByName("TransferOutMismatchCoris");
        }

        [TestMethod]
        public void GenerateNoWalkersIdentifierCoris()
        {
            ExecuteSuitByName("NoWalkersIdentifierCoris");
        }

        [TestMethod]
        public void GenerateResignationDateStillAppointed()
        {
            ExecuteSuitByName("ResignationDateStillAppointed");
        }

        [TestMethod]
        public void GenerateMultiplePartnersPerGroup()
        {
            ExecuteSuitByName("MultiplePartnersPerGroup");
        }

        [TestMethod]
        public void GenerateMultipleAdminPerSubGroup()
        {
            ExecuteSuitByName("MultipleAdminPerSubGroup");
        }

        [TestMethod]
        public void GenerateInsufficientNumberOfDirectors()
        {
            ExecuteSuitByName("InsufficientNumberOfDirectors");
        }

        [TestMethod]
        public void GenerateMatterNumberMissing()
        {
            ExecuteSuitByName("MatterNumberMissing");
        }

        [TestMethod]
        public void GenerateEntityAssignedRelationshipManager()
        {
            ExecuteSuitByName("EntityAssignedRelationshipManager");
        }

        [TestMethod]
        public void GenerateRelationshipManagerAssignmentRejected()
        {
            ExecuteSuitByName("RelationshipManagerAssignmentRejected");
        }

        [TestMethod]
        public void GenerateEntityAssignedCSE()
        {
            ExecuteSuitByName("EntityAssignedCSE");
        }

        [TestMethod]
        public void GenerateCseRejected()
        {
            ExecuteSuitByName("CseRejected");
        }

        [TestMethod]
        public void GenerateEntityAssignedCdd()
        {
            ExecuteSuitByName("EntityAssignedCdd");
        }

        [TestMethod]
        public void GenerateCddRejected()
        {
            ExecuteSuitByName("CddRejected");
        }

        [TestMethod]
        public void GenerateAnnualReturnsNotFiled()
        {
            ExecuteSuitByName("AnnualReturnsNotFiled");
        }

        [TestMethod]
        public void GenerateUnpostedShareTransactions()
        {
            ExecuteSuitByName("UnpostedShareTransactions");
        }

        [TestMethod]
        public void GenerateMissingAGMMeetingNotice()
        {
            ExecuteSuitByName("MissingAGMMeetingNotice");
        }

        [TestMethod]
        public void GenerateAGMDatesInPast()
        {
            ExecuteSuitByName("AGMDatesInPast");
        }

        [TestMethod]
        public void GenerateMultipleSharedBillingContactsPerGroup()
        {
            ExecuteSuitByName("MultipleSharedBillingContactsPerGroup");
        }

        [TestMethod]
        public void GenerateMultipleGroupPerSharedBillingContacts()
        {
            ExecuteSuitByName("MultipleGroupPerSharedBillingContacts");
        }

        [TestMethod]
        public void GenerateCorisBalanceNotUpdated()
        {
            ExecuteSuitByName("CorisBalanceNotUpdated");
        }

        [TestMethod]
        public void GenerateCorisBalanceIsTooLow()
        {
            ExecuteSuitByName("CorisBalanceIsTooLow");
        }

        [TestMethod]
        public void GenerateSubGroupCodeIncorrect()
        {
            ExecuteSuitByName("SubGroupCodeIncorrect");
        }

        [TestMethod]
        public void GenerateIncorrectGroupForIndividual()
        {
            ExecuteSuitByName("IncorrectGroupForIndividual");
        }

        [TestMethod]
        public void GenerateBusinessPurposeMissing()
        {
            ExecuteSuitByName("BusinessPurposeMissing");
        }

        [TestMethod]
        public void GeneratePartnerNotAssigned()
        {
            ExecuteSuitByName("PartnerNotAssigned");
        }

        [TestMethod]
        public void GenerateSetupAdministratorGroup()
        {
            ExecuteSuitByName("SetupAdministratorGroup");
        }

        [TestMethod]
        public void GenerateApplicantForBusinessMissing()
        {
            ExecuteSuitByName("ApplicantForBusinessMissing");
        }

        [TestMethod]
        public void GenerateClientContactMissing()
        {
            ExecuteSuitByName("ClientContactMissing");
        }

        [TestMethod]
        public void GenerateBusinessContactMissing()
        {
            ExecuteSuitByName("BillingContactMissing");
        }

        [TestMethod]
        public void GenerateSourceOfFundsMissing()
        {
            ExecuteSuitByName("SourceOfFundsMissing");
        }

        [TestMethod]
        public void GenerateIncorporationNumberMismatch()
        {
            ExecuteSuitByName("IncorporationNumberMismatch");
        }

        [TestMethod]
        public void GenerateBillingFileMissing()
        {
            ExecuteSuitByName("BillingFileMissing");
        }

        [TestMethod]
        public void GenerateCDDOfficerMissing()
        {
            ExecuteSuitByName("CDDOfficerMissing");
        }

        [TestMethod]
        public void GenerateReferralOfficeCodeMissing()
        {
            ExecuteSuitByName("RefferralOfficeMissing");
        }

        [TestMethod]
        public void GenerateAnnualReturnDateMissing()
        {
            ExecuteSuitByName("AnnualReturnDateMissing");
        }

        [TestMethod]
        public void GenerateCSEOfficerMissing()
        {
            ExecuteSuitByName("CSEOfficerMissing");
        }

        [TestMethod]
        public void GenerateRelationshipManagerMissing()
        {
            ExecuteSuitByName("RelationshipManagerMissing");
        }

        [TestMethod]
        public void GeneratePlaceOfIncorporationMissing()
        {
            ExecuteSuitByName("PlaceOfIncorporationMissing");
        }

        [TestMethod]
        public void GenerateDateResignDirectorOfficerMissing()
        {
            ExecuteSuitByName("DateResignDirectorOfficerMissing");
        }

        [TestMethod]
        public void GenerateAddressRegisteredMembersMissing()
        {
            ExecuteSuitByName("AddressRegisteredMembersMissing");
        }

        [TestMethod]
        public void GenerateShareholderNotUpdated()
        {
            ExecuteSuitByName("ShareholderNotUpdated");
        }

        [TestMethod]
        public void GenerateFinancialYearEndMissing()
        {
            ExecuteSuitByName("FinancialYearEndMissing");
        }

        [TestMethod]
        public void GenerateMultipleBillingContacts()
        {
            ExecuteSuitByName("MultipleBillingContacts");
        }

        [TestMethod]
        public void GenerateMultipleClientContacts()
        {
            ExecuteSuitByName("MultipleClientContacts");
        }
        [TestMethod]
        public void GenerateAlertDirectorChange()
        {
            ExecuteSuitByName("AlertDirectorChange");
        }
        [TestMethod]
        public void TestProcessSyncUsers()
        {
            ExecuteSuitByName("SyncUsers");
        }

        [TestMethod]
        public void TestProcessSyncRoles()
        {
            ExecuteSuitByName("SyncRoles");
        }

        [TestMethod]
        public void GenerateGetDealTypeBlankAdminGroupSPV()
        {
            ExecuteSuitByName("DealTypeBlankAdminGroupSPV");
        }

        [TestMethod]
        public void GenerateAdminGroupBlankDealTypeOther()
        {
            ExecuteSuitByName("AdminGroupBlankDealTypeOther");
        }

        [TestMethod]
        public void GenerateMissingDealDates()
        {
            ExecuteSuitByName("MissingDealDates");
        }
        [Ignore]
        [TestMethod]
        public void GenerateServiceRequest()
        {
            ExecuteSuitByName("ServiceRequest");
        }

        [TestMethod]
        public void GenerateMissingSection10Document()
        {
            ExecuteSuitByName("MissingSection10Document");
        }

        [TestMethod]
        public void GenerateAlertOfficerResigned()
        {
            ExecuteSuitByName("AlertOfficerResigned");
        }
        [TestMethod]
        public void RunSyncVpToWeb()
        {
            ExecuteSuitByName("SyncVpToWeb");
        }
        [TestMethod]
        public void RunSync360ToWeb()
        {
            ExecuteSuitByName("Sync360ToWeb");
        }
        #endregion

        
    }
}
