﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Walkers.Project360.CardGames;
using Walkers.Project360.Model;
using System.Collections.Generic;
using Walkers.Project360.DataAccess.Migrations;
using Walkers.Project360.DataAccess;
using System.Data.Entity.Migrations;
using System.Web.Mvc;

namespace Walkers.Project360.UnitTest
{
    [TestClass]
    public class DeckTests
    {
        private static Deck testDeck;

        /// <summary>
        ///  Seed the database for the data-driven unit tests.
        ///  NOTE: Unit test should never be run against the production evironment
        /// </summary>
        /// <param name="testContext"></param>
        [ClassInitialize]
        public static void InitTestClass(TestContext testContext)
        {
            var context = new EntityModel();

            context.Decks.AddOrUpdate(s => s.DeckName, new Deck { DeckName = "Test Deck 1", DeckDescription = "Test Deck Description 1", Enabled = true });
            context.Decks.AddOrUpdate(s => s.DeckName, new Deck { DeckName = "Test Deck 2", DeckDescription = "Test Deck Description 2", Enabled = false });
            context.SaveChanges();

        }

        [TestMethod]
        public void TestEnableDeck()
        {
            using (Repository repo = new Repository())
            {
                Deck MyDeck = new Deck { DeckName = "Test Deck Name 1", DeckDescription = "Test Deck Decription 1", Enabled = true };
                bool target = true;
                Assert.AreEqual(MyDeck.Enabled, target);
            }
        }

        [TestMethod]
        public void TestDisableDeck()
        {
            Deck MyDeck = new Deck { DeckName = "Test Deck Name", DeckDescription = "Test Deck Decription", Enabled = false };
            bool target = false;
            Assert.AreEqual(MyDeck.Enabled, target);
        }

        [TestMethod]
        public void TestGetDeckById()
        {
            Deck target = new Deck { Id = 1, DeckName = "Test Deck 1", DeckDescription = "Test Deck Description 1", Enabled = true };
                
            using (Repository repo = new Repository())
            {
                Deck d = repo.GetDeck(target.Id);
                Assert.IsNotNull(d);
            }
        }

        //[TestMethod]
        //public void TestDisablingDeckDisablesSuit()
        //{
        //    //Deck deck = new Deck { Id = 1, DeckName = "Test Deck 1", DeckDescription = "Test Deck Description 1", Enabled = true };
        //    //User testUser = new User { Username = "TestUser", DisplayName = "Test User", Roles = new List<RoleUser>() };


        //}

    }
}
