﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Walkers.Project360.Model.ClientPortalIdentity;

namespace Walkers.Project360.Model
{
    public interface IClientPortalRepository : IDisposable
    {
        EntityGroup GetEntityGroup(string entityGroupCode, EntityGroupType type);
        ICollection<EntityGroup> GetEntityGroups(EntityGroupType type);

        vw_C360_MasterGroup GetEntityMasterGroup(string entCode);
        ICollection<vw_C360_MasterGroup> GetEntityMasterGroups();
        ICollection<vw_C360_MasterGroup> GetEntityMasterGroups(string term);

        vw_C360_SubGroup GetEntitySubgroup(string entityGroupCode);
        ICollection<vw_C360_SubGroup> GetEntitySubGroupsByName(string term);
        ICollection<vw_C360_SubGroup> GetEntitySubGroups(string entityGroupCode);

        vw_C360_Entity GetEntity(string entCode);
        ICollection<vw_C360_Entity> GetEntityBySubGroup(string entityGroupCode, int limit);

        User Link360User(ApplicationUser portalUser);
        void AddNewClientPortalUser(ApplicationUser portalUser);
        void AddPortalGroupVisibility(User user, string selectedGroups, EntityGroupType groupType);
        void AddPortalGroupVisibility(int userId, string selectedGroups, EntityGroupType groupType);
        void ReassignPortalGroupVisibility(int userId, string selectedGroups, EntityGroupType groupType);
        void DisableClientPortalUser(int userId);
        ICollection<vw_C360_Entity> GetPortalUserEntityVisibility(int userId, string searchTerm);
        ICollection<PortalUserGroupVisibility> GetPortalUserVisibilityWithGroupName(EntityGroupType groupType);
        ICollection<PortalUserGroupVisibility> GetPortalUserVisibilityWithGroupName(int userId, EntityGroupType groupType);
        bool IsPortalUserGroupVisible(int userId, string groupCode, EntityGroupType groupType);
        bool IsPortalUserEntityVisible(int userId, string entCode);

        User GetClientPortalUser(string userName);
        User GetUser(int id);
        Role GetRole(string name);
        List<User> GetUsers(string displayName);

        string GetSetting(string Key);
        int GetIntSetting(string Key);

        List<vw_C360_Entity> GetRegistrationByTypeDetails(string entityGroupCode, EntityGroupType entityGroupType, string type);
        Dictionary<string, int> GetRegistrationByTypeSummary(string entityGroupCode, EntityGroupType entityGroupType);

        List<vw_C360_Entity> GetEntityByTypeDetails(string entityGroupCode, EntityGroupType entityGroupType, string type);
        Dictionary<string, int> GetEntityByTypeSummary(string entityGroupCode, EntityGroupType entityGroupType);

        List<vw_C360_Entity> GetEntityByStatusDetails(string entityGroupCode, EntityGroupType entityGroupType, string status);
        Dictionary<string, int> GetEntityByStatusSummary(string entityGroupCode, EntityGroupType entityGroupType);

        Dictionary<string, int> GetAgedARSummary(string entityGroupCode, EntityGroupType entityGroupType);
        List<AgeBucket> GetAgeBuckets();
        ICollection<vw_C360_InvoiceHeader> GetGroupInvoices(string groupName, EntityGroupType groupType);
        
        ICollection<Deadline> GetEntityGroupDeadlines(string entityGroupCode, EntityGroupType entityGroupType);
        ICollection<Deadline> GetEntityGroupDeadline(string entityGroupCode, EntityGroupType entityGroupType, int deadlineTypeId, DateTime date);

        IEnumerable<FileStore> GetFile(string refCode, string identifier);
        FileStore GetCompressedFile(string entityGroupCode, EntityGroupType entityGroupType);
        FileStore GetFileFromUrl(string url);

        Task SendAsyncSMTP(Microsoft.AspNet.Identity.IdentityMessage identityMessage);
        string GetEmailMessage(string baseUrl, string emailHeader, string emailFooter, string portalCallBackPath);

        List<OfficerSummary> GetOfficerSummary(string entityGroupCode, EntityGroupType entityGroupType, string officerType);
        List<OfficerAppointment> GetOfficerDetails(string entityGroupCode, EntityGroupType entityGroupType, string officerType, string officerEntCode);
        List<vw_C360_Addresses> GetEntityAddresses(string entCode);
        List<vw_C360_Registration> GetEntityRegistrations(string entCode);
        List<vw_C360_ShareClass> GetEntityShareClasses(string entCode);
        List<vw_C360_Shareholders> GetEntityShareholders(string entCode);
        List<vw_C360_Officer> GetEntityOfficers(string entCode);
								iCal GetiCalEvent(string title, DateTime duedate, string UUID);
    }
}
