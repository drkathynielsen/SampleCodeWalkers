﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Walkers.Project360.Model.Utilities;

namespace Walkers.Project360.Model
{

    public enum ReferenceObjectOptions
    {
        ViewpointEntity,
        ViewpointIndividual,
        Suit,
        CorisFile,
        CorisBalance,
        Request,
        TestData,
        ViewpointLookup,
        AuditHistory
    }
    public enum CardStatusOptions
    {
        [Description("")]
        None = 0,

        Ignored = 1, //this is for auto generated cards that can be set to be ignored by the autogenerator

        Open = 2,

        Completed = 3,

        Cancelled = 4 //this is for requests that are user cancelled
    }

    public enum CardEscalationStatusOptions
    {

        None = 0,

        [Description("In Progress")]
        InProgress = 1,

        [Description("Due")]
        Due = 2,

        [Description("Overdue")]
        Overdue = 3
    }


    [Table("Card")]
    public class Card
    {
        public Card()
        {
            DueDate = DateTime.Now;
            Status = CardStatusOptions.Open;
        }
        [Key]
        public int Id { get; set; }

        public virtual Suit Suit { get; set; }

        #region Reference

        public string UniqueId
        {
            get
            {
                string result;
                if (this.CardReferences == null) this.CreatePrimaryReference();

                if (this.CardReferences.Count>1 )
                    { result = string.Join("|", this.CardReferences.OrderBy(c => c.ReferenceCode).Select(c => c.ReferenceCode)); }
                else { result = this.ReferenceCode; }
                return result;
            }
        }

        public CardReference PrimaryReference
        {
            get
            {
                if (this.CardReferences == null) this.CreatePrimaryReference();
                var result = this.CardReferences.Where(c => c.IsPrimary == true).FirstOrDefault();
                if (result == null)
                {
                    this.CreatePrimaryReference();
                    result = this.CardReferences.Where(c => c.IsPrimary == true).FirstOrDefault();
                }
                return result;
            }
        }
        private void CreatePrimaryReference()
        {
            if (this.CardReferences == null) this.CardReferences = new List<CardReference>();
            if (this.CardReferences.Where(c => c.IsPrimary == true).FirstOrDefault() == null) this.CardReferences.Add(new CardReference { IsPrimary = true });
        }
        [NotMapped]
        public string ReferenceCode
        {
            get { return this.PrimaryReference.ReferenceCode; }
            set { this.PrimaryReference.ReferenceCode = value; }
        }
        [NotMapped]

        [DisplayName("Reference Name")]
        public string ReferenceName
        {
            get { return this.PrimaryReference.ReferenceName; }
            set { this.PrimaryReference.ReferenceName = value; }
        }

        [NotMapped]
        [DisplayName("Reference")]
        public string ReferenceDetail
        {
            get { return this.PrimaryReference.ReferenceDetail; }
            set { this.PrimaryReference.ReferenceDetail = value; }
        }
        [NotMapped]
        public ReferenceObjectOptions ReferenceObject
        {
            get { return this.PrimaryReference.ReferenceObject; }
            set { this.PrimaryReference.ReferenceObject = value; }
        }
        [NotMapped]
        [Column("ReferenceObject")]
        public string ReferenceObjectString
        {
            get { return this.ReferenceObject.ToString(); }
            private set { ReferenceObject = EnumExtensions.ParseEnum<ReferenceObjectOptions>(value); }
        }

        public virtual ICollection<CardReference> CardReferences { get; set; }

        #endregion

        public DateTimeOffset EscalateDate { get; set; }

        public DateTimeOffset DueDate { get; set; }

        [DisplayName("Status")]
        [Range(1, int.MaxValue, ErrorMessage = "A status is required")]
        public CardStatusOptions Status { get; set; }

        [DisplayName("Escalation Status")]
        [NotMapped]
        public CardEscalationStatusOptions EscalationStatus
        {
            get
            {
                if (this.Status == CardStatusOptions.Open)
                {
                    if (DateTimeOffset.Now > this.DueDate) return CardEscalationStatusOptions.Overdue;
                    if (DateTimeOffset.Now > this.EscalateDate) return CardEscalationStatusOptions.Due;
                    return CardEscalationStatusOptions.InProgress;
                }
                else
                    return CardEscalationStatusOptions.None;
            }
        }

        //private string hardTitle;
        public string HardTitle { get; set; }

        [NotMapped]
        public string Title
        {   //returns a persisted title if it exists otherwise returns a dynamic one
            get
            {
                if (HardTitle == null)
                {
                    if (this.Suit != null)
                    {
                        return this.Suit.CardTitle(this);
                    }
                    else
                    {
                        return "Card";
                    }
                }
                else
                {
                    return HardTitle;
                }
            }
        }
        public void OverrideTitle(string fixedTitle)
        {
            this.HardTitle = fixedTitle;
        }

        public virtual ICollection<CardUser> Users { get; set; }
        public void AssignUser(User user)
        {
            if (user != null)
            {
                if (this.Users == null)
                    this.Users = new List<CardUser>();

                if (!this.IsUserAssigned(user))
                {
                    CardUser newCardUser = new CardUser();
                    newCardUser.Card = this;
                    newCardUser.User = user;
                    this.Users.Add(newCardUser);
                }
            }
        }
        public List<CardUser> GetCardUser(int userId)
        {
            var query = from cardUser in this.Users
                        where cardUser.User.Id == userId
                        select cardUser;
            return query.ToList();
        }

        public List<CardUser> GetCardUser(User user)
        {
            return this.GetCardUser(user.Id);
        }

        public bool IsUserAssigned(User user)
        {
            return GetCardUser(user).Any();
        }

        public bool IsUserAssigned(int userId)
        {
            return GetCardUser(userId).Any();
        }

        public void ReassignUsers(List<User> assignedUsers)
        {
            //find new users
            var newUsers = assignedUsers.Where(x => !this.Users.Select(ux => ux.User.Id).Contains(x.Id)).ToList();
            newUsers.ForEach(u => this.AssignUser(u));

            //find users that have been removed
            var removedUsers = this.Users.Where(x => !assignedUsers.Select(ux => ux.Id).Contains(x.User.Id)).ToList();
            removedUsers.ForEach(u => this.Users.Remove(u));
        }

        public virtual ICollection<CardEvent> Events { get; set; }
        public void AddEvent(CardEvent cardEvent)
        {
            if (this.Events == null)
                this.Events = new List<CardEvent>();

            this.Events.Add(cardEvent);
        }
        public void AddEvent(CardEventType eventType, User doneByUser, string title, string comment)
        {
            CardEvent cardEvent = new CardEvent();
            cardEvent.Card = this;
            cardEvent.Type = eventType;
            cardEvent.DoneByUserId = doneByUser.Id;
            cardEvent.DoneByName = doneByUser.Username;
            cardEvent.Title = title ?? "Message For You";
            cardEvent.Comment = comment ?? "";

            this.AddEvent(cardEvent);
        }
        public List<Message> MakeUserMessages(User fromUser, string subject)
        {
            List<Message> result = new List<Message>();
            if (this.Users != null)
            {
                foreach (CardUser cu in this.Users)
                {
                    MessageCard m = MakeMessage(fromUser, cu.User, subject); //new MessageCard();

                    result.Add(m);
                }
            }
            return result;
        }
        public MessageCard MakeMessage(User fromUser, User toUser, string subject)
        {
            MessageCard result = new MessageCard();
            result.To = toUser;
            result.From = fromUser;
            result.Subject = subject;
            result.Card = this;
            return result;
        }

        public void Complete(User currentUser)
        {
            this.DateCompleted = DateTime.Now;
            this.Status = CardStatusOptions.Completed;
            this.AddEvent(CardEventType.Completed, currentUser, "Card Completed", null);

        }
        public void ProcessServiceRequest(string status, User currentUser)
        {

            switch (status)
            {
                case "Complete":
                    this.Complete(currentUser);
                    break;
                case "Cancel":
                    this.Status = CardStatusOptions.Cancelled;
                    break;
                default:
                    //do nothing if still in progress
                    break;
            }
        }
        /// <summary>
        /// Extend the card's due date by the specified timespan.
        /// The escalation date is also extended by the same amount of time.
        /// </summary>
        /// <param name="TimeToExtend"></param>
        public void ExtendDueDate(TimeSpan TimeToExtend)
        {
            this.EscalateDate = this.EscalateDate.Add(TimeToExtend);
            this.DueDate = this.DueDate.Add(TimeToExtend);
        }

        public DateTimeOffset? DateCreated { get; set; }
        public DateTimeOffset? DateCompleted { get; set; }
        public string CreatedBy { get; set; }
    }


}
