﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Walkers.Project360.Model
{
    public enum SuitType
    {
        Exception = 1,
        Approval = 2,
        Request = 3,
        ScheduledProcess = 4,
        AuditAlert = 5
    }

    [Table("Suit")]
    public class Suit
    {
        const string ReferenceDate = "1/1/1900 00:00";
        const int EscalateDaysDefault = 2;
        const int DueDaysDefault = 3;

        public Suit()
        {
            AssignCardTitleBehavior();
            this.IsRunning = false;
            this.DaysTillEscalate = TimeSpan.FromDays(EscalateDaysDefault);
            this.DaysTillDue = TimeSpan.FromDays(DueDaysDefault);
        }
        [Key]
        public int Id { get; set; }

        public virtual Deck Deck { get; set; }

        [Required]
        [DisplayName("Name")]
        public string SuitName { get; set; }
        
        [Required]
        private SuitType suitType;
        public SuitType Type { get { return suitType; } set { suitType = value; this.AssignCardTitleBehavior(); } }

        [NotMapped]
        [DisplayName("Frequency (Days.hh:mm:ss)")]
        public TimeSpan ExecuteFrequencyMinutes
        { //This was exposed in order to enable the code to work with a timeSpan object, even though the database stores a datetime
            get
            {
                TimeSpan results = TimeSpan.Zero;
                //make sure that a real date had been stored, not the minimum datetime  value
                if (this.ExecuteFrequencyStored > Convert.ToDateTime(ReferenceDate))
                {
                    results = this.ExecuteFrequencyStored.Subtract(Convert.ToDateTime(ReferenceDate));
                }
                return results;
            }
            set
            {
                DateTime results = Convert.ToDateTime(ReferenceDate).Add(value);
                this.ExecuteFrequencyStored = results;
            }
        }

        [Required]
        public DateTime ExecuteFrequencyStored { get; set; }

        /// <summary>
        /// When did the suit automation last start
        /// </summary>
        [DisplayName("Last Started")]
        public DateTime? LastStarted { get; set; }

        /// <summary>
        /// When did the suit automation last finish
        /// </summary>
        [DisplayName("Last Executed")]
        public DateTime? LastExecuted { get; set; }

        /// <summary>
        /// How long did the last suit automation take to complete
        /// </summary>
        [NotMapped]
        public TimeSpan? LastExecutedDuration
        {
            get
            {
                if (this.LastStarted != null && this.LastExecuted != null && this.LastExecuted.Value > this.LastStarted.Value)
                    return this.LastExecuted.Value.Subtract(this.LastStarted.Value);
                else
                    return null;
            }
        }

        [Required]
        [DisplayName("Is Running")]
        public Boolean IsRunning { get; set; }

        public Boolean Enabled { get; set; }

        [DisplayName("Description")]
        public String SuitDescription { get; set; }

        [DisplayName("Resolution")]
        public String SuitResolution { get; set; }

        [DisplayName("Evaluator Behavior Name")]
        [Index(IsUnique = true)]
        [Required]
        [MaxLength(255)]
        public String EvalBehaviorName { get; set; }

        [DisplayName("Escalation Window (Days.hh:mm:ss)")]
        [NotMapped]
        public TimeSpan DaysTillEscalate
        {//This was exposed in order to enable the code to work with a timeSpan object, even though the database stores a datetime
            get
            {
                TimeSpan results = TimeSpan.Zero;
                //make sure that a real date had been stored, not the minimum datetime  value
                if (this.TillEscalateStored > Convert.ToDateTime(ReferenceDate))
                {
                    results = this.TillEscalateStored.Subtract(Convert.ToDateTime(ReferenceDate));
                }
                return results;
            }
            set
            {
                DateTime results = Convert.ToDateTime(ReferenceDate).Add(value);
                this.TillEscalateStored = results;
            }
        }

        public DateTime TillEscalateStored { get; set; } //The field where DaysBeforeToEscalate is persisted.

        [DisplayName("Time to Resolve (Days.hh:mm:ss)")]
        [NotMapped]
        public TimeSpan DaysTillDue
        {//This was exposed in order to enable the code to work with a timeSpan object, even though the database stores a datetime
            get
            {
                TimeSpan results = TimeSpan.Zero;
                //make sure that a real date had been stored, not the minimum datetime  value
                if (this.TillDueStored > Convert.ToDateTime(ReferenceDate))
                {
                    results = this.TillDueStored.Subtract(Convert.ToDateTime(ReferenceDate));
                }
                return results;
            }
            set
            {
                DateTime results = Convert.ToDateTime(ReferenceDate).Add(value);
                this.TillDueStored = results;
            }
        }

        public DateTime TillDueStored { get; set; } //The field where DaysTillDue is persisted.

        [NotMapped]
        public ISuitEvalBehavior EvalBehavior { get; set; }
        [NotMapped]
        public ISuitCardTitleBehavior CardTitleBehavior { get; set; }

        void AssignCardTitleBehavior()
        {
            if (this.Type == SuitType.Request || this.Type ==SuitType.AuditAlert)
            {
                CardTitleBehavior = new ServiceRequestTitleBehavior();
            }
            else
            {
                CardTitleBehavior = new ViewpointExceptionTitleBehavior();
            }
        }

        public string CardTitle(Card card)
        {
            return CardTitleBehavior.GetCardTitle(card);
        }

        public DateTime NextRunDate
        {
            get
            {
                if (this.LastExecuted.HasValue)
                    return this.LastExecuted.Value.Add(this.ExecuteFrequencyMinutes);
                else
                    return DateTime.MinValue;
            }
        }

        public DateTimeOffset GetEscalateDate(DateTimeOffset startDate)
        {
            TimeSpan daysToAdd = TimeSpan.FromDays(EscalateDaysDefault);
            if (this.DaysTillEscalate != TimeSpan.Zero)
            {
                daysToAdd = this.DaysTillEscalate;
            }
            return startDate.Add(daysToAdd);
        }

        public DateTimeOffset GetDueDate(DateTimeOffset startDate)
        {
            TimeSpan daysToAdd = TimeSpan.FromDays(DueDaysDefault);
            if (this.DaysTillDue != TimeSpan.Zero)
            {
                daysToAdd = this.DaysTillDue;
            }
            return startDate.Add(daysToAdd);
        }

        /// <summary>
        /// Used to track whether a suit failed during automatic create and complete. Only holds the first exception.
        /// </summary>
        private Exception _exception;
        [NotMapped]
        public Exception AutomationException
        {
            get { return _exception; }
            set
            {
                if (_exception == null) _exception = value;
            }
        }

    }

    public class ViewpointExceptionTitleBehavior : ISuitCardTitleBehavior
    {
        public string GetCardTitle(Card card)
        {
            return card.Suit.SuitName + " Exception on " + card.ReferenceName + " (" + card.ReferenceCode + ")";
        }
    }

    public class ServiceRequestTitleBehavior : ISuitCardTitleBehavior
    {
        public string GetCardTitle(Card card)
        {
            return card.Suit.SuitName + " on " + card.ReferenceName + " (" + card.ReferenceCode + ")";
        }
    }

}