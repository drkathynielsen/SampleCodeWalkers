﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Walkers.Project360.Model
{

    [Table("CardUser")]
    public class CardUser
    {
        /// <summary>
        /// Warning: this is a system reserve field. Do not use. It will not contain valid data until the object has been added to the DB context. Can use repo.AddCard method
        /// </summary>
        [Key]
        [Column(Order = 10)]
        [ForeignKey("Card")]
        public int CardId { get; set; }

        /// <summary>
        /// Warning: this is a system reserve field. Do not use. It will not contain valid data until the object has been added to the DB context. Can use repo.AddCard method
        /// </summary>
        [Key]
        [Column(Order = 20)]
        [ForeignKey("User")]
        public int UserId { get; set; }

        public virtual Card Card { get; set; }
        public virtual User User { get; set; }


        public DateTimeOffset? DateCreated { get; set; }

        public CardUser()
        {
            DateCreated = DateTime.Now;
        }
    }
}
