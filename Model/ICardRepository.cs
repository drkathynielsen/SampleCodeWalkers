﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Walkers.Project360.Model
{
    public interface ISuitRepository
    {
        Card GetCard(int id);

        List<Card> GetSuitCards(Suit suit);

        void AddCard(Card newCard);

        void SaveCard(Card card);
        void SaveCards(List<Card> cards);

        User GetViewpointUser(string ViewpointRefCode);

        User GetSystemUser();

        void SaveRunningSuit(Suit suit);

        void SaveFinishedSuit(Suit suit);

        List<ExceptionViolation> GetViolations(Suit suit);

        List<Request> GetNewRequests();

        EntityAdmin GetCardData(string entCode);

        void SaveRequestCards(List<RequestCard> cards);

        int GetIntSetting(string Key);

        void RunProcess(Suit suit);

        Task<bool> SendAsyncSMTP(System.Net.Mail.MailMessage mail, User currentUser);

        string GetSetting(string Key);
    }
}
