﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Walkers.Project360.Model.Utilities;

namespace Walkers.Project360.Model
{
    [Table("CardReference")]
    public class CardReference
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Card")]
        public int CardId { get; set; }

        public Card Card { get; set; }

        public bool IsPrimary { get; set; }

        public string ReferenceCode { get; set; }

        public string ReferenceName { get; set; }

        public string ReferenceDetail { get; set; }
        [NotMapped]
        public ReferenceObjectOptions ReferenceObject { get; set; }

        [Column("ReferenceObject")]
        public string ReferenceObjectString
        {
            get { return this.ReferenceObject.ToString(); }
            private set { ReferenceObject = EnumExtensions.ParseEnum<ReferenceObjectOptions>(value); }
        }
    }
}
