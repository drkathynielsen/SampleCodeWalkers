﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Walkers.Project360.Model
{
    public enum CardEventType
    {
        Created = 1,
        Completed = 2,
        Reassigned = 3,
        Escalated = 4,
        ExtendDueDate = 5,
        ChangeStatus = 6,
        Comment = 7,
        ApprovalResponse = 8,
        SystemUpdated =9
    }
    [Table("CardEvent")]
    public class CardEvent
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public Card Card { get; set; }

        [Required]
        public CardEventType Type { get; set; }

        public string Title { get; set; }

        [Required]
        public DateTimeOffset DoneDate { get; set; }

        public string DoneByName { get; set; }
        public int DoneByUserId { get; set; }

        public String Comment { get; set; }

        public CardEvent()
        {
            this.DoneDate = DateTimeOffset.Now;
        }
    }




}
