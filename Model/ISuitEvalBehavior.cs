﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Walkers.Project360.Model
{
    public interface ISuitEvalBehavior
    {
        void Evaluate(Suit suit, ISuitRepository repository);
    }
}
